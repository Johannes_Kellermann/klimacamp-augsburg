unset border
set style fill solid 1.0
set encoding utf8
set tics scale 0
set boxwidth 0.5
unset ytics
set xlabel "Haushaltsnettoeinkommen (pro Person)" 
set yrange [ -90 : 90 ]
set terminal svg size 900,480
set output "klimapraemie.svg"
set title "Monatliche Ent- und Belastung durch CO2-Prämie\n(195 € pro Tonne CO₂, damit monatliche Prämie von 180 €; Lenkungswirkung unberücksichtigt)"
plot \
  "klimapraemie.txt" using ($3 > 0 ? $3 : 1/0):xticlabels(1) with boxes fc '#5fbbc8' t "", \
  "klimapraemie.txt" using ($3 < 0 ? $3 : 1/0):xticlabels(1) with boxes fc '#b6016a' t "", \
  "klimapraemie.txt" using ($3 > 0 ? $2 : 1/0):3:(sprintf("+%d €", $3)) with labels offset char 0,1 t "", \
  "klimapraemie.txt" using ($3 < 0 ? $2 : 1/0):3:(sprintf("–%2d €", -$3)) with labels offset char 0,-1 t "", \
  0 w l lw 3 lc 0 t ""
