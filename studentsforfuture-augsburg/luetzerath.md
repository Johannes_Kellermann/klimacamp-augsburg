---
layout: page
title: "Widerstand gegen Zerstörungswut: Lützerath"
permalink: /luetzerath/
nav_order: 10
---

<img src="/banner2.webp" alt="" width="1203" height="580" style="width: 100%; height: auto; aspect-ratio: attr(width) / attr(height)">

<img src="/banner.webp" alt="" width="422" height="596" style="width: 100%; height: auto; aspect-ratio: attr(width) / attr(height)">

**Lützerath**, liebevoll von uns in der Klimagerechtigkeitsbewegung **Lützi**
genannt, ist ein Dorf in NRW. Es ist Opfer des Zerstörungswahns von RWE, der
einseitige Profitgier über das Gemeinwohl stellt: Denn Lützi soll vollständig
zerstört werden, um Platz zu machen für -- eine neue
<del>Autobahn</del> <del>Kiesgrube</del> **Kohlegrube**.

Bis zum 7.1. hat Lützi aufgrund von anhängigen Gerichtsverfahren eine kurze
Gnadenfrist bekommen. Aber voraussichtlich kommt es am 8.1. zur
**Räumung**. Und dann gibt es nur eine Möglichkeit, Lützi vor
den Interessen der Kohleprofitlobby zu retten: Wir müssen viele sein. In Lützi
sowie in ganz Deutschland mit zahlreichen Soli-Aktionen. Aber vor allem in
Lützi.

<p style="text-align: center">
  <a href="https://www.youtube.com/watch?v=9s3TkD6c0cQ">
    <img src="/video-luetzerath.webp" alt="" width="320" height="180" style="width: 100%; height: auto; aspect-ratio: attr(width) / attr(height)">
    <br>
    Widerstand gegen Zerstörungswut – Lützerath und ZAD
    Rheinland (YouTube)
  </a>
</p>

# Nächste Schritte

1. Informiere dich!
2. Besuch unser Aktionstraining!
3. Fahr mit uns nach Lützi!
