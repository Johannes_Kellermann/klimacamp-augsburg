---
layout: home
title: Aktuelles
permalink: /
nav_order: 1
---

# Aktuelles

<img src="/solidarisch-durch-die-krise.webp" width="540" height="540" style="width: 100%; height: auto;
aspect-ratio: attr(width) / attr(height)">


## Liebe Interessierte und Aktive,

am Montag, 10.1. spazieren die Querdenker\*innen wieder unangemeldet und
ungeschützt durch unsere Stadt.

Das Aktionsbündnis „Augsburg solidarisch“ ruft deshalb zu einer
Gegenkundgebung auf, in der wir einen solidarischen Weg aus der Pandemie
fordern und vertreten.

Wir interessieren uns nicht für Verschwörungserzählungen, unglaubwürdige
Experten und abstruse Erklärungen. Wir halten mit Fakten dagegen, sehen uns
im Team Wissenschaft und sind in engem Austausch mit denen, die diese
Pandemie hautnah in den Kliniken ertragen müssen. Deshalb gehen wir hier
bewusst in den Widerstand.

Kommt am 10. Januar um 18 Uhr zum Kö auf unsere Kundgebung und zeigt, dass
wir viele Menschen sind, die sich gegenseitig solidarisch durch die Pandemie
helfen und diesen Unsinn nicht mehr hinnehmen wollen. Alerta!

<script data-no-instant>
  {% include instantclick.min.js %}
  InstantClick.init();
</script>
