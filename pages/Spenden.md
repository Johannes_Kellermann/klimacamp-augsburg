---
layout: page
title: Spenden
permalink: /spenden/
nav_order: 140
---

# Spenden
## Geldspenden
Wir sind über Spenden sehr dankbar und wissen diese sehr zu schätzen.

Wir finanzieren darüber Campmaterialien, Flyer, Plakate, Banner, gelegentlich
Klettermaterialien, juristische Ausgaben und diverse kleinere Posten.

Dankenswerterweise nimmt die [Bürgerstiftung
Augsburg](https://www.buergerstiftung-augsburg.de/) für uns Spenden entgegen.
Die Kontodaten lauten:

    Name: Bürgerstiftung Augsburg
    IBAN: DE22 7205 0000 0000 0263 69
    Verwendungszweck: Klimacamp (unbedingt angeben, sonst kann die Spende nicht zugeordnet werden!)

## Sachspenden
Es gibt es ein paar Dinge, die wir immer wieder gebraucht werden und die gerne jederzeit [im Camp](/mitmachen/#adresse) vorbei gebracht werden können.

**Bitte keine Gegenstände neu kaufen und nur überflüssiges Material spenden!**

### Dringend benötigt
- **Feldbetten**
- Kletterseile (dynamsich / statisch) auch gebrauchte alte als Materialseil!
- **große Holzsäge**
- **Zieheisen/Schälmesser/Schäleisen** zum Entrinden von Baumstämmen**
- Tassen/Becher aus Edelstahl, Emaillie oder Hartplastik
- Panzertape
- Planen
- **Bettlaken, Bettbezüge, Tischdecken und andere Stoffe jeder Farbe als Bannerstoff**
- Eddings und andere Permanentmarker
- **Mannschaftszelte , feuerfeste Mannschaftszelte, Bundeswehrzelte, Küchenzelte**
- **Fahrradschlösser, alte Fahrräder, Fahrradteile**
- Beamer (gerne auch als Leihgabe)

### Werkzeuge und Baumaterial
- Säge, Hammer, Zangen, Schraubenzieher, Schraubschlüssel, ...
- Akkuschrauber, Akkubetriebenes Werkzeug, Akkus
- Holzbretter/-latten und Kanthölzer (so lang wie möglich)
- Holzbalken
- Paletten
- Planen (LKW-Planen, Werbebanner, etc.)
- Wellblech und ähnliche Dächer
- bruchfeste transparente (Plexiglas, Polycarbnat) Scheiben
- Panzertape- Brechstange, Axt- Handkarren, Schubkarren,
- Metallstangen- Schrauben, Nägel, Metallwinkel, ...
- Fenster

### Küchenutensilien
- bruchfestes, wiederverwendbares Geschirr (besonders Becher und  Teller) aus Emaille oder Hartplastik
- Große Töpfe
- sonstiges Geschirr (bruchfest)
- Besteck- Großküchenbedarf
- **Nudelmaschine**

### Essen (vegan, im Besten Fall Bio und Regional)
- **vegane Aufstriche**
- **Snacks, vegane Schokolade und Müsliriegel**


### Fahrradbedarf
(für unsere gemeinschaftlichen Fahrräder und die Bikekitchen, die sich um die Reperatur und Pflege kümmert)
- **Schlösser jeder Art **
- gebrauchte Fahräder, Lastenräder & Anhänger
- Schläuche
- Werkzeug (Inbusschlüsssel, Schraubenschlüssel, ...)


### Outdoor
- **Hängematten**
- Feuerzeuge
- Taschenmesser, Rettungsmesser
- stabile Trinkwasserkanister alle Größen
- Rucksäcke
- verschließbare Taschen
- Zelte
- Sekundenkleber
- Abtönfarbe und Acrylfarbe
- Bannerstoff (Bettlaken, Stoffe, Tischdecken und Co jeder Farbe)
- Erste-Hilfe-Sets
- **große lebensmittelsicher verschließbare Kisten**
- **Eddings** und andere Stifte

### Elektronik
- **Powerbanks**
- Laptops
- **gebrauchte Smartphones und Handys**
- Registrierte SIM-Karten
- **Kopflampen** (am besten mit Rotlicht)
- **Megaphon**
- Solarpanele
- Solarladeregler
- PV/Solar Verkabelung Solarstecker

### Klettermaterialien
*Gebrauchtes Klettermaterial bitte immer beschriften mit Art, Kaufzeitpunkt, Nutzungsdauer und -intensität (Zettel dran, nicht direkt draufschreiben).*
- Kletterseile (dynamsich / statisch) auch gebrauchte alte als Materialseil!
- Klettergurte
- Karabiner (jede Form), Materialkarabiner
- Seilrollen
- Baumpflegeausrüstung
- Drahtseile
- und was ihr sonst so übrig habt

### Kleidung (alles was warm hält)
- Gummistiefel
- Thermo-Unterwäsche
- Regenkleidung, Winterkleidung
- warme Winterschuhe
- **warme Socken**
- Arbeitshandschuhe
