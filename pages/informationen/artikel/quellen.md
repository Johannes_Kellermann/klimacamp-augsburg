---
layout: page
title: Quellen
permalink: /informationen/artikel/quellen/
parent: "Weitere Informationen"
nav_order: 10
---

# Quellen – Woher beziehen wir unsere Informationen?

Mit unseren Forderungen und Aussagen
folgen wir einschlägigen Empfehlungen
von relevanten Gremien und Institutionen, um sicher zu gehen,
dass sie auf einem soliden nachvollziehbarem Fundament stehen,
sowohl in wissenschaftlichen als auch in sozialen Aspekten.
Hier benennen wir einige wichtige Quellen,
die unser Wirken beeinflusst haben.


### Weltklimaberichte

Die IPCC-Berichte sind uns so wichtig, dass wir
[unsere eigene Übersicht](/weltklimaberichte/)
mit Links zu allen aktuellen Berichten pflegen.
Siehe [Weltklimaberichte](/weltklimaberichte/).<br>
Die Sachstandsberichte und Sonderberichte des Weltklimarats (IPCC)
bilden die wissenschaftliche und argumentative Grundlage
unseres Klimaaktivismus.<br>
Das Bundesverfassungbericht schreibt über den IPCC:
> „Der tatsächliche Hintergrund des anthropogenen Klimawandels,
> seine Folgen und die Risiken werden in den Sachstandsberichten
> und Sonderberichten des „Weltklimarats“
> (Zwischenstaatlicher Ausschuss für Klimaänderungen ‒
> Intergovernmental Panel on Climate Change <IPCC>) beschrieben.
> Diese gelten als zuverlässige Zusammenfassungen des aktuellen
> Kenntnisstands zum Klimawandel
> <br>...<br>
> Aufgabe des IPCC ist es, in einer umfassenden und objektiven Weise
> den Stand der wissenschaftlichen Forschung zum Klimawandel darzustellen
> und damit eine Grundlage für wissenschaftsbasierte Entscheidungen zu bieten.
> <br>...<br>
> Das nach wissenschaftlicher Expertise zusammengestellte Autorengremium entscheidet,
> ob die von den Regierungen vorgeschlagenen Umformulierungen richtig sind
> <br>...“

Quelle:
[https://www.bundesverfassungsgericht.de/SharedDocs/Entscheidungen/DE/2021/03/rs20210324_1bvr265618.html](https://www.bundesverfassungsgericht.de/SharedDocs/Entscheidungen/DE/2021/03/rs20210324_1bvr265618.html)


### Klimaschutz 2030: Studie für ein Augsburger Klimaschutzprogramm

Die *Studie für ein Augsburger Klimaschutzprogramm*,
welche nach dem Institut, welches sie durchgeführt hat,
auch oft kurz „KlimaKom-Studie“ genannt wird,
wurde 2020 nach Gründung des Klimacamps von der Stadt in Auftrag gegeben.
Die Ergebnisse wurden im November 2021 veröffentlicht.
Seitdem bilden die Ergebnisse der Studie
ein wichtiges Argument für unsere Forderungen gegenüber der Stadt.
* [Studie als PDF](https://www.augsburg.de/fileadmin/user_upload/umwelt_soziales/umwelt/klima%20und%20energie/Studie_Klimaschutz_2030_mit_allen_anlagen.pdf)
* [Zusammenfassung der Studie als PDF](https://www.augsburg.de/fileadmin/user_upload/umwelt_soziales/umwelt/klima%20und%20energie/21-10-29_Klimastudie_Augsburg_Zusammenfassung.pdf)


### Laudato si’

Die *Laudato si’* ist eine päpstliche Enzyklika
und ein wichtiges Dokument zur allgemeinen Einordnung
von Klimagerechtigkeit.
Man muss nicht katholisch oder religiös sein,
um diesen Text zu schätzen.
Darin beschreibt Papst Franziskus
die Wichtigkeit von sozialer Gerechtigkeit, Umwelt- und Klimaschutz.<br>
Die offizielle deutsche Übersetzung der Laudato si’
kann auf der Webseite des Vatikans gelesen werden:
[https://www.vatican.va/content/francesco/de/encyclicals/documents/papa-francesco_20150524_enciclica-laudato-si.html](https://www.vatican.va/content/francesco/de/encyclicals/documents/papa-francesco_20150524_enciclica-laudato-si.html)<br>
Papst Franziskus sprach auch 2019 in einer Rede
von einem weltweiten Klimanotstand.
Hier in Deutschland weniger bekannt ist Bartholomäus I
der griechisch-orthodoxen Kirche.
Auch er ist ein wichtiger Fürsprecher des Umweltschutzes.<br>
Die große Übereinstimmung zwischen Religionen
und der Klimagerichtigkeitsbewegung ist nicht überraschend.
Während die einen von der Bewahrung der Schöpfung und Nächstenliebe sprechen,
sprechen die anderen von Klimaneutralität und Solidarität.
Meinen tun sie damit alle dasselbe.

### Beschluss des Bundesverfassungsgerichts vom 24. März 2021

Der Beschluss des Bundesverfassungsgerichts ist wegweisend.
Darin heißt es, dass das Grundgesetz den Staat
zur Herstellung von Klimaneutralität verpflichtet
und der Ausstoß von Treibhausgasen in einem Umfang,
der das Weltklima verändert,
die Grundrechte zukünftiger Generationen verletzt.
Der Staat ist dazu verpflichtet,
Voraussetzungen zur verhältnismäßigen Verteilung von Freiheitschancen
über die Generationen zu sichern.
* [Beschluss auf der Webseite des Bundesverfassungsgerichts](https://www.bundesverfassungsgericht.de/SharedDocs/Entscheidungen/DE/2021/03/rs20210324_1bvr265618.html)
* [Beschluss als PDF](https://www.bundesverfassungsgericht.de/SharedDocs/Downloads/DE/2021/03/rs20210324_1bvr265618.pdf?__blob=publicationFile&v=6)

### Ergebnisse des Bürgerrats Klima

Der „Bürgerrat Klima“ wurde 2021
vom Verein BürgerBegehren Klimaschutz e. V. durchgeführt
und steht unter der Schirmherrschaft
von Bundespräsident a. D. Horst Köhler.
160 zufällig ausgeloste Menschen wurden hierbei wissenschaftlich
beraten und dazu eingeladen, Empfehlungen auszuarbeiten,
wie Deutschland unter Berücksichtigung gesellschaftlicher,
wirtschaftlicher und ökologischer Gesichtspunkte
die Ziele des Pariser Klimaschutzabkommens einhalten kann.
Ähnliche Bürgerräte gab es bereits in anderen Ländern,
wie beispielsweise in Frankreich.<br>
Bürgerräte sind ein Mechnismus zur Erarbeitung von Vorschlägen,
die gesellschaftliche Akzeptanz finden und frei von parteipolitischen
Einflussnahmen entstehen.<br>
Weitere Informationen zum Ablauf des Bürgerrats
kann man auf [https://buergerrat-klima.de/](https://buergerrat-klima.de/)
nachlesen.<br>
Die vom „Bürgerrat Klima“ erarbeiteten Empfehlungen können
[hier](https://buergerrat-klima.de/content/pdfs/B%C3%BCrgerrat%20Klima%202021_Das%20B%C3%BCrgergutachten.pdf)
heruntergeladen werden.<br>


## Informationen aus dem Programm des Klimacamps

### Vortrag: *100% Erneuerbare Energie – (Wie) Geht das?*

Der Vortrag „*100% Erneuerbare Energie – (Wie) Geht das?*“
des Energieexperten Dr. Peter Klafka
wurde bereits zwei Mal im Umfeld des Klimacampes gehalten.
Am 16.07.2020 (Tag 16 des Klimacamps) fand der Vortrag
im Zeughaus statt.
Am 22.11.2021 (Tag 510 des Klimacamps) wurde der Vortrag
im Rahmen der [Public Climate School](https://www.klimauni-augsburg.de/)
an der Universität abgehalten.<br>
Link: [Aufzeichnung des Vortrags im Zeughaus](https://www.youtube.com/watch?v=AxjU6MzrbS8)

### Vortrag: *Einfluss des Klimawandels auf Flora, Fauna und Lebensräume im Allgäu*

Der Biologe Dr. Michael Schneider hielt am 18.08.2020
(Tag 49) am Klimacamp einen Vortrag mit dem Titel
„*Einfluss des Klimawandels auf Flora, Fauna und Lebensräume im Allgäu*“.
Wir haben keine Aufzeichnung des Vortrags,
allerdings sind viele Inhalte des Vortrags von ihm
auch frei zugänglich im Internet veröffentlicht worden.

* Webseite: [Einfluss des Klimawandels auf Flora, Fauna und Lebensräume in den Landkreisen Ober-, Ost- und Unterallgäu](http://odsfm.com/klimawandel)
* PDF: [Einfluss des Klimawandels auf Flora, Fauna und Lebensräume in den Landkreisen Ober-, Ost- und Unterallgäu (Bayern, Deutschland)](http://odsfm.com/wp-content/uploads/2019/10/Einfluss-des-Klimawandels-auf-Flora-Fauna-und-Lebensr%C3%A4ume-in-den-Landkreisen-Ober-Ost-und-Unterallg%C3%A4u.pdf)
