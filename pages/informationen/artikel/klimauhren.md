---
layout: page
title: Klimauhren – Warum gibt es verschiedene?
permalink: /informationen/artikel/klimauhren/
nav_exclude: true
---

<link href="/style/carbon_clock_table_style.css" rel="stylesheet" type="text/css">

# Klimauhren

<div style="background-color: #FF2020; text-align:center; font-size: 2em">
  <p>
    <div id="main-clock">Juli 2029</div>
  </p>
</div>

Was ist eine Klimauhr?
Wie berechnet sich ihr Wert?

Zunächst mal muss man wissen,
dass es verschiedene Klimauhren gibt.
Jeder Klimauhr liegen zwei Werte zu Grunde,

* eine **Temperaturgrenze**, die es nicht zu überschreiten gilt, und
* eine **Wahrscheinlichkeit**,
  mit der die Temperaturgrenze nicht überschritten werden soll.

Mit der Temperaturgrenze und Wahrscheinlichkeit
kann man der wissenschaftlichen Literatur ein Restbudget
an Treibhausgasen entnehmen.
Das Restbudget wird anhand der Erkenntnisse,
die die Wissenschaft über das Klimasystem gewonnen hat,
geschätzt.
Sie sind mit einer gewissen Unsicherheit verbunden.
Unsere Klimauhren arbeiten mit den Restbudgets,
die im sechsten Sachstandsbericht des IPCC
von Arbeitsgruppe I (AR6-WGI) ermittelt wurden.
Für Details siehe unsere [Übersicht](/weltklimaberichte/)
über die Weltklimaberichte.

Die Restbudgets in AR6-WGI gelten ab dem 01.01.2020.
Also ziehen wir von den Budgets die Emissionen an Treibhausgasen ab,
die in den Jahren 2020 und 2021 ausgestoßen worden waren.
Dann berechnen wir,
wie lange sich ein Ausstoß von der Höhe von 2021 fortsetzen kann,
bis das gesamte verbliebene Restbudget aufgebraucht ist.

Bei unseren Berechnungen
gehen wir von einem Ausstoß von 39 Gigatonnen CO₂-Äquivalent
für 2020 aus und 42,2 Gigatonnen CO₂-Äquivalent für 2021
und die nachfolgenden Jahre aus.
Die Quellenlage ist etwas schwierig.
Manchmal geht nicht klar hervor,
ob sich eine Emissionsmenge
ausschließlich auf die CO₂-Emissionen beschränkt
oder auf die Emissionen aller Treibhausgase
in CO₂-Äquivalenten als Maßeinheit bezieht.

## Restbudgets nach AR6-WGI und die daraus abgeleiteten Klimauhren

### Die Restbudgets nach AR6-WGI

<p>
  <div>
    Restbudgets für gewisse Temperaturgrenzen
    und Wahrscheinlichkeiten für die Nichtüberschreitung.
    Die Werte sind Table 5.8 des Berichts der ersten Arbeitsgruppe (WGI)
    des sechsten Sachstandsberichts (AR6)
    „Sixth Assessment Report (AR6) – The Physical Science Basis“
    entnommen.
    Die Restbudgets gelten ab dem 01.01.2020.
  </div>
  <table>
    <thead>
      <tr>
        <th style="width:5%">
          Erwärmung¹
        </th>
        <th colspan="10">
          Wahrscheinlichkeit diese Grenze nicht zu überschreiten
        </th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1,07 °C</td>
        <td colspan="10" style="text-align: center; color: #800000">
          bereits überschritten: Durchschnitt des Jahrzehnts 2010 - 2019 (laut AR6 WGI)
          <br>
          <div style="color:#FF0000; font-style:italic;">Die Geschwindigkeit der Erwärmung wurde auf 0,23 °C pro Jahrzehnt geschätzt.</div>
        </td>
      </tr>
      <tr>
        <td>1,09 °C</td>
        <td colspan="10" style="text-align: center; color:#800000">
          bereits überschritten: Durchschnitt des Jahrzehnts 2011 - 2020 (laut AR6 WGII)
        </td>
      </tr>
      <tr>
        <td>1,2 °C</td>
        <td colspan="10" style="color:#800000; text-align: center">
          wahrscheinlich im Jahr 2021 bereits überschritten
        </td>
      </tr>
    </tbody>
    <thead>
      <tr>
        <th style="border-top: none">
          Erwärmung¹
        </th>
        <th colspan="2" style="border-top: none">17%</th>
        <th colspan="2" style="border-top: none">33%</th>
        <th colspan="2" style="border-top: none">50%</th>
        <th colspan="2" style="border-top: none">67%</th>
        <th colspan="2" style="border-top: none">83%</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1,3 °C</td>
        <td colspan="2" style="color:#C04000">400 Gt</td>
        <td colspan="2" style="color:#C00000">250 Gt</td>
        <td colspan="2" style="color:#C00000">150 Gt</td>
        <td colspan="2" style="color:#800000">100 Gt</td>
        <td colspan="2" style="color:#400000">50 Gt</td>
      </tr>
      <tr>
        <td>1,4 °C</td>
        <td colspan="2" style="color:#C08000">650 Gt</td>
        <td colspan="2" style="color:#C04000">450 Gt</td>
        <td colspan="2" style="color:#C04000">350 Gt</td>
        <td colspan="2" style="color:#C00000">250 Gt</td>
        <td colspan="2" style="color:#C00000">200 Gt</td>
      </tr>
      <tr>
        <td>1,5 °C</td>
        <td colspan="2" style="color:#C08000">900 Gt</td>
        <td colspan="2" style="color:#C08000">650 Gt</td>
        <td colspan="2" style="color:#C04000">500 Gt</td>
        <td colspan="2" style="color:#C04000">400 Gt</td>
        <td colspan="2" style="color:#C04000">300 Gt</td>
      </tr>
      <tr>
        <td>1,6 °C</td>
        <td colspan="2" style="color:#808000">1200 Gt</td>
        <td colspan="2" style="color:#C08000">850 Gt</td>
        <td colspan="2" style="color:#C08000">650 Gt</td>
        <td colspan="2" style="color:#C08000">550 Gt</td>
        <td colspan="2" style="color:#C04000">400 Gt</td>
      </tr>
      <tr>
        <td>1,7 °C</td>
        <td colspan="2" style="color:#404000">1450 Gt</td>
        <td colspan="2" style="color:#808000">1050 Gt</td>
        <td colspan="2" style="color:#C08000">850 Gt</td>
        <td colspan="2" style="color:#C08000">700 Gt</td>
        <td colspan="2" style="color:#C08000">550 Gt</td>
      </tr>
      <tr>
        <td>1,8 °C</td>
        <td colspan="2" style="color:#404000">1750 Gt</td>
        <td colspan="2" style="color:#808000">1250 Gt</td>
        <td colspan="2" style="color:#808000">1000 Gt</td>
        <td colspan="2" style="color:#C08000">850 Gt</td>
        <td colspan="2" style="color:#C08000">650 Gt</td>
      </tr>
      <tr>
        <td>1,9 °C</td>
        <td colspan="2" style="color:#404000">2000 Gt</td>
        <td colspan="2" style="color:#404000">1450 Gt</td>
        <td colspan="2" style="color:#808000">1200 Gt</td>
        <td colspan="2" style="color:#808000">1000 Gt</td>
        <td colspan="2" style="color:#C08000">800 Gt</td>
      </tr>
      <tr>
        <td>2,0 °C</td>
        <td colspan="2" style="color:#404000">2300 Gt</td>
        <td colspan="2" style="color:#404000">1700 Gt</td>
        <td colspan="2" style="color:#808000">1350 Gt</td>
        <td colspan="2" style="color:#808000">1150 Gt</td>
        <td colspan="2" style="color:#C08000">900 Gt</td>
      </tr>
      <tr>
        <td>2,1 °C</td>
        <td colspan="2" style="color:#404000">2550 Gt</td>
        <td colspan="2" style="color:#404000">1900 Gt</td>
        <td colspan="2" style="color:#404000">1500 Gt</td>
        <td colspan="2" style="color:#808000">1250 Gt</td>
        <td colspan="2" style="color:#808000">1050 Gt</td>
      </tr>
      <tr>
        <td>2,2 °C</td>
        <td colspan="2" style="color:#404000">2850 Gt</td>
        <td colspan="2" style="color:#404000">2100 Gt</td>
        <td colspan="2" style="color:#404000">1700 Gt</td>
        <td colspan="2" style="color:#404000">1400 Gt</td>
        <td colspan="2" style="color:#808000">1150 Gt</td>
      </tr>
      <tr>
        <td>2,3 °C</td>
        <td colspan="2" style="color:#404000">3100 Gt</td>
        <td colspan="2" style="color:#404000">2300 Gt</td>
        <td colspan="2" style="color:#404000">1850 Gt</td>
        <td colspan="2" style="color:#404000">1550 Gt</td>
        <td colspan="2" style="color:#808000">1250 Gt</td>
      </tr>
      <tr>
        <td>2,4 °C</td>
        <td colspan="2" style="color:#404000">3350 Gt</td>
        <td colspan="2" style="color:#404000">2500 Gt</td>
        <td colspan="2" style="color:#404000">2050 Gt</td>
        <td colspan="2" style="color:#404000">1700 Gt</td>
        <td colspan="2" style="color:#404000">1400 Gt</td>
      </tr>
    </tbody>
  </table>
  <sub>
    1 Alle Angaben zur Aufheizung beziehen sich auf die Aufheizung
    relativ zur Durchschnittstemperatur der Jahre 1850-1900.
  </sub>
  <div style="font-style: italic">
    Erläuterung der Tabelle:
    Die Zeilen der Tabelle geben verschiedene Temperaturgrenzen an.
    Die verschiedenen Spalten stehen
    für verschiedene Wahrscheinlichkeiten
    zur Einhaltung der Temperaturgrenze.
    Gegeben eine Zeile und eine Spalte,
    kann man der Tabelle entnehmen,
    welche Treibhausgasemissionen ab dem 01. Januar 2020
    nicht überschritten werden dürfen.
  </div>
</p>


### Geschätzter Zeitpunkt, zu dem die Restbudgets bei gleichbleibendem Ausstoß verbraucht sind

<p>
  <div>
    Zeitpunkte, zu denen die Restbudgets für gewisse Temperaturgrenzen
    und Wahrscheinlichkeiten bei Beibehaltung
    der aktuellen Treibhausgasemissionen verbraucht werden/wurden, sind:
  </div>
  <table>
    <thead>
      <tr>
        <th style="width:5%">
          Erwärmung¹
        </th>
        <th colspan="10">
          Wahrscheinlichkeit diese Grenze nicht zu überschreiten
        </th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1,07 °C</td>
        <td colspan="10" style="text-align: center; color: #800000">
          bereits überschritten: Durchschnitt des Jahrzehnts 2010 - 2019 (laut AR6 WGI)
          <br>
          <div style="color:#FF0000; font-style:italic;">Die Geschwindigkeit der Erwärmung wurde auf 0,23 °C pro Jahrzehnt geschätzt.</div>
        </td>
      </tr>
      <tr>
        <td>1,09 °C</td>
        <td colspan="10" style="text-align: center; color:#800000">
          bereits überschritten: Durchschnitt des Jahrzehnts 2011 - 2020 (laut AR6 WGII)
        </td>
      </tr>
      <tr>
        <td>1,2 °C</td>
        <td colspan="10" style="color:#800000; text-align: center">
          wahrscheinlich im Jahr 2021 bereits überschritten
        </td>
      </tr>
    </tbody>
    <thead>
      <tr>
        <th style="border-top: none">
          Erwärmung¹
        </th>
        <th colspan="2" style="border-top: none">17%</th>
        <th colspan="2" style="border-top: none">33%</th>
        <th colspan="2" style="border-top: none">50%</th>
        <th colspan="2" style="border-top: none">67%</th>
        <th colspan="2" style="border-top: none">83%</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1,3 °C</td>
        <td colspan="2" style="color:#C04000">Juli 2029</td>
        <td colspan="2" style="color:#C00000">Dezember 2025</td>
        <td colspan="2" style="color:#C00000">August 2023</td>
        <td colspan="2" style="color:#800000">Juni 2022</td>
        <td colspan="2" style="color:#400000">April 2021</td>
      </tr>
      <tr>
        <td>1,4 °C</td>
        <td colspan="2" style="color:#C08000">Juni 2035</td>
        <td colspan="2" style="color:#C04000">September 2030</td>
        <td colspan="2" style="color:#C04000">Mai 2028</td>
        <td colspan="2" style="color:#C00000">Dezember 2025</td>
        <td colspan="2" style="color:#C00000">Oktober 2024</td>
      </tr>
      <tr>
        <td>1,5 °C</td>
        <td colspan="2" style="color:#C08000">Mai 2041</td>
        <td colspan="2" style="color:#C08000">Juni 2035</td>
        <td colspan="2" style="color:#C04000">Dezember 2031</td>
        <td colspan="2" style="color:#C04000">Juli 2029</td>
        <td colspan="2" style="color:#C04000">März 2027</td>
      </tr>
      <tr>
        <td>1,6 °C</td>
        <td colspan="2" style="color:#808000">Juni 2048</td>
        <td colspan="2" style="color:#C08000">März 2040</td>
        <td colspan="2" style="color:#C08000">Juni 2035</td>
        <td colspan="2" style="color:#C08000">Februar 2033</td>
        <td colspan="2" style="color:#C04000">Juli 2029</td>
      </tr>
      <tr>
        <td>1,7 °C</td>
        <td colspan="2" style="color:#404000">Juni 2054</td>
        <td colspan="2" style="color:#808000">Dezember 2044</td>
        <td colspan="2" style="color:#C08000">März 2040</td>
        <td colspan="2" style="color:#C08000">August 2036</td>
        <td colspan="2" style="color:#C08000">Februar 2033</td>
      </tr>
      <tr>
        <td>1,8 °C</td>
        <td colspan="2" style="color:#404000">Juli 2061</td>
        <td colspan="2" style="color:#808000">September 2049</td>
        <td colspan="2" style="color:#808000">Oktober 2043</td>
        <td colspan="2" style="color:#C08000">März 2040</td>
        <td colspan="2" style="color:#C08000">Juni 2035</td>
      </tr>
      <tr>
        <td>1,9 °C</td>
        <td colspan="2" style="color:#404000">Juni 2067</td>
        <td colspan="2" style="color:#404000">Juni 2054</td>
        <td colspan="2" style="color:#808000">Juni 2048</td>
        <td colspan="2" style="color:#808000">Oktober 2043</td>
        <td colspan="2" style="color:#C08000">Januar 2039</td>
      </tr>
      <tr>
        <td>2,0 °C</td>
        <td colspan="2" style="color:#404000">Juli 2074</td>
        <td colspan="2" style="color:#404000">Mai 2060</td>
        <td colspan="2" style="color:#808000">Januar 2052</td>
        <td colspan="2" style="color:#808000">April 2047</td>
        <td colspan="2" style="color:#C08000">Mai 2041</td>
      </tr>
      <tr>
        <td>2,1 °C</td>
        <td colspan="2" style="color:#404000">Juni 2080</td>
        <td colspan="2" style="color:#404000">Januar 2065</td>
        <td colspan="2" style="color:#404000">August 2055</td>
        <td colspan="2" style="color:#808000">September 2049</td>
        <td colspan="2" style="color:#808000">Dezember 2044</td>
      </tr>
      <tr>
        <td>2,2 °C</td>
        <td colspan="2" style="color:#404000">Juli 2087</td>
        <td colspan="2" style="color:#404000">Oktober 2069</td>
        <td colspan="2" style="color:#404000">Mai 2060</td>
        <td colspan="2" style="color:#404000">März 2053</td>
        <td colspan="2" style="color:#808000">April 2047</td>
      </tr>
      <tr>
        <td>2,3 °C</td>
        <td colspan="2" style="color:#404000">Juni 2093</td>
        <td colspan="2" style="color:#404000">Juli 2074</td>
        <td colspan="2" style="color:#404000">November 2063</td>
        <td colspan="2" style="color:#404000">Oktober 2056</td>
        <td colspan="2" style="color:#808000">September 2049</td>
      </tr>
      <tr>
        <td>2,4 °C</td>
        <td colspan="2" style="color:#404000">Mai 2099</td>
        <td colspan="2" style="color:#404000">April 2079</td>
        <td colspan="2" style="color:#404000">August 2068</td>
        <td colspan="2" style="color:#404000">Mai 2060</td>
        <td colspan="2" style="color:#404000">März 2053</td>
      </tr>
    </tbody>
  </table>
  <sub>
    1 Alle Angaben zur Aufheizung beziehen sich auf die Aufheizung
    relativ zur Durchschnittstemperatur der Jahre 1850-1900.
  </sub>

</p>

### Die Uhren

<p>
  <div>
    Zeit die verbleibt,
    bis die Restbudgets für gewisse Temperaturgrenzen
    und Wahrscheinlichkeiten bei Beibehaltung
    der aktuellen Treibhausgasemissionen verbraucht sind
  </div>
  <noscript>
    <div style="background-color: #FFFF20; font-color: #FF0000; padding: 1em; text-align:center">
      Die nachfolgende Tabelle benötigt JavaScript,
      um die korrekten Zeiträume anzuzeigen.
      Andernfalls wird wie oben auch lediglich ein Datum angezeigt.
    </div>
  </noscript>
  <table>
    <thead>
      <tr>
        <th style="width:5%">
          Erwärmung¹
        </th>
        <th colspan="10">
          Wahrscheinlichkeit diese Grenze nicht zu überschreiten
        </th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1,07 °C</td>
        <td colspan="10" style="text-align: center; color: #800000">
          bereits überschritten: Durchschnitt des Jahrzehnts 2010 - 2019 (laut AR6 WGI)
          <br>
          <div style="color:#FF0000; font-style:italic;">Die Geschwindigkeit der Erwärmung wurde auf 0,23 °C pro Jahrzehnt geschätzt.</div>
        </td>
      </tr>
      <tr>
        <td>1,09 °C</td>
        <td colspan="10" style="text-align: center; color:#800000">
          bereits überschritten: Durchschnitt des Jahrzehnts 2011 - 2020 (laut AR6 WGII)
        </td>
      </tr>
      <tr>
        <td>1,2 °C</td>
        <td colspan="10" style="color:#800000; text-align: center">
          wahrscheinlich im Jahr 2021 bereits überschritten
        </td>
      </tr>
    </tbody>
    <thead>
      <tr>
        <th style="border-top: none">
          Erwärmung¹
        </th>
        <th colspan="2" style="border-top: none">17%</th>
        <th colspan="2" style="border-top: none">33%</th>
        <th colspan="2" style="border-top: none">50%</th>
        <th colspan="2" style="border-top: none">67%</th>
        <th colspan="2" style="border-top: none">83%</th>
      </tr>
    </thead>
    <tbody>
      <tr id="1.3">
        <td>1,3 °C</td>
        <td colspan="2" style="color:#C04000">Juli 2029</td>
        <td colspan="2" style="color:#C00000">Dezember 2025</td>
        <td colspan="2" style="color:#C00000">August 2023</td>
        <td colspan="2" style="color:#800000">Juni 2022</td>
        <td colspan="2" style="color:#400000">abgelaufen</td>
      </tr>
      <tr id="1.4">
        <td>1,4 °C</td>
        <td colspan="2" style="color:#C08000">Juni 2035</td>
        <td colspan="2" style="color:#C04000">September 2030</td>
        <td colspan="2" style="color:#C04000">Mai 2028</td>
        <td colspan="2" style="color:#C00000">Dezember 2025</td>
        <td colspan="2" style="color:#C00000">Oktober 2024</td>
      </tr>
      <tr id="1.5">
        <td>1,5 °C</td>
        <td colspan="2" style="color:#C08000">Mai 2041</td>
        <td colspan="2" style="color:#C08000">Juni 2035</td>
        <td colspan="2" style="color:#C04000">Dezember 2031</td>
        <td colspan="2" style="color:#C04000">Juli 2029</td>
        <td colspan="2" style="color:#C04000">März 2027</td>
      </tr>
      <tr id="1.6">
        <td>1,6 °C</td>
        <td colspan="2" style="color:#808000">Juni 2048</td>
        <td colspan="2" style="color:#C08000">März 2040</td>
        <td colspan="2" style="color:#C08000">Juni 2035</td>
        <td colspan="2" style="color:#C08000">Februar 2033</td>
        <td colspan="2" style="color:#C04000">Juli 2029</td>
      </tr>
      <tr id="1.7">
        <td>1,7 °C</td>
        <td colspan="2" style="color:#404000">Juni 2054</td>
        <td colspan="2" style="color:#808000">Dezember 2044</td>
        <td colspan="2" style="color:#C08000">März 2040</td>
        <td colspan="2" style="color:#C08000">August 2036</td>
        <td colspan="2" style="color:#C08000">Februar 2033</td>
      </tr>
      <tr id="1.8">
        <td>1,8 °C</td>
        <td colspan="2" style="color:#404000">Juli 2061</td>
        <td colspan="2" style="color:#808000">September 2049</td>
        <td colspan="2" style="color:#808000">Oktober 2043</td>
        <td colspan="2" style="color:#C08000">März 2040</td>
        <td colspan="2" style="color:#C08000">Juni 2035</td>
      </tr>
      <tr id="1.9">
        <td>1,9 °C</td>
        <td colspan="2" style="color:#404000">Juni 2067</td>
        <td colspan="2" style="color:#404000">Juni 2054</td>
        <td colspan="2" style="color:#808000">Juni 2048</td>
        <td colspan="2" style="color:#808000">Oktober 2043</td>
        <td colspan="2" style="color:#C08000">Januar 2039</td>
      </tr>
      <tr id="2.0">
        <td>2,0 °C</td>
        <td colspan="2" style="color:#404000">Juli 2074</td>
        <td colspan="2" style="color:#404000">Mai 2060</td>
        <td colspan="2" style="color:#808000">Januar 2052</td>
        <td colspan="2" style="color:#808000">April 2047</td>
        <td colspan="2" style="color:#C08000">Mai 2041</td>
      </tr>
      <tr id="2.1">
        <td>2,1 °C</td>
        <td colspan="2" style="color:#404000">Juni 2080</td>
        <td colspan="2" style="color:#404000">Januar 2065</td>
        <td colspan="2" style="color:#404000">August 2055</td>
        <td colspan="2" style="color:#808000">September 2049</td>
        <td colspan="2" style="color:#808000">Dezember 2044</td>
      </tr>
      <tr id="2.2">
        <td>2,2 °C</td>
        <td colspan="2" style="color:#404000">Juli 2087</td>
        <td colspan="2" style="color:#404000">Oktober 2069</td>
        <td colspan="2" style="color:#404000">Mai 2060</td>
        <td colspan="2" style="color:#404000">März 2053</td>
        <td colspan="2" style="color:#808000">April 2047</td>
      </tr>
      <tr id="2.3">
        <td>2,3 °C</td>
        <td colspan="2" style="color:#404000">Juni 2093</td>
        <td colspan="2" style="color:#404000">Juli 2074</td>
        <td colspan="2" style="color:#404000">November 2063</td>
        <td colspan="2" style="color:#404000">Oktober 2056</td>
        <td colspan="2" style="color:#808000">September 2049</td>
      </tr>
      <tr id="2.4">
        <td>2,4 °C</td>
        <td colspan="2" style="color:#404000">Mai 2099</td>
        <td colspan="2" style="color:#404000">April 2079</td>
        <td colspan="2" style="color:#404000">August 2068</td>
        <td colspan="2" style="color:#404000">Mai 2060</td>
        <td colspan="2" style="color:#404000">März 2053</td>
      </tr>
    </tbody>
  </table>
  <sub>
    1 Alle Angaben zur Aufheizung beziehen sich auf die Aufheizung
    relativ zur Durchschnittstemperatur der Jahre 1850-1900.
  </sub>
</p>

<script>
const milliSecondsPerMinute = 60 * 1000;
const milliSecondsPerHour = 60 * milliSecondsPerMinute;
const milliSecondsPerDay = 24 * milliSecondsPerHour;
const milliSecondsPerYear = 365 * milliSecondsPerDay;

const budgets = [
 { key: "1.3", label_de: "1,3 °C", label_en: "1.3 °C", budgets: [400, 250, 150, 100, 50]},
 { key: "1.4", label_de: "1,4 °C", label_en: "1.4 °C", budgets: [650, 450, 350, 250, 200]},
 { key: "1.5", label_de: "1,5 °C", label_en: "1.5 °C", budgets: [900, 650, 500, 400, 300]},
 { key: "1.6", label_de: "1,6 °C", label_en: "1.6 °C", budgets: [1200, 850, 650, 550, 400]},
 { key: "1.7", label_de: "1,7 °C", label_en: "1.7 °C", budgets: [1450, 1050, 850, 700, 550]},
 { key: "1.8", label_de: "1,8 °C", label_en: "1.8 °C", budgets: [1750, 1250, 1000, 850, 650]},
 { key: "1.9", label_de: "1,9 °C", label_en: "1.9 °C", budgets: [2000, 1450, 1200, 1000, 800]},
 { key: "2.0", label_de: "2,0 °C", label_en: "2.0 °C", budgets: [2300, 1700, 1350, 1150, 900]},
 { key: "2.1", label_de: "2,1 °C", label_en: "2.1 °C", budgets: [2550, 1900, 1500, 1250, 1050]},
 { key: "2.2", label_de: "2,2 °C", label_en: "2.2 °C", budgets: [2850, 2100, 1700, 1400, 1150]},
 { key: "2.3", label_de: "2,3 °C", label_en: "2.3 °C", budgets: [3100, 2300, 1850, 1550, 1250]},
 { key: "2.4", label_de: "2,4 °C", label_en: "2.4 °C", budgets: [3350, 2500, 2050, 1700, 1400]}
];

function millisecondsToString(milliseconds) {
  if (milliseconds < 0) {
    return [{ content: "abgelaufen", span: "2", borderLeft: "solid", borderRight: "solid", color: "#400000", alignment: "center" }];
  }
  const years = Math.floor(milliseconds / milliSecondsPerYear);
  milliseconds = milliseconds - years * milliSecondsPerYear;
  const days = Math.floor(milliseconds / milliSecondsPerDay);
  let textColor = "#C00000";
  if (years != 0) {
    if (years < 5) {
      textColor = "#C00000";
    } else if (years < 10) {
      textColor = "#C04000";
    } else if (years < 20) {
      textColor = "#C08000";
    } else if (years < 30) {
      textColor = "#808000";
    } else if (years < 80) {
      textColor = "#404000";
    }

    let resultingText = years + ((years == 1) ? " Jahr " : " Jahre ") +  days + ((days == 1) ? " Tag" : " Tage");

    return [ { color: textColor, alignment: "center", borderLeft: "solid", borderRight: "solid", content: resultingText, span: "2" } ];
  }

  milliseconds = milliseconds - days * milliSecondsPerDay;
  const hours = Math.floor(milliseconds / milliSecondsPerHour);
  let resultingText = days + ((days == 1) ? " Tag " : " Tage ") +  hours + ((hours == 1) ? " Stunde" : " Stunden");
  return [ { color: "#800000", alignment: "center", borderLeft: "solid", borderRight: "solid", content: resultingText, span: "2" } ];
}

function calculateRemainingTime(budget, lossPerMillisecond) {
  const timestamp20200101 = 1577836800000;
  const length2020InMilliseconds = 31622400000;
  const budgetLoss2020 = 39000000000;
  const start = new Date(timestamp20200101 + length2020InMilliseconds);
  const now = new Date();
  const goneBy = now - start;
  const timeWasLeft = (budget - budgetLoss2020) / lossPerMillisecond;
  const timeIsLeft = timeWasLeft - goneBy;
  return millisecondsToString(timeIsLeft);
}

function computeTable(budgets, lossPerMillisecond) {
  const table = document.getElementById("table");
  budgets.forEach( limit => {
    let row = document.getElementById(limit.key);
    if (row == null) {
      row = table.insertRow();
    } else {
      row.innerHTML = null;
    }
    row.insertCell().innerHTML = limit.label_de;
    limit.budgets.forEach( budget => {
      const response = calculateRemainingTime(1000000000 * budget, lossPerMillisecond);
      response.forEach( part => {
        var cell = row.insertCell();
        cell.colSpan = part.span;
        cell.style.borderLeft = part.borderLeft;
        cell.style.borderRight = part.borderRight;
        cell.style.color = part.color;
        cell.style.textAlign = part.alignment;
        cell.innerHTML = part.content;
      });
    });
  });
}

function computeMainClock(budget, lossPerMillisecond) {
  const response = calculateRemainingTime(1000000000 * budget, lossPerMillisecond);
  const mainClock = document.getElementById("main-clock");
  mainClock.innerHTML = response[0].content;
}

const budgetLoss2021 = 42200000000.0;
const lossPerMillisecond = budgetLoss2021 / milliSecondsPerYear;
computeTable(budgets, lossPerMillisecond);
computeMainClock(budgets[2].budgets[3], lossPerMillisecond);
</script>
