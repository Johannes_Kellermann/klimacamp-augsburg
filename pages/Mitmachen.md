---
layout: page
title: Mitmachen
permalink: /mitmachen/
has_children: true
nav_order: 90
---

# Vorbeikommen & mitmachen

## Adresse

Wenn du Lust hast vorbeizukommen, ob für ein kurzes Gespräch oder mehrere Tage
-- du findest uns direkt neben dem Augsburger Rathaus (Adresse: "Am Fischmarkt").


## Du bist willkommen!

Zum Klimacamp (und unseren digitalen Strukturen) kommen ständig neue Menschen
hinzu, die sich für Klimagerechtigkeit engagieren möchten. Du kannst dich
laufenden Projekten anschließen, neue vorantreiben (wir haben
mittlerweile viel Know-how angesammelt, um dich dabei möglichst gut
unterstützen zu können) oder auch einfach Zeit im Camp verbringen, um uns
kennenzulernen und dich in die lange Reihe von Menschen, die das Camp seit
Gründung stabil hielten, einzureihen. :-)

Wir sind eine öffentliche Versammlung. Es gibt keine Mitgliederliste oder
Verpflichtungen. Engagiere dich so, wie du Zeit hast, und nimm dich zurück,
wenn du dich auf andere Lebensbereiche konzentrieren möchtest oder musstest.

WLAN und Strom zum Arbeiten sind vorhanden.


## Schreib uns oder ruf an

Wenn du weitere Infos möchtest, schreib uns auf Insta (@klimacamp) oder
Telegram oder ruf an :-) [+49 176 95110311](tel:+4917695110311)
