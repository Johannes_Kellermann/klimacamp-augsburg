---
layout: page
title: "IPCC Reports"
permalink: /en/ipcc-reports/
parent: "English"
has_children: false
has_toc: false
nav_order: 20
---

# IPCC Reports

Since its foundation
the Intergovernmental Panel on Climate Change (IPCC)
has been working on summarizing in its reports
the current scientific knowledge on the world's climate
and the factors that influence it.
These reports are freely available.


## Download links to the most recent world climate reports

(Latest update: May 2022)

| Report       | Year | Summary for Policymakers | Technical Summary | Full Version |
|:-------------|------|:-------------------------|:------------------|:-------------|
| AR5          | 2014 | [AR5-SPM-EN](https://archive.ipcc.ch/pdf/assessment-report/ar5/syr/AR5_SYR_FINAL_SPM.pdf) | --- | [AR5-FULL-EN](https://archive.ipcc.ch/pdf/assessment-report/ar5/syr/SYR_AR5_FINAL_full_wcover.pdf) |
| SR15         | 2018 | [SR15-SPM-EN](https://www.ipcc.ch/site/assets/uploads/sites/2/2019/05/SR15_SPM_version_report_LR.pdf) | [SR15-TS-EN](https://www.ipcc.ch/site/assets/uploads/sites/2/2019/05/SR15_TS_High_Res.pdf) | [SR15-FULL-EN](https://www.ipcc.ch/site/assets/uploads/sites/2/2019/06/SR15_Full_Report_High_Res.pdf) |
| SRCCL        | 2019 | [SRCCL-SPM-EN](https://www.ipcc.ch/site/assets/uploads/sites/4/2020/02/SPM_Updated-Jan20.pdf) | [SRCCL-TS-EN](https://www.ipcc.ch/site/assets/uploads/sites/4/2020/07/03_Technical-Summary-TS_V2.pdf) | [SRCCL-FULL-EN](https://www.ipcc.ch/site/assets/uploads/sites/4/2021/07/210714-IPCCJ7230-SRCCL-Complete-BOOK-HRES.pdf) |
| SROCC        | 2019 | [SROCC-SPM-EN](https://www.ipcc.ch/site/assets/uploads/sites/3/2019/11/03_SROCC_SPM_FINAL.pdf) | [SROCC-TS-EN](https://www.ipcc.ch/site/assets/uploads/sites/3/2019/11/04_SROCC_TS_FINAL.pdf) | [SROCC-FULL-EN](https://www.ipcc.ch/site/assets/uploads/sites/3/2019/12/SROCC_FullReport_FINAL.pdf) |
| AR6 – Part 1 | 2021 | [AR6-WGI-SPM-EN](https://www.ipcc.ch/report/ar6/wg1/downloads/report/IPCC_AR6_WGI_SPM_final.pdf) | [AR6-WGI-TS-EN](https://www.ipcc.ch/report/ar6/wg1/downloads/report/IPCC_AR6_WGI_TS.pdf) | [AR6-WGI-FULL-EN¹](https://www.ipcc.ch/report/ar6/wg1/downloads/report/IPCC_AR6_WGI_Full_Report.pdf) |
| AR6 – Part 2 | 2022 | [AR6-WGII-SPM-EN](https://report.ipcc.ch/ar6wg2/pdf/IPCC_AR6_WGII_SummaryForPolicymakers.pdf) | [AR6-WGII-TS-EN¹](https://report.ipcc.ch/ar6wg2/pdf/IPCC_AR6_WGII_FinalDraft_TechnicalSummary.pdf) | [AR6-WGII-FULL-EN¹](https://report.ipcc.ch/ar6wg2/pdf/IPCC_AR6_WGII_FinalDraft_FullReport.pdf) |
| AR6 – Part 3 | 2022 | [AR6-WGIII-SPM-EN](https://report.ipcc.ch/ar6wg3/pdf/IPCC_AR6_WGIII_SummaryForPolicymakers.pdf) | [AR6-WGIII-TS-EN¹](https://report.ipcc.ch/ar6wg3/pdf/IPCC_AR6_WGIII_FinalDraft_TechnicalSummary.pdf) | [AR6-WGIII-FULL-EN¹](https://report.ipcc.ch/ar6wg3/pdf/IPCC_AR6_WGIII_FinalDraft_FullReport.pdf) |

<sup>1 Final editing in progress.
It is possible that the reports are subject to minor changes,
which can result in pictures or chapters
being shifted to other pages.</sup>

This tabular overview should allow fast access
to the most recent world climate reports.
<br>
The links refer to the official downloads
of the [IPCC's website](https://www.ipcc.ch/).

We want to base current discussions objectively on scientific facts.
<br>
We hope that every relevant politician
has read at least the "Summary for Policymakers"
of the current reports.
Conversations with political advisers and information from the media
can not fully replace personally reading the reports or their summaries.
Every media performs a selection of information
and focuses only on some individual aspects.
To get a mostly complete picture of the information
one has to read the reports or their summaries by one's selves.


## IPCC Reports / Topics

### AR5 – Fifths Assessment Report
The report summarized the current state of knowledge at its time.

### SR15 – Special Report on Global Warming of 1.5 °C
The report presents the more drastic consequences
of a global warming by 2°C
in comparison to a warming of 1.5°C.
In addition it specifies what has to be done
to limit the warming to 1.5°C.

### SRCCL – Special Report on Climate Change and Land
The report focuses on the effects of global warming
for land masses and their ecosystem.

### SROCC – Special Report on the Ocean and Cryosphere in a Changing Climate
The report focuses on the effects of global warming
on the oceans and cryosphere.

### AR6 – Sixths Assessment Report
The report is the latest summary of what is known
about the world's climate and incorporates
the information of the special reports, which were published
since the publication of the Fifth Assessment Report.
The report consists of three parts that were published in 2021 and 2022.

* Part 1 (WGI) summarizes the physical basics of climate change.
  The report contains estimates for the carbon budgets
  for different rates of warming and different propabilities.<br>
  Part 1 was published in 2021.
* Part 2 (WGII) describes the impact, effects and necessary adaptations
  of various degress of warming.
  Parts of the report are also describing results of warming
  that are today already affecting the lifes of many people.<br>
  Part 2 was published on the 28th of February 2022.
* Part 3 (WGIII) describes measures to reduce the rate of warming.<br>
  Part 2 was published on the 4th of April 2022.<br>
  In 2021 a preliminary version of Part 3 was leaked
  on [https://scientistrebellion.com](https://scientistrebellion.com/we-leaked-the-upcoming-ipcc-report/)
  because scientists feared
  that it might be watered down by politicians.
  Spoiler: It was.²


## Structure of the reports

Typically the reports consist of multiple parts

 * The **Summary for Policymakers (SPM)**
   is a short summary of the report
   that is illustrated with many pictures and graphics.
   Usually it consists only of a few dozen pages.<br>
   The SPMs are to some degree subject to political influence.²
   Scientists do have a right to veto and can deny misrepresentations,
   but not the dilution of the wording.

 * The **Technical Summary (TS)**
   is a much more detailled summary.
   It contains more information and
   is better in explaining complex mechanisms.

Sometimes the reports are complemented
with Supplementary Material (SM) and attachments,
such as tables, a glossary to explain technical terms and much more.

<sup>
  2 Source:
  <a href="https://www.climatechangenews.com/2022/04/04/saudi-arabia-dilutes-fossil-fuel-phase-out-language-with-techno-fixes-in-ipcc-report/">https://www.climatechangenews.com/2022/04/04/saudi-arabia-dilutes-fossil-fuel-phase-out-language-with-techno-fixes-in-ipcc-report/</a>
</sup>
