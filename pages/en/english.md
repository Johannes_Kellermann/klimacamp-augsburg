---
layout: page
title: English
has_children: true
has_toc: false
permalink: /en/
nav_order: 20
---

We are the Camp for Climate Action in the city of Augsburg (*Klimacamp Augsburg* or *Augsburger Klimacamp*).
Augsburg is an old city with Roman origin,
the place with the most public holidays in all of Germany,
with around 300,000 inhabitants the third biggest city in Bavaria
and the twenty-third biggest city in Germany.

The camp started on the 1st of July 2020 and
hence is the oldest climate camp of its kind in existence.
The camp is a place of protest, activism,
networking, teaching, learning and politics.
Activists from all over Germany and even some other European countries
have visited and stayed for some nights.
Some went back home and started similar projects there.
Some stayed in contact.

So far we have won every legal battle that threatened the existence of the camp.
The plan is to stay until our demands are fulfilled or we run out of strength.
There have to be two people in the camp at any time
so that our camp is legally protected by the freedom of assembly
and we can stay against the wishes of the city.
In the beginning, we thought that we might be able to keep this up for a few days.
With the support of many people (including lawyers)
we have been able to maintain the camp for more than one and half year.
The circle of supporters of the camp
includes people of various age groups
and all levels of education from schoolchildren to postdocs.


# Our demands

We echo a number of [demands](/en/demands/)
of scientists to the local government of Augsburg.
These demands refer to climate justice,
the transition to renewable energy,
changes to the mobility sector in Augsburg,
safeguards for local ecosystems
and other policies.

* [our demands (English)](/en/demands/)
* [our demands (German)](/forderungen/)


# Program

At times the climate camp has a packed program
of panel discussions, lectures, talks and other events.
COVID, the weather and the fact that all events are organised by volunteers
put limitations on the program that we are able to offer.
Guests of events of the camp include scientists, climate activists,
representatives of green citizen energy associations
and politicians of local, state and federal level among others.


# History of the camp

The climate camp started on the 1st of July 2020
in protest of a federal law,
which extended the phase-out of coal until 2038
and gave huge sums of money to coal companies.
The camp is located directly next to
[Augsburg's famous town hall](https://en.wikipedia.org/wiki/Augsburg_Town_Hall).
Because of insufficient climate policies by our local government,
we soon extended our demands and wrote them
in an open letter to the city council.
Friendly visits of our new neighbors in the town hall
were met with calling the police
and threats of the forceful removal of the camp.
Despite a notice of eviction by the city
we stayed next to the town hall and took legal action.
The city lost and the notice of eviction was declared unlawful
by the local court (Verwaltungsgericht Augsburg).
The city filed an appeal against the court decision.
Thanks to this we got the unlawfulness confirmed
with much media attention
by the Bavarian federal state court (Bayerischen Verwaltungsgerichtshof).
The city respected the second court's decision
and didn't file an appeal to the
federal administrative court (Bundesverwaltungsgericht).

![The logo of the Klimacamp Augsburg: A tent in front of the town hall. A window in the town hall shows the earth.](/pages/material/images/logo.png)

In autumn 2020 the city commissioned a study
on possible measures for climate protection in Augsburg.
Although we appreciate the commissioning of the study,
our impression is that in the following year the city government
used the incomplete study as a pretext for rejecting
many good proposals for climate protection measures.

In the beginning of 2021 the city accepted one of our demands.
The city accepted that 9.7 million tons of CO₂ is a fair carbon budget
that Augsburg should strieve to adhere to.

Autumn 2021 saw the publication of the study commissioned by the city.
The suggestions of the study are mostly compatible
and in some cases even identical to our demands.

In December 2021 we had to move from the Fischmarkt
between the town hall and the [Perlachturm](https://en.wikipedia.org/wiki/Perlachturm)
to the Moritzplatz 300 meters to the south.
The reason for this was a report on the structural stability of the Perlachturm
and the risk of stones from the tower hitting the camp.
On the 12th of May 2022 we started the relocation
back to the Fischmarkt.

Our plans for the future are to
 * remind the city officials of our demands
   and the findings of the study that they themselves commissioned,
 * continue to inform the public
 * and work as a networking platform for climate activists.

Even if at one point we lose the ability to maintain the camp,
we strieve to maintain our activity in other forms.
The camp is already far more than a collection of tents.

An incomplete list of past events in and around the camp
can be found under [Programm & Tagebuch](/programm/)
written in German.

![A map of the city center of Augsburg with markings for the main station,
the town hall and the current and temporary location of the camp.](/pages/material/images/location.png)
We use image data from [https://www.openstreetmap.org](https://www.openstreetmap.org).


# COVID

We treat COVID seriously and have rules
regarding masks, distance and the usage of tents and sleeping spaces.
