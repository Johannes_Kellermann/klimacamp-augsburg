---
layout: page
title: "Weitere Informationen"
has_children: true
has_toc: false
permalink: /informationen/
nav_order: 60
---

# Artikel

Hier sammeln wir Beiträge für Personen,
die sich inhaltlich tiefer mit dem Thema *Klimagerechtigkeit*
auseinander setzen möchten.
*Was sind unsere Quellen?*
*Welche Funktion erfüllt Klimaaktivismus?*
Alle paar Wochen soll ein weiterer Artikel hinzu kommen.

* [Quellen – Woher nehmen wir unsere Informationen?](/informationen/artikel/quellen/)<br>
  Hier geben wir eine Auflistung
  einiger unserer wichtigsten inhaltlichen Quellen
  inklusive einer kurzen Beschreibung an.
  Wo es uns möglich ist,
  verlinken wir auch direkt auf die Originalquelle.
* [(Klima-)Aktivismus](/informationen/artikel/klimaaktivismus/)<br>
  Welche Funktion hat Aktivismus in unserer Gesellschaft?
* [Mobilitätswende](/informationen/artikel/mobilitaetswende/)<br>
  Was verstehen wir unter einer Mobilitätswende und
  wie unterscheidet sie sich von einer reinen Antriebswende?


# Multimediale Inhalte

* [Podcasts vom Dezember 2020](/podcast/)
* [Videologs vom Dezember 2020](/videolog/)
