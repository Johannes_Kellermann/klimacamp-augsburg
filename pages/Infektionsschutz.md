---
layout: page
title: Infektionsschutz
permalink: /infektionsschutz/
nav_exclude: true
---

# Willkommen auf der Infektionsschutzseite vom Augsburger Klimacamp

Falls du das Gefühl hast, du könntest eine ansteckende Krankheit haben, die die
Gesundheit anderer Klimacamper\*innen gefährdet, dann kannst du dies
hier mit oder ohne Angabe deines Namens an die Vertrauenspersonen der
Infektionsschutz TF melden. So können wir versuchen, Infektionsketten von
ansteckenden Krankheiten bei uns in der Bewegung zu unterbrechen. #StaySafe

<style>
  textarea { width: 100%; }
</style>

<form action="https://www.speicherleck.de/cgi-bin/mail-tf.pl" method="post">
  <label for="name">Dein Name (freiwillig):</label>
  <input type="text" name="name" id="name"><br>
  <label for="mail">Deine E-Mail-Adresse (freiwillig):</label>
  <input type="mail" name="mail" id="mail"><br>
  <br>
  <label for="description">Welche Krankheit bzw. welche Symptome hast du? Wurdest du positiv getestet?</label>
  <textarea id="description" name="description"></textarea><br>
  <br>
  <label for="im-camp">Warst du in der letzten Zeit im Camp? Wenn ja, wann?</label>
  <textarea id="im-camp" name="im-camp"></textarea><br>
  <br>
  <label for="contact">Hast du dich in Pavillions aufgehalten und wenn ja, in welchen? Hast du Decken/Schlafsäcke benutzt, die nicht deine eigenen waren?</label><br>
  <textarea id="contact" name="contact"></textarea><br>
  <br>
  <label for="close">Mit welchen Menschen hattest du in der letzten Zeit engeren Kontakt? (freiwillig)</label>
  <textarea id="close" name="close"></textarea><br>
  <br>
  <label for="misc">Möchtest du uns noch etwas mitteilen?</label>
  <textarea id="misc" name="misc"></textarea><br>
  <br>
  <input type="submit" value="Absenden">
</form>
