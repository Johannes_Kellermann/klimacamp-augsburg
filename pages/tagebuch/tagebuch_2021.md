---
layout: page
title: "Tagebuch 2021"
permalink: /tagebuch/2021/
parent: "Programm & Tagebuch"
nav_order: 1990
---

# Tagebuch des Jahres 2021

## Donnerstag 09.12.2021 -- **Tag 527**

Aufgrund der Gefahr durch herabfallende Steine am Perlachturm ist das
Klimacamp vorrübergehend zum Moritzplatz umgezogen.
Sobald die Gefahr gebannt ist,
wollen wir unseren alten Platz in Sichtweite der Augsburger Stadtpolitik
wieder einnehmen.


## 47. Kalenderwoche (22. November bis 27. November 2021)

<div style="background-color: orange; padding: 1em">
  Öffentliche Vortragsreihe zur Klimakrise: <a
  href="https://klimauni-augsburg.de/">Public Climate School</a> in
  Augsburg vom 22. bis 27. November 2021
</div>

## Mittwoch 10.11.2021 -- **Tag 498**

Im Klimabeirat der Stadt Augsburg werden die Ergebnisse im Vorjahr
in Auftrag gegebenen Studie
„Klimaschutz 2030: Studie für ein Augsburger Klimaschutzprogramm“
vorgestellt.
Die Ergebnisse der Studie können auf der Webseite der Stadt Augsburg
als [Langfassung](https://www.augsburg.de/fileadmin/user_upload/umwelt_soziales/umwelt/klima%20und%20energie/Studie_Klimaschutz_2030_mit_allen_anlagen.pdf)
wie auch als [Zusammenfassung](https://www.augsburg.de/fileadmin/user_upload/umwelt_soziales/umwelt/klima%20und%20energie/21-10-29_Klimastudie_Augsburg_Zusammenfassung.pdf)
heruntergeladen werden.

## Freitag 24.09.2021 bis Samstag 26.09.2021

„[Themenwochenende zum Klimawandel](https://staatstheater-augsburg.de/pm_klimawochenende)“ des Staatstheaters Augsburg<br>
Auch das Klimacamp beteiligte sich am Programm,
beispielsweise durch den Vortrag
„Optionen des Handelns - Blick hinter die Kulissen des Klimacamps“
am 25.09.2021.

## Freitag 24.09.2021 **GLOBALER KLIMASTREIK, in Augsburg am Ulrichsplatz um 16.00 Uhr.**

Mit Fridays for Future auf die Straße!
[https://fridaysforfuture.de/allefuersklima/](https://fridaysforfuture.de/allefuersklima/)


## Donnerstag 23.09.2021 -- **Tag 450**

Von 17:00 bis 19:30 am Rathausplatz:
_Stell deine Frage! Podiumsdiskussion zum Thema: Struktureller Rassismus in der Bildungsarbeit._
Jüngst gibt es Beschlüsse, die Antirassismus, Diskriminierungssensibilität,
Intersektionalität und Selbstreflexion von der Entwicklungsbildung ausschließen.
Wir möchten diese Beschlüsse diskutieren und Handlungsoptionen entwickeln.

## Sonntag 19.09.2021

Kidical Mass Augsburg, Fahrraddemo für Kinder und alle Augsburger*innen,
Start 14.30 Uhr Königsplatz/Konrad-Adenauer-Allee, Veranstalter: [https://augsburg.adfc.de/artikel/kidicalmass](https://augsburg.adfc.de/artikel/kidicalmass)


## Freitag 17.09.2021

Teilnahme am Parking Day Augsburg [https://de.wikipedia.org/wiki/Parking_Day](https://de.wikipedia.org/wiki/Parking_Day)

<a href="/sharepics/2021-kc-programm-kw28.png" target="_blank" title="klicken für großansicht">
    <img src="/sharepics/2021-kc-programm-kw28.png" style="height: 25em" alt="Klimacamp Augsburg Programm Kalenderwoche 28, 2021">
</a>


## 🗓🏃‍♀️⛺️ **Veranstaltungen am Klimacamp Kalenderwoche 28 (12.7. - 18.7.2021)** 🗓🏃‍♀️⛺️

Do: Um 18 Uhr starten wir vom Klimacamp aus zu einem Neulinge-freundlichen Kletterworkshop.

Sa: Heute könnte der Lohwi zum Tode verurteilt werden... setze dich dagegen ein! Wir demonstrieren in Meitingen! Gemeinsame Abfahrt vom Augsburger Hbf am Morgen, Details bald auf [lohwibleibt.de](https://www.lohwibleibt.de/) 🚂

In 10 Wochen ist Bundestagswahl! Von Samstag auf Sonntag starten wir eine kleine, gemütliche Mitternachtsaktion zur Bundestagswahl. 🗳 Für geeignete Orte schauen wir uns gemeinsam am Freitag in der Stadt um 🚲🌳

Sa: Um ca 20:00 (genaue Zeit wird noch nachgereicht) erzählt Josef Metzger von der Bürgeraktion "Das bessere Müllkonzept" über die hohen aber kaum erwähnten Emissionen der Augsburger Müllverbrennungsanlage 🔥🗑♻️

Di: Ihr wolltet schon immer mal mitbekommen, was und wer hinter der Organisation von zivilem Ungehorsam steckt? Dann seid ihr richtig beim offenen Treffen von Extinction Rebellion (XR) Augsburg! Di um 19 Uhr im Camp. 🦋

Immer donnerstags und sonntags könnt ihr das Klimacamp kennenlernen beim Einstiegstreffen 👀❗️

Di, Do, Fr gibt es Erfahrungsberichte zu verschiedenen Themen in den Nähkästchen-Plaudereien 💬🛋 — immer werktags, außer wenn andere Programmpunkte den Tag schon füllen. Dieses Mal mit einer ganzen Reihe an scheinbaren Ordnungswidrigkeiten, die aber völlig legal sind. 😳 🙌

Wir freuen uns auf euch! 😊❤️

---


<a href="/sharepics/2021-kc-programm-kw27.png" target="_blank" title="klicken für großansicht">
    <img src="/sharepics/2021-kc-programm-kw27.png" style="height: 25em" alt="Klimacamp Augsburg Programm Kalenderwoche 27, 2021">
</a>
<img src="/sharepics/2021-07-11-B17-raddemo.jpg" style="height: 25em" alt="Sharepic B17-Fahrraddemo Juli 2021">

🗓🏃‍♀️⛺️ **Veranstaltungen am Klimacamp Kalenderwoche 27 (5.7. - 11.7.2021)** 🗓🏃‍♀️⛺️

1️⃣ Zahlreiche Aktionen zum Mitmachen zum Schutz des Lohwalds (15 Zugminuten von Augsburg) -- wir arbeiten dazu seit Monaten mit einer Bürger\*inneninitiative zusammen, nun steht die Rodungsentscheidung vor der Tür. Der Lohwi ist eigentlich rechtlich besonders geschützt, doch das zählt leider wenig, wenn der Mensch, der den Lohwi zur Werterhöhung seines Stahlwerks mit viel Abstand einer der größten Sponsoren der CSU ist

2️⃣ Wir schauen der Regierung auf die Finger — Sondersitzung vom Umweltausschuss

3️⃣ Wie sich die AfD im Klimacamp zum Narren machte — eine Geschichte übers Gendern, Platzverweise, Alkohol und unerwartete Stargäste

4️⃣ Zweite große B17-Fahrraddemo in der Geschichte Augsburgs!

5️⃣ (nicht von uns organisiert) Demo zum Polizeiaufgabengesetz: In Bayern dürfen Menschen nicht nur inhaftiert werden, wenn sie rechtskräftig wegen einer Straftat verurteilt wurden, sondern auch "einfach so" (Fachausdruck: "zur Gefahrenabwehr"). Eine*r von uns wäre wegen des PAG fast für vier Wochen ins Gefängnis gekommen, nachdem er*sie als Walderhaltungsprotest zwei Nächte in einer Hängematte im Forst Kasten verbrachte

Am Abend lassen wir immer den Tag gemütlich ausklingen. Auch dazu herzliche Einladung, wir freuen uns auf euch! 😊



---


<a href="/sharepics/2021-kc-programm-kw26.png" target="_blank" title="klicken für großansicht">
    <img src="/sharepics/2021-kc-programm-kw26.png" style="height: 25em" alt="Klimacamp Augsburg Programm Kalenderwoche 26, 2021">
</a>

## 🗓🏃‍♀️⛺️ **Veranstaltungen am Klimacamp Kalenderwoche 26 (28.06 - 04.07.2021)** 🗓🏃‍♀️⛺️

Am Donnerstag, 01.07., wird unser Klimacamp schon ein Jahr alt. Eigentlich ein Grund zum Trauern, denn das heißt, dass unsere Stadtregierung uns noch nicht ausreichend ambitionierte Vorhaben in ihrer Klimapolitik gezeigt hat — sonst wären wir schon lange wieder weg. An diesem Tag gibt es für alle zum spontanen Mitmachen Platzparks bauen und Banner malen 🌸🌿🎨 Dazu noch fetzige Reden, eine Pressekonferenz am Abend und einiges mehr 🏕

Ihr wolltet schon immer mal mitbekommen, wie eine FFF-Demo oder ziviler Ungehorsam von Extinction Rebellion (XR) organisiert wird? Dann seid ihr beim Kennenlerntreffen/Plenum unserer Augsburger Ortsgruppen genau richtig! Diese Woche Dienstag und Samstag 🗣💞🦋 Am Dienstag werden sogar letzte Vorbereitungen für die FFF-Demo am Freitag besprochen 😱

Fr-So gibt es Workshops und Künstlerisches zur Mobilitätswende 🚲🤸. Verschöner deine Shirts mit Radmotiven👕, lass dein Bike durchchecken🔧, drucke Kunst mit Radteilen 🎨 oder spinne Ideen 💭 für eine lebenswerte Nutzung von Parkplätzen 🐝🤙✌️✊

Immer Donnerstags und Sonntags könnt ihr das Klimacamp kennenlernen beim Einstiegstreffen 👀❗️

Mo-Mi gibt es Erfahrungsberichte zu verschiedenen Themen in den Nähkästchen-Plaudereien 💬🛋 — immer Werktags, außer wenn andere Programmpunkte den Tag schon füllen.

Wir freuen uns auf euch! 😊

---

<a href="/sharepics/2021-kc-programm-kw25.png" target="_blank" title="klicken für großansicht">
    <img src="/sharepics/2021-kc-programm-kw25.png" style="height: 25em" alt="Klimacamp Augsburg Programm Kalenderwoche 25, 2021">
</a>

## 🗓🏃‍♀️⛺️ **Veranstaltungen am Klimacamp Kalenderwoche 25 (21. - 27.06.2021)** 🗓🏃‍♀️⛺️

Immer Donnerstags und Sonntags könnt ihr das Klimacamp kennenlernen beim Einstiegstreffen 👀❗️

Mo-Fr gibt es Erfahrungsberichte zu verschiedenen Themen in den Nähkästchen-Plaudereien 💬🛋 — ab jetzt immer Werktags, außer wenn andere Programmpunkte den Tag schon füllen.

Und nicht von uns organisiert, aber neben dem Klimacamp startend: die Critical Mass. Wie jeden letzten Freitag im Monat um 18:00 am Rathausplatz, zusammen gemütlich durch die Stadt radeln 🚴‍♀️🚲🚴‍♂️

Wir freuen uns auf euch! 😊


---


## Sonntag 20.06.2021 -- **Tag 355**
### `17:00 Uhr` Klimacamp Einsteiger\*innenrunde
Alle interessierten Menschen, die uns näher kennenlernen oder im Camp ein bisschen aktiv werden wollen, sind willkommen, vorbeizukommen und mehr zu erfahren :-)

## Donnerstag 17.06.2021
### `16:00 Uhr` Klimacamp Einsteiger\*innenrunde
Alle interessierten Menschen, die uns näher kennenlernen oder im Camp ein bisschen aktiv werden wollen, sind willkommen, vorbeizukommen und mehr zu erfahren :-)

### `19:30 Uhr` Walderkundung mit angehendem Förster
Treffpunkt an der Straßenbahnhaltestelle "Dürrer Ast". Wir freuen uns auf alle Waldinteressierten!


## Sonntag, 06.06.2021
### `14:00 Uhr` Fahrraddemo über die B17, Treffpunkt Plärrer

## ~~ hier fehlen ein paar Einträge, die wir demnächst nachtragen

## Freitag, 19.03.2021
### `11 Uhr` am Königsplatz: Globalstreik von Fridays for Future
Der 19. März wird fett! Seit über 2 Jahren streiken wir für mehr Klimagerechtigkeit.
Wir fordern nicht mehr, als dass die Politik ihre eigenen Versprechen, das 1,5-Grad-Grenze, einhält.
Der Globalstreik steht deshalb unter den Hashtags #NoMoreEmptyPromises und #AlleFür1Komma5.

Mehr Infos [hier](https://www.fff-augsburg.de/nomoreemptypromises/).

## Samstag 13.03.2021 -- **Tag 256**
### `11:00 Uhr` [Baum-Demo](https://baumallianz-augsburg.de/archive/1944)

## Sonntag 28.02.2021
### `14:00 Uhr` Demo zum Erhalt des Lohwalds in Meitingen

<img src="https://www.speicherleck.de/iblech/stuff/augsburg-klimacamp/5.jpeg" style="width: 100%">

Einer der reichsten Menschen Deutschlands möchte einen besonders geschützten
Bannwald roden, um sein Kohlestrom-betriebenes Stahlwerk zu vergrößern. Zuvor
baute er in die andere Richtung einen Parkplatz hin, sodass es jetzt "keine
andere Möglichkeit" gibt. Laut BUND Naturschutz ist insbesondere der Boden des
Bannwalds ökologisch unglaublich wertvoll. Alle umliegenden Gemeinden sind
gegen diesen Irrsinn.

Der Mann, um den es geht, ist Max Aicher, 86 Jahre, Besitzer mehrerer
Schlösser, und der Bannwald ist direkt bei uns vor Ort. Es ist der Lohwald in
Meitingen.

Es gibt gleich drei Bürger\*inneninitiativen, die gegen dieses aus der Zeit
gefallene Projekt kämpfen. Ein breites Bündnis ruft daher zur Demo diesen
Sonntag auf, darunter Klimacamp und Fridays for Future Augsburg. Meitingen ist
klein, da kann eine Demo viel bewirken! Kommt unbedingt vorbei! Bitte teilt
diesen Aufruf!

Falls ihr nicht an unserer Zubringerdemo teilnehmen möchte: direkter Treffpunkt
"Am Lohwald" südlich der Lechstahlwerke.

## Freitag 26.02.2021
### `14:00 Uhr` Lerne aktivistisches Klettern!

<img src="https://www.speicherleck.de/iblech/stuff/augsburg-klimacamp/1.jpeg" style="width: 30%">
<img src="https://www.speicherleck.de/iblech/stuff/augsburg-klimacamp/2.jpeg" style="width: 30%">
<img src="https://www.speicherleck.de/iblech/stuff/augsburg-klimacamp/3.jpeg" style="width: 30%">

## Donnerstag 25.02.2021
### `16:00 Uhr` Interview mit Salzburger Zeitung

### `18:00 Uhr` Online-Workshop: Hoffnung durch Handeln (Dr. Tobias Bayr, Klimaforscher am GEOMAR Kiel)

<img src="https://www.speicherleck.de/iblech/stuff/augsburg-klimacamp/4.jpeg" style="width: 100%">

Wie kann mensch es schaffen, sich mit den globalen Themen wie Klimaerwärmung,
ökologische Krise und soziale Ungerechtigkeit zu beschäftigen, ohne sich von
der Größe der Themen überwältigen zu lassen? In diesem Workshop erhältst du
„Werkzeug“, das dir dabei helfen kann. Dies kann uns helfen, aus der Ohnmacht
ins Handeln zu kommen und sich kraftvoll für unseren Planeten einzusetzen.

[Dr. Tobias Bayr](http://www.klimafrosch.de/) wurde 1979 geboren, studierte
Meteorologie und arbeitet als Klimaforscher am GEOMAR in Kiel. Von 2014 bis
2015 lebte er im Ökodorf ZEGG. Er ist ein leidenschaftlicher „Change Maker“,
der von Tiefenökologie, Buddhismus und gewaltfreier Kommunikation inspiriert
ist.

Einladungslink: [via Zoom](https://uni-kiel.zoom.us/j/83022867468?pwd=dmVneERLZnFwbE5jNm9udm5FWjJuZz09)
(Meeting-ID: 830 2286 7468, Kenncode: 996563)

## Freitag 05.02.2021
### `5:45 Uhr` Lehrpfaderrichtung im Augsburger Stadtwald

## Mittwoch 03.02.2021
### `13:00 Uhr` Interview mit ARD
### `17:00 Uhr` Demonstration mit Gehzeugen in der Hermanstraße

## Montag 01.02.2021
### `6:00 Uhr` Baumbesetzung in Gersthofen beim Paul-Klee-Gymnasium

## Mittwoch 27.01.2021
### `7:00 Uhr` Interview mit BR (den gesamten Vormittag über)

## Montag 25.01.2021
### `9:30 Uhr` Treffen mit BR
### `12:00 Uhr` Interview mit Radiosender M94.5

## Samstag 02.01.2021 -- **Tag 186**
### `11:30 Uhr` Pressekonferenz
