---
layout: page
title: SWA
permalink: /swa/
nav_exclude: true
---

# Klimacamper\*innen beenden Greenwashing-Kampagne der Augsburger Stadtwerke

<img src="https://www.speicherleck.de/swa.png" style="width: 2px" height="2px">

Die Stadtwerke Augsburg (swa) sind das drittgrößte Stadtwerk Bayerns, und
versorgen 350.000 Bürger\*innen der Stadt,und deren Umgebung mit Strom, Erdgas,
Fernwärme und Trinkwasser. Die eigens gesteckten Ziele der swa sind eine
nachhaltig positive Entwicklung der Stadt und der Region, unter
Berücksichtigung von Umweltschonung, Nachhaltigkeit und Wirtschaftlichkeit [1].

Tatsächlich sind die swa in einigen Punkten vorbildlich: In Augsburg fährt
dank regionalem Biogas die umweltfreundlichste Busflotte Deutschlands, die swa
setzen, wie zum Beispiel mit einem Fernwärme-Blockheizkraftwerk, auf
regionale, klimaneutrale Technologien, oder auch mit einer sogenannten
Power-to-Gas-Anlage auf zukunftsfähige Speichertechnologien, die für eine
erfolgreiche Energiewende notwendig sind. [2] Der Augsburger Verkehrsverbund
AVV, dem die swa angehören, und der den Nahverkehr im Großraum Augsburg
organisiert, führt demnächst ein 365-Euro-Ticket für Jugendliche und
Auszubildende ein. [3]

Allerdings beteiligen sich die Stadtwerke immer noch an klimaschädlichen
Technologien: Im Jahr 2019 bezogen die swa 27 % Strom aus fossilen
Energieträgern, davon 18,6 % aus Kohlekraft und 7,8 % aus Erdgas (Stand 2020
[4]). Diese stellen die klimaschädlichsten Formen der Energiegewinnung dar: pro
Gigawattstunde werden bei Energiegewinnung aus Kohle 820 Tonnen CO2, bei Erdgas
490 Tonnen CO2 ausgestoßen. Zum Vergleich: bei Wasserkraft, Windkraft oder
Solarenergie sind es jeweils 34, 4 bzw. 5 Tonnen. [5] Ausserdem verursacht die
Luftverschmutzung durch die Nutzung fossiler Energieträger zahlreiche
Krankheiten und Tode. Pro Terrawattstunde Kohlestrom müssen ungefähr 25
Menschen ihr Leben lassen, die Folgen der Klimakrise sind dabei noch gar nicht
mit einbezogen. [5] 

In einem Gespräch mit den swa im Klimacamp vergangenes Jahr wurde uns
bestätigt, dass es die Stadtwerke lediglich 4 Millionen Euro kosten würde, auf
100 % Ökostrom umzustellen, und dabei die Preise für die Verbraucher\*innen
gleich zu halten. Die Ökostromumstellung Anfang 2021 [6] ist leider nur
Zertifikatehandel, was heißt, dass bei uns der Anteil erneuerbarer Energien auf
dem Papier steigt, allerdings dieser anderswo sinkt und so im Gesamtnetz nicht
zunimmt. Dadurch erkaufen wir uns lediglich ein reines Gewissen, anstatt die
Energiewende zu unterstützen. Ausserdem fand die Umstellung nur für
Privathaushalte statt -- Unternehmen beziehen immer noch den dreckigen grauen
Strommix.

Ebenso stagnieren seit Jahren die Fahrgastzahlen im öffentlichen Nahverkehr.
Die letzten Tariferhöhungen machten den ÖPNV lediglich für Abo-Kund\*innen
günstiger, was die Attraktivität vor allem für Neukund\*innen deutlich senkt.
Vor allem auf dem Land gibt es oft zu wenig Busse, keine oder nur eine
schlechte Anbindung. An manchen Tagen erfolgt gar keine Bedienung der Linien.
Die Preise für Einzelfahrscheine ins Umland übersteigen die Kosten für eine
Autofahrt deutlich. Für eine Verkehrswende muss der Nahverkehr allerdings eine wirkliche
Alternative darstellen, da ihn die Menschen sonst nicht nutzen. Dazu gehört
auch die Bezahlbarkeit, damit sich nicht nur finanziell privilegierte Menschen
den ÖPNV leisten können, sowie die Schnelligkeit der Verbindungen,
welche zum Beispiel durch das Schnell- und Ringbuslinienkonzept "Verkehr 4.0
für den Ballungsraum Augsburg" seit Jahren an die Lokalpolitik herangetragen
wird. [7] **Um es erneut zu betonen: Es ist im Sinne der Klimagerechtigkeit
wichtig, dass diese wissenschaftlich notwendigen Maßnahmen sozial gerecht
gestaltet werden. Eine Änderung hin zu einer klimagerechten Stadt, bedeutet ein
gutes Leben für alle. Ein nachhaltiges und gerechtes Leben darf kein Privileg
sein.**

Unsere Oberbürgermeisterin Eva Weber wirbt damit, Augsburg zur
klimafreundlichsten Stadt Bayerns zu machen [8]. Da die Stadtwerke Augsburg zu
100 % im Besitz der Stadt sind, fordern wir den sofortigen Ausstieg aus der
Kohleenergie durch den Kauf von echtem Ökostrom, und nicht lediglich
Zertifikaten, sowie eine Umsetzung des Verkehrskonzept "Verkehr 4.0", einen
Ausbau sowie eine Vergünstigung des öffentlichen Nahverkehrs in Augsburg, als
schnell umsetzbare Maßnahmen, um das oben genannte Versprechen einhalten zu
können.

Ebenso fordern wir Transparenz in der Kommunikation über dieses Thema. Es kann
nicht sein, dass Frau Weber und auch die Stadtwerke das Klimacamp immer mit der
Begründung, dass kein Geld vorhanden sei, vertrösten, dann aber in eine große
Greenwashing-Kampagne für die Stadtwerke investiert wird, in welcher eine
scheinbar ökologische Ausrichtung vorgegaukelt wird.

Während der Amtszeit von Oberbürgermeister Kurt Gribl verkaufte die Stadt
Augsburg Anteile an erdgas schwaben, was mehrere Millionen Euro in die
Stadtkassen spülte. Die Stadtwerke halten zurzeit 35,1 % an erdgas schwaben.
[9] erdgas schwaben besitzt zwar eine Unternehmenstochter für regenerative
Stromerzeugung, allerdings beteiligen sie sich auch an klimaschädlichem Erdgas,
und beziehen ebenfalls ungefähr 20 % fossile Energieträger im Stromgeschäft.
[10] Ein Verkauf der restlichen Anteile würde einen Ausstieg aus fossilen
Energieträgern bedeuten, sowie Gelder in die Stadtkassen spülen, welche für
eine klimagerechte Umstrukturierung, und somit auch ein Erreichen der
ehrgeizigen Ziele der Stadt, Eva Webers und den Stadtwerken Augsburg, genutzt
werden könnten (gerade nachdem die Stadt sich das ihr noch zustehende
CO2-Budget von 9,7 Mio. Tonnen zum Einhalten des 1,5°-Limits als Ziel gesetzt
hat). [11]

[1] https://www.sw-augsburg.de/ueber-uns/unternehmen/<br>
[2] https://www.sw-augsburg.de/fileadmin/content/6_pdf_Downloadcenter/8_Innovation_Nachhaltigkeit/swa_Umwelterklaerung_27Okt_72dpi.pdf<br>
[3] https://www.br.de/nachrichten/bayern/365-euro-ticket-fuer-jugendliche-in-augsburg-beschlossen,SRHSZL2<br>
[4] https://www.sw-augsburg.de/fileadmin/content/6_pdf_Downloadcenter/1_Energie/swa_Strom-Mix.pdf<br>
[5] https://ourworldindata.org/safest-sources-of-energy<br>
[6] https://www.sw-augsburg.de/magazin/detail/stadtwerke-augsburg-stellen-auf-oekostrom-um/<br>
[7] https://www.verkehr4x0.de/<br>
[8] https://www.evaweber.de/news/detail/blue-city-eva-weber-plant-augsburg-als-klimafreundlichste-grossstadt-bayerns<br>
[9] https://de.wikipedia.org/wiki/Erdgas_Schwaben#Umstrukturierung_zur_erdgas_schwaben_gmbh<br>
[11] https://ratsinfo.augsburg.de/bi/to020.asp?TOLFDNR=30051<br>
[10] https://www.erdgas-schwaben.de/privat/stromkennzeichnung
