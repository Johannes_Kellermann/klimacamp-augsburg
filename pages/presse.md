---
layout: page
title: "Presse"
has_children: true
has_toc: false
permalink: /presse/
nav_order: 80
---

Da unser Navigationsmenü zu lang wurde,
haben wir Pressemitteilungen, Pressespiegel und Offene Briefe
hier zusammengefasst.

* [Offene Briefe](/offeneBriefe/) enthält eine eine Liste
  mit offenen Briefen,
  die das Klimacamp an Politiker\*innen wie auch Unternehmen
  geschrieben hat.
* [Pressemitteilungen](/pressemitteilungen/)
  enhält eine Liste von Pressemitteilungen,
  die wir oder Organisationen,
  mit denen wir zusammenarbeiten,
  veröffentlicht haben.
* [Pressespiegel](/pressespiegel/)
  enthält eine Liste von Berichten über uns.
