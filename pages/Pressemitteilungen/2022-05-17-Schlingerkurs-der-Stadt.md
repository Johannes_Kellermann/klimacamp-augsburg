---
layout: page
title:  "17.05.2022: Schlingerkurs der Stadt"
date:   2022-05-17 10:00:00 +0100
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
permalink: /pressemitteilungen/2022-05-17-schlingerkurs-der-stadt/
nav_order: 315
---

*Pressemitteilung vom Augsburger Klimacamp am 17.5.2022*

# Stadt wechselt den Kurs gegenüber dem Klimacamp – aber unklar in welche Richtung

Nach der dritten Niederlage vor Gericht (Eilverfahren am Amtsgericht,
Hauptverfahren am Amtsgericht, Hauptverfahren am Bayerischen
Verwaltungsgerichtshof) erwarteten viele in Augsburg einen
grundsätzlichen Kurswechsel der Stadt den Aktivist\*innen gegenüber. Ein
solcher zeichnet sich zuletzt auch ab – allerdings sind die Signale
widersprüchlich. Nach Eva Webers großer Stellungnahme vom 21. April, in
der sie den Klimacamper\*innen eine ganze Reihe von Vorwürfen gemacht
hatte, setzte die Oberbürgermeisterin am gestrigen Montagabend bei der
Zukunftspreisverleihung auf eine andere Strategie: Nachdem Frau Weber
seit 22 Monaten ein laufendes Gesprächsangebot von den Klimacamper\*innen
hatte, dem sie täglich nachkommen könnte, lud sie nun selbst das
Klimacamp ein, sich mit ihr unter ihren Bedingungen zusammenzusetzen und
Kompromisse für Klimaschutz in Augsburg zu erarbeiten.

Aktivistin Charlotte Lauter (19) bezeichnet das als „Weber-Masche“.
„Frau Weber ist immer zu allen 'gesprächsbereit', aber handlungsbereit
ist sie nie. Das merkt man aber erst, wenn man sich länger um ein Thema
kümmert oder von ihr zweifach verklagt wird.“ Zudem versicherte die
Stadt Augsburg in Gesprächen zum Umzug des Klimacamps vom Fischmarkt zum
Moritzplatz die Rückkehr nach den Bauarbeiten. Tatsächlich ist die neue
Materialfläche fürs Klimacamp nur etwa halb so groß. Das war anders
abgesprochen.

Von einer „gereichten Hand“ (wie MdL Cemal Bozoglu auf Instagram
schreibt
[https://www.instagram.com/p/Cdpu3PYNYlU/](https://www.instagram.com/p/Cdpu3PYNYlU/))
war bereits am
Dienstag nur mehr wenig zu spüren: Ein Aufgebot von Ordnungsbehörde und
Polizei kam zu zehnt um neun Uhr morgens zur Überprüfung von
Versammlungsauflagen ins Klimacamp neben dem Rathaus. Die Aktion ließ
nun wieder einen harten Kurs heraushören: Prominent zum Einsatz kam ein
Lasermessgerät zur millimetergenauen Abstandsbestimmung.

„Dasselbe Aufgebot an Ordnungsbehörde und Polizei hätte den Vormittag
über auch Falschparker und Rowdy-Radfahrer kontrollieren können. Die
machen jeden Tag den Fußgängern als schwächste Verkehrsteilnehmer das
Leben schwer. Mehr Verkehrswende, weniger Rechtsstreits!“, fordert
Aktivistin Lucia Reng (22) die Stadt auf. „Die Klimakrise wird nicht
weniger brisant, nur weil im Camp eine Palette zwanzig Zentimeter weiter
rechts oder links steht. Und während sich die Stadt im Kleinkrieg
verliert, befeuert sie immer noch mit Steuermitteln die Kohle- und
Ölindustrie.“ Damit bezieht sich Reng darauf, dass der von der
städtischen KlimaKom-Studie empfohlene Divestmentprozess immer noch
nicht eingeleitet wurde und die Stadt sowohl direkt als auch indirekt
toxische Investments in der fossilen Energiewirtschaft tätigt (etwa durch
Geldeinlagen bei Banken, die das machen, oder durch die Beteiligung der
Stadtwerke an dem bayerngas-Konzern).
