---
layout: page
title:  "10.12.2021: Entsetzen über einseitigen Radentscheid-Vertragsbruch der CSU -- Lob für grüne Fraktion"
date:   2021-12-10 22:00:00 +0200
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
nav_order: 224
---

*Pressemitteilung vom Augsburger Klimacamp am 10. Dezember 2021*

# Entsetzen über einseitigen Radentscheid-Vertragsbruch der CSU -- Lob für grüne Fraktion

Über Monate gab es Verhandlungen, bis am 05.07.2021 die Initiatoren des
Augsburger Radentscheids und die Stadt schließlich einen
öffentlich-rechtlichen Vertrag schlossen. Das damals noch gemeinsam so
verkündete Ziel: Den Fahrradverkehr in Augsburg nicht länger strukturell
benachteiligen und dadurch ermöglichen, dass mehr Bürger\*innen auch die
Chance bekommen, vom Auto aufs Rad umzusteigen. Nun brach die CSU in der
gestrigen Bauausschusssitzung dieses Versprechen. Mit ihrem Beschluss
setzten sie sich einseitig über die Vertragsvorgaben zur
Stellplatzsatzung hinweg. Die Aktivist\*innen des Augsburger Klimacamps
sind entsetzt, nach eigenen Angaben aber wenig überrascht. Nur Grüne und
SPD stimmten gegen die Beschlussvorlage von CSU-Baureferent Gerd Merkle.

Im gemeinsamen Vertrag war ursprünglich vereinbart, dass
Immobilieneigentümer\*innen mehr Freiheiten erhalten dürfen, das heißt
auf Wunsch weniger PKW-Stellplätze und mehr Radstellplätze vorsehen
dürfen. „Die CSU zeigte sich in der gestrigen Bauausschussitzung
unwillig und unfähig, selbst diesen kleinen Schritt auf dem Weg zur
Mobilitätswende einzuschlagen“, so Charlotte Lauter (19) vom Klimacamp.
Lauter könne nicht nachvollziehen, dass die CSU offenbar nicht einmal
Willens ist, Klimaschutzmaßnahmen umzusetzen, welche die Stadt nichts
kosten.

„In ihren Worten gibt sich die CSU heutzutage einen grünen Anstrich und
Söder umarmt Bäume. Doch in ihren Handlungen wird immer wieder klar, wie
sehr die CSU einseitig klimaschädliche Partikularinteressen des
motorisierten Individualverkehrs bedient“, so Lauter weiter. Der
gestrige Vertragsbruch demonstriere das wieder einmal mehr.

„Die Oberbürgermeisterin erklärt oft, dass sie nicht verstehe, wieso
Augsburgs Klimaschutzverzögerungen aus wissenschaftlicher Sicht
zukunftsschädlich und riskant seien“, so Lauter weiter. „Im Februar
setzte sich die Stadt eine klare CO₂-Obergrenze. Im Juli hat sie die
Bus- und Trampreise erhöht, zum Jahreswechsel treibt sie die
Preisschraube weiter in die Höhe, und nun bricht die CSU auch noch
einseitig den Radentscheid-Vertrag. Und da wundert sich Frau Weber noch,
dass wir ihr Schneckentempo kritisieren?“


## Fehlende Kommunikation mit dem Aktionsbündnis „Fahrradstadt jetzt“

„Die Augsburger CSU wirft uns immer wieder vor, nicht gesprächsbereit zu
sein“, erinnert Lauter. „Dieser Vorwurf verkennt natürlich, dass es die
physikalischen Gegebenheiten sind, sprich die realen Bedrohungen durch
die Erdaufheizung, die den Zeitdruck herstellen, und dass mit diesen
nicht verhandelt werden kann. Aber die CSU übt selbst eine Politik des
Schweigens.“ Lauter bezieht sich mit ihrer Aussage darauf, dass den
Radentscheidinitiator\*innen in den Verhandlungen zugesichert worden war,
dass sie in die weiteren Schritte kommunikativ eingebunden werden
würden. „Einzig aus formaljuristischen Gründen hielt diese Zusicherung
nicht Einzug in den Vertragstext.“ Doch nun fand vor dem Vertragsbruch
keine Kommunikation mit dem Aktionsbündnis „Fahrradstadt jetzt“ statt.


## Positiver Einsatz der grünen Fraktion

Als klaren Gegensatz dazu sehen die Klimacamper\*innen den Einsatz der
grünen Fraktion im gestrigen Bauausschuss. Nach Ansicht der
Klimacamper\*innen versuchten sie nicht nur, den Vertragsbruch
aufzuhalten, sondern auch, die konsequente Ablehnung der Empfehlungen
der Bürgerversammlung zu verhindern.

Diese hatte am 12.10.2021 der Stadt eine Reihe von Maßnahmen,
insbesondere für den Sektor der städtischen Mobilität, empfohlen. Nahezu
alle Anträge wurden aber mit dem Verweis auf formaljuristische Fehler
verworfen. „Muss man denn Anwältin sein, um in der Bürgerversammlung
Anträge zu stellen?“, wundert sich Lauter. Die Grünen setzten sich in
der Bauausschusssitzung dafür ein, dass die Empfehlungen ihrer Intention
nach und nicht ihrem Wortlaut nach beurteilt werden, konnten sich damit
gegen ihre Koalitionspartnerin aber nicht durchsetzen. Nicht einmal eine
Zurückstellung einiger Anträge, um mehr Vorbereitungszeit zu schaffen,
ließ die CSU zu.


## Ausführliches Sitzungsprotokoll

Die Stadt hält ausführliche Sitzungsprotokolle, die das
Abstimmungsverhalten der einzelnen Stadträt\*innen sowie die
Diskussionsbeiträge offenbaren, unter Verschluss.

Ein von Klimacamper\*innen angefertigtes eigenes Protokoll gibt es aber
[hier](https://augsburg.klimacamp.eu/pages/material/Protokolle/2021-12-09-protokoll-bauausschuss.pdf).
