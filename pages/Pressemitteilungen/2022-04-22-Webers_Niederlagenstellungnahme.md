---
layout: page
title:  "22.04.2022: Augsburger Klimacamp zu Webers Niederlagenstellungnahme"
date:   2022-04-22 10:00:00 +0100
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
permalink: /pressemitteilungen/2022-04-22-antwort-auf-stellungnahme-webers/
nav_order: 307
---

*Pressemitteilung vom Augsburger Klimacamp am 22.4.2022*

# Augsburger Klimacamp zu Webers Niederlagenstellungnahme

Oberbürgermeisterin Weber offenbart in ihrer Niederlagenstellungnahme
schlechtes Verständnis für die Strukturen des Augsburger Klimacamps

Der Bayerische Verwaltungsgerichtshof legt nun die ausführliche Begründung für
sein Urteil vor: Das Augsburger Klimacamp war (zum Zeitpunkt des städtischen
Räumungsbescheids) eine vom Grundgesetz geschützte Versammlung.
Oberbürgermeisterin Eva Weber fordert daraufhin in einer vierseitigen
persönlichen Stellungnahme das Klimacamp als ihre Opposition auf, sich entweder
vollends in bestehende Verwaltungsstrukturen einzufinden oder die Zelte
abzubrechen. Das offenbart Weber als schlechte Verliererin. Sind es doch die
Menschen im Klimacamp, welche Brücken schlagen und für Maßnahmen werben, um
Augsburg auf einen klimagerechten Pfad zu bringen. Kaum ein Tag vergeht, an dem
nicht Passant\*innen im Campgespräch Aha-Moment erleben, klimapolitische
Zusammenhänge erkennen und neue Handlungsoptionen wahrnehmen.
Klimagerechtigkeit ist Querschnittsthema – und damit per definitionem
multithematisch.

Wie UN-Generalsekretär Antonio Guterres erst Anfang diesen Monats feststellte,
ist es nicht radikal, günstigere Busse und Trams sowie eine Abkehr von fossilen
Brennstoffen zu fordern. Aber es ist radikal, die fossile Infrastruktur weiter
auszubauen – so wie es die Stadt Augsburg und die Stadtwerke tun, wenn sie das
Fernwärmenetz weiter mit Erdgas betreiben [1] oder lieber 6,2 Millionen Euro in
ein unzeitgemäßes Parkleitsystem investieren statt in gut ausgebauten,
attraktiven und bezahlbaren ÖPNV.

Machen ist wie reden, nur krasser.

[1] [https://www.sw-augsburg.de/magazin/detail/das-heizkraftwerk-herz-der-fernwaerme/](https://www.sw-augsburg.de/magazin/detail/das-heizkraftwerk-herz-der-fernwaerme/)


KONTAKT
Ingo Blechschmidt (+49 176 95110311)


## NACHRICHTLICHE ANMERKUNGEN ZU EINZELNEN ÄUSSERUNGEN

### Klimacamper\*innen klären Missverständnisse zum Thema Klimagerechtigkeit auf

Für diese Menschen stehen wir seit nun fast zwei Jahren rund um die Uhr für
Gespräche zur Verfügung. Dabei stellt sich heraus: Fast alle finden es gut,
wenn ...

* ... Busse und Trams günstiger und ausgebaut werden. So können Privatpersonen
  öfter ihr Auto stehen lassen oder müssen sich gar nicht erst ein teures Auto
  zulegen.

* ... es ein gut ausgebautes Radwegenetz gibt, auf dem sich auch Kinder und
  ältere Radler\*innen sicher fühlen. Kopenhagen wurde so umgebaut, dass dort nun
  zwei Drittel aller Wege mit dem Fahrrad zurückgelegt werden. Die wenigen
  verbleibenden Autofahrten (Lieferverkehr, Arztbesuche, ...) sind dann bequemer
  und flüssiger.

* ... die Stadtwerke nicht mehr Kohle- und Gasstrom beziehen. Wir
  subventionieren damit Putins Rüstungsindustrie oder andere Staaten, in denen
  Menschenrechtsverletzungen an der Tagesordnung sind. Privatkunden sind eh schon
  umgestellt, Unternehmen können statt 30 ct/kWh für Graustrom auch 31 ct/kWh für
  Ökostrom zahlen.

Das Klimacamp sieht sich als Brückenbauer. Es trägt den wissenschaftlichen
Kenntnisstand unter die breite Bevölkerung und arbeitet auf eine Verkleinerung
der Diskrepanz zwischen dem, was möglich und existenziell notwendig wäre, und
dem, was die Stadt tut, hin. Das Klimacamp formuliert wissenschaftliche
Erkenntnisse und Empfehlungen von Bürger\*innenräten als politischen
Forderungen.


### Klimacamper\*innen stellen sich zu Wahlen auf

* Diejenigen, die besonders von der Klimakrise betroffen sind, sind unter 18
  und können daher weder wählen noch gewählt werden.

* Die damals wie jetzt noch minderjährige Schülerin Janika Pondorf versuchte
  es zur Kommunalwahl 2020 dennoch und sammelte innerhalb von nur zwei Tagen die
  benötigten 470 Stützunterschriften. Sie wurde vom Wahlamt abgewiesen.

* Der Augsburger Klimaaktivist Alexander Mai (25, Mathe-Student und
  IT-Entwickler) stellte sich 2022 als von der ÖDP unterstützter parteiloser
  Kandidat für die Bundestagswahl auf.

* Die Sachverhalte sind klar, alle Studien kommen zum selben Schluss, alle
  aus Bevölkerungsquerschnitten zusammengesetzten Bürger\*innenräten empfehlen
  dasselbe: Stadt, Land und Bund müssen dringend Klimagerechtigkeitsmaßnahmen
  beschließen. Damit können wir eine unkontrollierte Erdaufheizung vermeiden und
  nebenbei die Lebensqualität durch größere Freiheit in der Wahl des
  Verkehrsmittels, durch neue gemütliche Fußgänger\*innenzonen und geringere
  Lärmbelastung steigern. Es braucht nicht noch eine weitere Partei. Der neue
  IPCC-Bericht hat deutlich gemacht, dass dafür auch gar keine Zeit ist. Die
  bereits amtierenden Politiker\*innen müssen jetzt handeln!


### Klimacamper\*innen arbeiten in städtischen Gremien mit

* Wir engagieren uns im städtischen Klimabeirat.

* Wir engagieren uns im städtischen Nachhaltigkeitsbeirat.

* Wir engagieren uns bei der Bürger\*innenversammlung. Das brachte aber nichts
  und wird auch in Zukunft nichts bringen: Nahezu alle Empfehlungen, sowohl die
  von uns als auch die von anderen Bürger\*innen eingebrachten, wurden im Stadtrat
  mit vorgeschobenen formaljuristischen Gründen abgelehnt. Eine echte inhaltliche
  Diskussion fand nie statt.

* Wir engagieren uns im städtischen Umweltausschuss – bei allen vier Malen seit
  Gründung von Fridays for Future, in denen uns Rederecht zugesprochen wurde.

* Wir organisierten zusammen mit FAL und ADFC das erfolgreiche
  Bürger\*innenbegehren „Fahrradstadt JETZT“.


### Das Klimacamp unterbreitet konkrete an die Stadt Augsburg angepasste Maßnahmenvorschläge

* Etwa im Klimabeirat, im Nachhaltigkeitsbeirat und in der
  Bürger\*innenversammlung.

* Wir sind natürlich keine Expert\*innen für alles. Die Stadt gab aber bei
  Expert\*innen eine Studie für konkrete Klimaschutzmaßnahmen in Augsburg in
  Auftrag, die sog. KlimaKom-Studie „Klimaschutz 2030: Studie für ein Augsburger
  Klimaschutzprogramm“. Diese Studie rügt, wie viel Zeit die Stadt bereits
  ungenutzt verstreichen ließ, und gibt kluge Ziele, wie eine Reduktion des
  motorisierten Individualverkehrs bis ~~2030~~ **2040** um 50% vor.
  Diese Studie lässt die Stadt leider enttäuschend weit links liegen.<br>
  ***In der ursprünglichen Version der Pressemitteilung hieß es,
  dass die Studie eine Halbierung des motorisierten Individualverkehrs
  bis 2030 empfiehlt.
  Tatsächlich empfiehlt die Studie die Halbierung bis 2040.
  Die Reduzierung des motorisierten Individualverkehrs
  sollte lediglich bereits vor 2030 Wirkung zeigen.
  Wir bitten diesen Fehler zu entschuldigen.***

* Wir organisieren kontinuierlich Workshops, Vorträge und Diskussionsrunde
  mit Expert\*innen. Dazu waren auch schon immer Lokalpolitiker\*innen
  eingeladen. Politiker\*innen der CSU lassen sich dabei in den seltensten Fällen
  blicken.


### Klimacamper\*innen verlassen ihre Komfortzone

Wir übernachten bei –20 °C im Camp, investieren Stunden in die aktivistische
Arbeit, werden von Behörden systematisch schikaniert (die unverhältnismäßigen
Hausdurchsuchungen sind große Belastungen) und halten queerfeindlichen
Brandanschlägen stand. Nicht alle Passant\*innen sind an konstruktivem Dialog
mit uns interessiert und wollen oft nur Dampf ablassen – auch in diesen Fällen
versuchen wir, in einem inhaltlichen Gespräch den Leuten noch etwas mitzugeben.


### Das Klimacamp erkennt städtische Klimaschutz-Erfolge an

Wir erkennen an, dass die Stadt das Thema Klimaschutz als Schlüsselthema
identifiziert hat und an verschiedenen Punkten an Klimaschutzmaßnahmen
arbeitet. Wir müssen trotzdem auf die physikalischen Gegebenheiten hinweisen,
die erheblichen Zeitdruck herstellen und zeitnahes Handeln erfordern.


### Das Klimacamp arbeitet inhaltlich

Neben direkten Aktionen und politischen Stellungnahmen organisieren wir
kontinuierlich Vorträge, Workshops und Podiumsdiskussionen rund um
Klimagerechtigkeit, verfassen Informationsmaterialien, betätigen uns in und
tauschen uns mit anderen Umweltverbänden aus – oder sind auch in ihnen oder
Parteien aktiv. Wir stehen im Dialog mit Parteien und Fraktionen auf Kommunal-
und Landesebene, Stadtwerken und Stadtverwaltung.
