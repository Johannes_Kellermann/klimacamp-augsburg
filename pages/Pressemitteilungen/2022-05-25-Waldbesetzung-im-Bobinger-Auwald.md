---
layout: page
title:  "25.05.2022: Waldbesetzung im Bobinger Auwald"
date:   2022-05-25 10:00:00 +0100
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
permalink: /pressemitteilungen/2022-05-25-waldbesetzung-im-bobinger-auwald/
nav_order: 317
---

*Pressemitteilung unabhängiger Klimaaktivist\*innen am 25.5.2022*

# Klimaaktivist\*innen eilen Aktion zum Erhalt ihres Walds zu Hilfe

Klimaaktivist\*innen eilen mit illegaler Aktion Bürgern in Bobingen
zum Erhalt ihres Walds zu Hilfe:
Besetzung nach Vorbild des Hambacher Forsts geplant,
Informationen auf [bobinger-auwald-bleibt.de](bobinger-auwald-bleibt.de)

Nachdem der Wehringer Bürgermeister Manfred Nerlinger (CSU) statt
Flächen zu entsiegeln den Bobinger Auwald roden und so neue Flächen
versiegeln möchte, kündigen unabhängige Klimaaktivist\*innen (sowohl
Aktive im Klimacamp als auch Kletter\*innen, die sich nicht im Klimacamp
engagieren) für die Nacht vom Mittwoch (25.5.) auf den Donnerstag
(26.5.) eine erste Aktion zur Waldverteidigung an. Sie folgen damit
einem Hilferuf sicht machtlos fühlender Bobinger Bürger\*innen. Auf der
neu eingerichteten Website bobinger-auwald-bleibt.de informieren sie
Bürger\*innen über ihre Projekte.


UNGEFRAGTES GEWERBEGEBIET

Wie die AZ berichtete [1,2], möchte Herr Nerlinger den Bobinger Auwald
roden, um Platz für ein neues Gewerbegebiet zu schaffen. Nachfrage
besteht nach den letzten öffentlichen Informationen für ein solches
Gebiet indes noch keine [3]. Herr Nerlinger stellt sich mit seinem
Vorhaben gegen die Bundesregierung, die in ihrem Aktionsprogramm
Natürlicher Klimaschutz Kommunen zum Erhalt ihrer Wälder auffordert [4].
„Wälder roden in Zeiten der Klimakrise? Das geht gar nicht!“,
unterstützt Kletteraktivistin Charlotte Lauter (19) die Weisung der
Bundesregierung und verweist darauf, dass Aufforstungsexperimente
spekulativ und 80 bis 100 Jahre benötigen, bis sie in demselben Maße CO₂
binden können und dieselben Funktionen erfüllen wie guter gewachsener
Naturwald [5].


BESETZUNG WIE IM HAMBACHER FORST

„Die Waldzerstörung ist ein Planungsdinosaurier“, kommentiert Lauter die
Rodungspläne von Herrn Nerlinger. „Heute würde das gar nicht mehr
genehmigt werden. Um den Gerichten die nötige Zeit zu geben, planen wir
daher nach dem Vorbild des Hambacher Forsts eine Besetzung des Bobinger
Auwalds. Da wir erst geräumt werden müssen, bevor der erste Baum fallen
kann, können wir damit die Rodung in die Länge ziehen und Bürger\*innen
über die Rodungspläne aufklären.“ Im Hambacher Forst errichteten
Aktivist\*innen 2012 zahlreiche Baumhausdörfer, bevor sechs Jahre später
ein Gericht die Rodungspläne für illegal erklärte. Die polizeiliche
Räumung des verwandten Dannenröder Walds war 2020 der größte
Polizeieinsatz Hessens und kostete Schätzungen zu Folge 150 Millionen
Euro [6]. „Aktuell genießt die Gemeinde Wehringen aufgrund nachhaltiger
Forstarbeit einen guten Ruf. Möchte Herr Nerlinger diesen Ruf wirklich
so sehr beschädigen und mit der größten polizeilichen Waldräumung
Bayerns in Verbindung bringen?“


INFORMATIONSVERANSTALTUNGEN ZUR EINBINDUNG DER ANWOHNER\*INNEN

Für die Nacht vom Mittwoch (25.5.) auf den Donnerstag (26.5.) kündigen
die Aktivist\*innen eine erste vorbereitende Aktion zur Waldverteidigung
an. „Die Waldbesetzung erfolgt dann zum Beginn der Rodungssaison hin“,
erklärt Lauter. „Zuvor werden wir in Informationsveranstaltungen die
Bobinger Anwohner\*innen ihres Auwalds über unsere Pläne detailliert
aufklären und einbinden: welches Holz etwa zum Baumhausbau geeignet ist
und wie sie uns mit Essen unterstützen können. Auch werden wir
öffentliche Kletterworkshops veranstalten, damit auch die Bürger\*innen
vor Ort den Baumhausbau unterstützen können.“ Die Aktivist\*innen teilen
mit, dass sie aktuelle Informationen an Bobingens Bürger\*innen auf der
neu eingerichteten Seite bobinger-auwald-bleibt.de veröffentlichen
werden. Dort sei auch eine Petition vom BUND Naturschutz zum Walderhalt
verlinkt.


AKTIVIST\*INNEN MAHNEN VOR VERLASS AUF SPEKULATIVE
AUFFORSTUNGSEXPERIMENTE

Die Rechtfertigungsversuche von Herrn Nerlinger [1] überzeugen Lauter
und ihre Mitstreiter\*innen nicht: „Herrn Nerlingers spekulative
Aufforstungsexperimente anderswo sind riskant. Immer häufiger versagen
Neupflanzungen. Die von der Rodungswut ausgesparten Biotope benötigen
zwingend den restlichen Wald als wertvolle Pufferzone. Fallen die Bäume,
so auch die Biotope.“ Nach Einschätzung des BUND Naturschutz sind Teile
des Auwalds ökologisch besonders bedeutsam und den Fichtenwald
umgebenden alten Buchen und Eichen sorgten bereits jetzt für
Naturverjüngung. [7]


REFERENZEN

[1] [https://www.augsburger-allgemeine.de/schwabmuenchen/wehringen-bobingen-proteste-wehringens-buergermeister-stellt-auwald-fakten-klar-id62762066.html](https://www.augsburger-allgemeine.de/schwabmuenchen/wehringen-bobingen-proteste-wehringens-buergermeister-stellt-auwald-fakten-klar-id62762066.html)
<br>
[2] [https://www.augsburger-allgemeine.de/schwabmuenchen/kommentar-auwald-proteste-jetzt-ist-zeit-fuer-einen-runden-tisch-id62763421.html](https://www.augsburger-allgemeine.de/schwabmuenchen/kommentar-auwald-proteste-jetzt-ist-zeit-fuer-einen-runden-tisch-id62763421.html)
<br>
[3] [https://web.archive.org/web/20220114115826/https://www.wehringen.de/aktuelles/detail/eintrag/an-der-waldstrasse-entsteht-ein-neues-gewerbegebiet-mit-rund-40000-quadratmetern-flaeche/](https://web.archive.org/web/20220114115826/https://www.wehringen.de/aktuelles/detail/eintrag/an-der-waldstrasse-entsteht-ein-neues-gewerbegebiet-mit-rund-40000-quadratmetern-flaeche/)
<br> (letzter Absatz: interessierte Konzerne sind aufgerufen, sich zum
geplanten Gewerbegebiet zu erkundigen)
<br>
[4] [https://www.bmuv.de/download/dl-aktionsprogramm-natuerlicher-klimaschutz](https://www.bmuv.de/download/dl-aktionsprogramm-natuerlicher-klimaschutz)
<br>
[5] [https://www.handelsblatt.com/technik/energie-umwelt/klima-orakel-wie-viele-baeume-sind-noetig-um-eine-tonne-co2-zu-binden/3201340.html](https://www.handelsblatt.com/technik/energie-umwelt/klima-orakel-wie-viele-baeume-sind-noetig-um-eine-tonne-co2-zu-binden/3201340.html)
<br>
[6] [https://augsburg.klimacamp.eu/danni](https://augsburg.klimacamp.eu/danni)
<br>
[7] [https://augsburg.bund-naturschutz.de/aktuelles/artikel/petition-gegen-neues-gewerbegebiet-der-gemeinde-wehringen](https://augsburg.bund-naturschutz.de/aktuelles/artikel/petition-gegen-neues-gewerbegebiet-der-gemeinde-wehringen)


## Nachtrag vom 26.05.2022

Bezugnehmend auf oben stehende Pressemitteilung folgende Information:

* Die Aktion war erfolgreich.

* Der Beginn eines Traversennetzwerks, mit dem später Lebensmittel und
  Menschen auch dann zwischen den beiden Waldteilen wechseln können,
  wenn am Boden Polizeiabsperrungen die freie Bewegung einschränken.

* Zudem wurde (oberhalb des Lichtraumprofils der Straße) ein Banner
  gespannt, Aufschrift: „Rettet den Wald für unsere Kinder“. Dieses
  klärt Autofahrer\*innen über die Rodungsabsicht von Herrn Nerlinger auf.

* Ort: nahe Tierklinik Dr. Stechele GbR Equopark (Waldstraße 31, 86517
  Wehringen)

* Fotos zur freien Verwendung:
  [www.bobinger-auwald-bleibt.de](www.bobinger-auwald-bleibt.de)
