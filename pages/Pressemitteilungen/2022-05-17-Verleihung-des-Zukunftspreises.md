---
layout: page
title:  "17.05.2022: Verleihung des Zukunftspreises"
date:   2022-05-17 10:00:00 +0100
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
permalink: /pressemitteilungen/2022-05-17-verleihung-des-zukunftspreises/
nav_order: 313
---

*Pressemitteilung vom Augsburger Klimacamp am 17.5.2022*

# Klimacamp Augsburg: Klimacamp Augsburg: Oberbürgermeisterin Weber stellt sich bei Zukunftspreisverleihung in den Mittelpunkt

„Ich, ich, ich“, 24 Mal „Ich“. Das war der Grundton von Eva Webers Rede
zur Verleihung des Zukunftspreises an das Klimacamp Augsburg. Was sie
sich eigentlich zur Aufgabe gemacht hatte: eine Laudatio zu halten, das
heißt eine „im Rahmen eines Festakts gehaltene feierliche Rede, in der
jemandes Leistungen und Verdienste gewürdigt werden“ (Duden). „An
diesem Abend sollte es um ausgezeichnetes Engagement in der Stadt
gehen“, so Janika Pondorf (17) vom Klimacamp. „Wir sind enttäuscht, dass
Oberbürgermeisterin Weber die Laudatio an sich riss, um ihre persönliche
Meinung gegenüber dem Klimacamp erneut an unpassender Stelle Ausdruck zu
verleihen.“ Wäre Frau Weber nicht diesen Sonderweg bei einem der
ausgezeichneten Projekte gegangen, hätte die Presse ausgeglichen über
alle sechs berichten können, so Pondorf. „Wir bitten Mitglieder der
Jury, uns die ursprüngliche Preisrede zukommen zu lassen – wir hätten
uns darüber gefreut zu hören, warum wir geehrt wurden. Kommt dazu auch
gerne ins Camp!“, ergänzt Franziska Pux (27). Vorgesehen war eigentlich,
dass in Form von Eva Webers Stadtratskollege Peter Rauscher (GRÜNE) ein
Jurymitglied den Preis an das Klimacamp verleihen sollte.

Auf Eva Webers Ergänzung des Preises um die Komponente, mit ihr „an
einem gemeinsamen Tisch Platz zu nehmen“, gehen die Aktivist\*innen
ein: „Frau Weber hat seit 22 Monaten ein laufendes Gesprächsangebot von
uns, dem sie täglich nachkommen könnte.“ Andere Politiker\*innen, sowohl
Stadträt\*innen vieler Parteien als auch Bundestagsabgeordnete wie
Claudia Roth, Ulrike Bahr und Volker Ullrich, waren dagegen schon im
ersten Campjahr zu Diskussionsrunden im Camp. „Jedenfalls freuen wir uns
über das Gesprächsangebot und nehmen es gerne an. Die Tagesordnung
können wir gemeinsam ausarbeiten. Dabei sollten wir versuchen, eine
sozial gerechte Augsburger Antwort auf die Klimakatastrophe zu finden.
Kompromisse kann es dabei in Einzelfragen natürlich geben, geleitet von
einem Konsens über die Notwendigkeit auch umfassender Veränderungen –
das gebietet schon die Vorsorgepflicht für kommende Generationen gemäß
§20a Grundgesetz und Artikel 141 der Bayerischen Verfassung“, fügt
Julius Natrup (35) hinzu.

„Unser Versammlungsthema Klimagerechtigkeit geht über eindimensionalen
Klimaschutz weit hinaus, da es die soziale Komponente in den Vordergrund
stellt", so Pondorf. Damit bezieht sich die 17-jährige Holbeinerin
darauf, dass sowohl global gesehen als auch auf kommunaler Ebene ärmere
Menschen von der Klimakrise am Meisten betroffen sind, obwohl sie am
Wenigsten zu ihr beitragen. „Das ist ungerecht! Unsere Lösungsvorschlage
haben daher den Gerechtigkeitsaspekt immer fest im Auge – etwa, wenn wir
von der Bundesregierung fordern, Lebensmittelrettung aus
Supermarktmülltonnen zu legalisieren und stattdessen unternehmerische
Lebensmittelverschwendung zu kriminalisieren. Oder in Augsburg ein
sicheres Radwegenetz zu etablieren – denn von dem aktuellen
Flickenteppich unserer Radwege sind ärmere Leute ohne Auto besonders
betroffen.“
