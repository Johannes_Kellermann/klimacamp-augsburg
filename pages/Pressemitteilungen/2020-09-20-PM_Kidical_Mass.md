---
layout: page
title:  "20.09.2020: Mehr als 200 große und kleine Teilnehmer*innen erfreuten sich an der ersten Augsburger Kidical Mass"
date:   2020-09-20 23:00:00 +0200
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
nav_order: 20
---

*Pressemitteilung vom Augsburger Klimacamp am 20. September 2020*

# Mehr als 200 große und kleine Teilnehmer\*innen erfreuten sich an der ersten Augsburger Kidical Mass

[Fotos](https://www.speicherleck.de/iblech/stuff/fotos-kidical-mass-2020-09-20/)

Am heutigen 20. September eroberten große und kleine Radfahrende die
Straßen von Augsburg, bei der ersten Augsburger Kidical Mass, der
bunten Fahrrad-Demo für alle. Mehr als 200 Teilnehmer\*innen folgten dem
gemeinsamen Aufruf von ADFC, VCD und Fahrradstadt jetzt. Das Augsburger
Klimacamp richtete die Veranstaltung aus, die Augsburger Polizei
begleitete die Demonstration und sicherte die Wege.

„Warum? Weil auch Kinder gerne Radfahren!“, erklärte Paula Stoffels
(18), Aktivistin im Augsburger Klimacamp und Versammlungsleiterin der
Demonstration. „Dieses Wochenende hat gezeigt, wie frei Kinder sich in
ihrer Stadt bewegen könnten, wenn wir ihnen den Platz dafür geben. Genau
das wollten wir erlebbar machen – für die Kinder, aber auch
Politiker\*innen, die endlich entsprechende Entscheidungen fällen
müssen.“, ergänzt Organisatorin Simone Kraus (43) des bundesweiten
Bündnisses „Kinder aufs Rad“.

„Das hat wirklich Spaß gemacht!“, freute sich eine begeisterte
sechsjährige Radfahrerin, als die Fahrradkolonne am Wittelsbacher Park
ankam. Sie würde auch jederzeit wieder mitmachen. Unter den Teilnehmer\*innen
waren auch die Stadträte Peter Hummel (FW), Deniz Anan, Kerstin Kipp (beide Grüne) sowie etwa 10
Mitglieder von DIE PARTEI.

„Wir als Klimacamp sehen unser Hauptanliegen darin, mit Politiker\*innen
das direkte Gespräch zu suchen und Wissenschaftler\*innen für Vorträge
einzuladen.“, fährt Stoffels fort. Die Aktivist\*innen übernähmen aber
auch Verantwortung für die Organisation von Veranstaltungen wie der
Kidical Mass. „Vielleicht wird die nächste Kidical Mass ja sogar von der
Stadt selbst ausgerichtet?“

Die Kidical Mass fand in mehr als 90 Städten Deutschlands statt. Die
Kidical Mass fordert Tempo 30 innerorts und sichere Schulradwegenetze
als wichtiger Bestandteil durchgängiger, engmaschiger Radwegenetze in
den Städten. Im Umfeld von Schulen sollen flächendeckend Fahrradstraßen
und als Sofortmaßnahmen Schulstraßen nach Wiener Vorbild eingerichtet
werden.

**Bemerkung.** In einer früheren Version dieser Pressemitteilung vergaßen wir
leider, die Teilnahme von Kerstin Kipp zu nennen.
