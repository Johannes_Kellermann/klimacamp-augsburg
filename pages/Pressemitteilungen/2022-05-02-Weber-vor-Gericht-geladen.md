---
layout: page
title:  "02.05.2022: Klimacamper*innen laden Oberbürgermeisterin Eva Weber vor Gericht"
date:   2022-05-02 10:00:00 +0100
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
permalink: /pressemitteilungen/2022-05-02-weber-vor-gericht-geladen/
nav_order: 310
---

*Presseinformation vom Augsburger Klimacamp am 2.5.2022*

# Klimacamper\*innen laden Oberbürgermeisterin Eva Weber vor Gericht

Am 8.8.2021 hingen wir um 10:00 Uhr ein Banner ans Rathaus (Fotos zur
freien Verwendung angehängt). Das Banner klärte über die jährlichen
CO2-Emissionen der Stadt Augsburg seit 1990 auf und zeigte eine Prognose
der zukünftigen Emissionen auf Basis der gegenwärtigen Entwicklung sowie
den zur Einhaltung der 1,5°-Grenze nötigen Reduktionspfad auf. Für die
jüngeren Jahre mussten wir dazu verschiedene Quellen kombinieren, da die
Stadt diese nicht übersichtlich selbst zusammenstellt. Das Banner wurde
eine halbe Stunde später von der Stadt entfernt.

Nach dem Willen der Staatsanwaltschaft soll einer von uns, Kim Schulz (25),
dafür nun 1.600 € (40 Tagessätze à 40 €) Strafe zahlen. Die mündliche
Verhandlung am Amtsgericht findet diesen Mittwoch (4.5.2022) um 9:00 Uhr
in Sitzungssaal 146 (Gögginger Straße 101) statt.

a) Dies wird ein besonderes Verfahren, das als politisches Verfahren
geführt und verteidigt wird.

b) In dem Fall geht es um eine Banner-Aktion wie zahlreiche andere (auch
solche, die am Rathaus stattfanden), die größtenteils unkriminalisiert
blieben, hier jedoch wurde ein Strafbefehl über 1.600 € erlassen. Wir
wissen nicht, was der Grund hierfür ist. Ein paar von uns spekulieren,
dass es mit der transparenten Aufklärung über die städtischen Emissionen
zu tun haben könnte.

c) Wir beantragten die Vorladung von Oberbürgermeisterin Eva Weber. Als
Zeugin muss sie vor Gericht die Wahrheit sagen; Schulz wird sie zur
Befeuerung der Klimakrise durch die Stadt Augsburg befragen.
