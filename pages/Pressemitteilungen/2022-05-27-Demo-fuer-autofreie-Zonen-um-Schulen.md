---
layout: page
title:  "27.05.2022: Demo für autofreie Zonen um Schulen"
date:   2022-05-27 10:00:00 +0100
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
permalink: /pressemitteilungen/2022-05-27-demo-fuer-autofreie-zonen-um-schulen/
nav_order: 318
---

*Pressemitteilung von Augsburger Verkehrswendeaktivist\*innen am 27.05.2022*

Erstmals auch an der Ulrichschule, auf direkten Wunsch der Schulleitung:
Aktivist\*innen wandeln Hallstraße temporär in autofreie Zone um und
fordern autofreie Zonen rund um Schulen --
Aktivist\*innen rufen den Freitag zum Auto-Frei-Tag aus und nehmen die
Verkehrswende auf der Hallstraße selbst in die Hand

Dies soll ab sofort regelmäßig im Rahmen des „Auto-Frei-Tag“
stattfinden.

Aktivist\*innen hinter dem Augsburger Verkehrswendeplan nutzten am
heutigen Freitag (27.5.2022) das Versammlungsrecht, um temporär eine
autofreie Zone umzusetzen: für die Zeitdauer der beiden Pause des
Holbein-Gymnasiums (09:30-09:50 und 11:20-11:40) und der Ulrichschule
(10-11 Uhr) wird die Hallstraße durch die Versammlungsfreiheit für den
Autoverkehr gesperrt.

Eine solche Aktion fand auch schon am vorletzten Freitag (13.5.2022) im
Rahmen der Vorstellung des Verkehrswendeplans und dem darauffolgenden
Dienstag (17.05.2022) als Reaktion auf einen Unfall in der Hallstraße
statt, so Florian Lenz (21). „Wir zeigen heute, dass eine Sperrung der
Hallstraße für Autos möglich und notwendig ist.“ 

Seit mittlerweile mehreren Jahrzehnten ist eine autofreie Hallstraße im
Gespräch; es ist ein lang gehegtes Begehren der Schulfamilie ums
Holbein-Gymnasium. „Daher zeigen wir direkt während der Pausen des
Holbein-Gymnasiums, wie die autofreie Hallstraße aussehen könnte: als
sicherer Schulweg, als Freiraum für Schüler\*innen und als Bestandteil
einer zentralen autofreien Fahrradstraßenverbindung durch die
Innenstadt“, so Ute Grathwohl (49).  

„Die Zeit des ungezügelten Autoverkehrs vor Schulen ist vorbei“, erklärt
Ute Grathwohl (49), die sich beim Verkehrswendeplan insbesondere zum
Thema sichere Schulwege einbrachte. „Als Mutter ist mir dieses Thema ein
besonderes Herzensanliegen.“ Auch bei der Roten-Tor-Schule kam es erst
vor zwei Wochen zu einem Unfall, bei dem ein Grundschüler vor der Schule
von einem Auto erfasst und verletzt wurde. 

Die Aktivist\*innen kündigten an, fortan regelmäßig Schulstraßen mittels
Versammlungsrecht temporär autofrei umzugestalten, unter anderem im
Rahmen des Auto-Frei-Tags, im Rahmen dessen Straßen vor Augsburger
Schulen temporär für den Autoverkehr gesperrt werden sollen. „Dazu
ermächtigen wir auch die Schüler\*innen selbst durch
Versammlungsrecht-Workshops. Solange die Stadt keine Sofortmaßnahmen wie
im Verkehrswendeplan vorgeschlagen umsetzt und einseitig das
Partikularinteresse Auto fördert, bleibt Verkehrswende Handarbeit“,
erklärt Grathwohl. Selbst der ADAC fordere, dass Kinder zu Fuß oder per
Fahrrad unterwegs sein sollen, um das frühzeitig zu lernen – und dass
deshalb Elterntaxis, falls es sie noch gibt, mindestens 250m von
Kindergärten und Grundschulen wegbleiben, so die Aktivist\*innen. [1]

Grathwohl bezieht sich damit auch auf die seit Jahren kontinuierlich
ansteigenden Autozahlen in Augsburg [2]. Vor allem die für
Fußgänger\*innen mit geringer Körpergröße – klassischerweise Kinder im
Kita- und Schulalter – besonders gefährlichen großen SUV sind in immer
größerer Zahl auf den Straßen unterwegs. „Hier wäre die Politik gefragt,
durch einen günstigen und ausgebauten öffentlichen Personennahverkehr
attraktive Alternativen zu schaffen.“ Die Aktivist\*innen schlagen dafür
im Verkehrswendeplan ein Ausbau des Tramliniennetzes, etwa nach Hochzoll
Süd und Richtung Hammerschmiede vom Hauptbahnhof kommende durch die
Karlstraße und von Göggingen über Messe zum Innovationspark oder, wie
schon lange geplant, nach Haunstetten Süd, vor.
„Kurzfristig möglich ist die Etablierung von Schnellbussen auf den
Bundesstraßen mit enger Taktung im AVV-Gebiet zur Verknüpfung von
DB-Bahnhöfen, Straßenbahnästen und Randzielen.“

Die Stadt sei dabei in der Pflicht, öffentlichkeitswirksam entsprechende
Fördermittel bei Bund und Land einzufordern, sowie übergangsweise von
Augsburgs Unternehmen eine Nahverkehrsabgabe zu erheben. „Das ist nur
fair, schließlich profitieren unsere Unternehmen von einer guten
Mobilität ihrer Kund\*innen und Beschäftigen.“

[1] [https://www.adac.de/verkehr/verkehrssicherheit/kindersicherheit/schulweg/elterntaxi-hol-bringzonen/](https://www.adac.de/verkehr/verkehrssicherheit/kindersicherheit/schulweg/elterntaxi-hol-bringzonen/)
<br>
[2] [https://www.augsburg.de/fileadmin/user_upload/buergerservice_rathaus/rathaus/statisiken_und_geodaten/statistiken/2020Jahrbuch_Internet.pdf](https://www.augsburg.de/fileadmin/user_upload/buergerservice_rathaus/rathaus/statisiken_und_geodaten/statistiken/2020Jahrbuch_Internet.pdf), Abschnitt 7.02


ÜBER DEN VERKEHRSWENDEPLAN

Der Verkehrswendeplan ist der erste umfassende Konzeptentwurf für eine
Umgestaltung des öffentlichen Raums im Sinne einer Verkehrswende. Neben
fünf übergeordneten Zielen formuliert er zahlreiche konkrete Vorschläge
für Augsburgs Straßenraum.

Ausführliche Homepage zum Verkehrswendeplan:
[https://www.verkehrswende-augsburg.de](https://www.verkehrswende-augsburg.de)
Flyer zum Verkehrswendeplan (1. Auflage, Stand Mai 2022):
[https://www.verkehrswende-augsburg.de/assets/Flyer%20Verkehrswendeplan%20Augsburg.pdf](https://www.verkehrswende-augsburg.de/assets/Flyer%20Verkehrswendeplan%20Augsburg.pdf)


ÜBER DAS KONZEPT DER NAHVERKEHRSABGABE

(Quelle: [https://projektwerkstatt.de/index.php?domain_id=40&p=14668)](https://projektwerkstatt.de/index.php?domain_id=40&p=14668)

Bei der Nahverkehrsabgabe (versement transport) wird die französische
Taxe Versement de Transport (VT) als Vorbild genommen, die Kommunen ab
20.000 Einwohnern zweckgebunden zur ÖPNV-Finanzierung erheben können.
Das Steueraufkommen kann investiv und konsumtiv verwendet werden. Die
französische Nahverkehrsabgabe ist von Arbeitgebern mit mehr als zehn
Mitarbeitern und vom Einzelhandel als Nutznießer des ÖPNV-Angebots zu
entrichten. Die Nahverkehrsabgabe ist an die Lohnsumme gekoppelt und der
Steuer-Höchstsatz orientiert sich an der Einwohnerzahl der Kommune.

* Abgaben entweder pauschal für alle Gewerbetreibenden, die vom
  kostenlosen Personenverkehr profitieren (Handel, Dienstleister,
  Hotels, Gaststätten, Touristik usw.) oder speziell für die Anlieger an
  Linien, wenn Haltestellen z.B. an öffentlichen Einrichtungen,
  Geschäften und anderen Vielfachzielen eingerichtet werden. So wird in
  Frankreich der kostenlose Nahverkehr finanziert. Dafür nötig ist eine
  Ermächtigung für Kommunen, solche Nahverkehrsabgaben erheben zu
  können.

* Integration bisheriger Kund\*innen- und Sonderlinien in die
  Freifahrnetze. So bringen z.B. im ländlichen Bereich Supermärkte
  Solche Linien können in den Nulltarifs-Fahrplan integriert und der
  Handel an den Kosten beteiligt werden.
