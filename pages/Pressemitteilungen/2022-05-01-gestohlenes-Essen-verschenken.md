---
layout: page
title:  "01.05.2022: Klimaaktivist*innen verschenken gestohlenes Essen: Essen-Retten-Aktion vor Klimacamp"
date:   2022-05-01 10:00:00 +0100
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
permalink: /pressemitteilungen/2022-05-01-gestohlenes-essen-verschenken/
nav_order: 309
---

*Pressemitteilung vom Augsburger Klimacamp am 1.5.2022*

# Klimaaktivist\*innen verschenken gestohlenes Essen: Essen-Retten-Aktion vor Klimacamp

**KORREKTUR**<br>
Eine kleine Korrektur zur morgigen Essen-Retten-Aktion,
die in der unten stehenden Pressemitteilung beschrieben wurde:

Die Hehlerei muss auf 15:30 Uhr vorgezogen werden.
Vielleicht können wir eine zweite Verschenkaktion
auch zum ursprünglichen Termin 17:00 Uhr anbieten,
das hängt auch von dem Verhalten der Behörden ab.

Ferner möchten wir auf den unmittelbaren Klimabezug der Aktion hinweisen.
Die Lebensmittel, die Supermarktketten und andere Großkonzerne wegwerfen,
verursachen in ihrer Herstellung trotzdem CO2- und Methan-Emissionen.
Völlig unnötigerweise.
Ein Gesetz gegen unternehmerische Lebensmittelverschwendung
ist ein einfaches und wirksames Mittel, um diese Emissionen einzusparen.

**Ursprüngliche Pressemitteilung**

Am Dienstag, den 3.5.2022 verschenken Aktivist\*innen ab ~~17:00~~ 15:30 Uhr
gerettete Lebensmittel vor dem Klimacamp am Moritzplatz. In den
vergangenen Monaten sorgte diese Art Aktion bereits in Nürnberg für
Aufsehen, wo deswegen Jesuitenpater Jörg Alt polizeilich vernommen wurde [1].
Jetzt bringen die Aktivist\*innen dieses Konzept auch nach Augsburg.

Die verschenkten Lebensmittel wurden von den Aktivist\*innen in den
Nächten zuvor aus Mülltonnen von Supermärkten in Augsburg gerettet. Wer
sich hierbei erwischen lässt, kann wegen Diebstahl oder sogar schwerem
Diebstahl strafrechtlich verfolgt werden. „Trotz des Risikos
strafrechtlich verfolgt zu werden gehen wir containern. Wir können nicht
tatenlos mit ansehen, dass gute Lebensmittel im Müll landen anstatt
gegessen zu werden“, so Charlotte Lauter (19).

„Supermärkte werfen Unmengen guter Lebensmittel weg“, so Lauter. „Diese
unnötig produzierten Lebensmittel verursachen trotzdem CO2-Emissionen in
ihrer Herstellung. Wenn es in Deutschland wie in Frankreich ein Gesetz
gegen unternehmerische Lebensmittelverschwendung gäbe, könnte
Deutschland ganz leicht CO2-Emissionen einsparen!“, erklärt Lauter ihr
Engagement. „Wir fragen uns, wieso es in Deutschland nicht lange schon
so ein Gesetz gibt. Es entspricht doch ganz unserer Erziehung, gutes
Essen nicht wegzuwerfen, und würde der Bundesrepublik auch keinerlei
Kosten verursachen.“

[1] [https://www.br.de/nachrichten/bayern/bis-die-polizei-kommt-pater-aus-nuernberg-rettet-lebensmittel,SsFhNiM](https://www.br.de/nachrichten/bayern/bis-die-polizei-kommt-pater-aus-nuernberg-rettet-lebensmittel,SsFhNiM)

HINWEIS
Wir stehen für journalistische Begleitung beim vorbereitenden Containern
zur Verfügung. Wir möchten jedoch explizit darauf hinweisen, dass es
sich dabei um eine Straftat handelt. In Deutschland muss man zwar nur
bei gewissen besonders schweren Straftaten die Behörden kontaktieren,
sobald man von ihrer Planung erfährt (§ 138 StGB). Trotzdem empfiehlt
sich eine Abklärung mit einer Rechtsabteilung.
