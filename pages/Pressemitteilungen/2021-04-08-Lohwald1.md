---
layout: page
title:  "08.04.2021: Ziviler Ungehorsam: Klimagerechtigkeitsaktivist*innen hängen Banner zum Schutz des Lohwalds auf"
date:   2021-04-08 07:00:00 +0200
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
nav_order: 209
---

*Pressemitteilung von Wald statt Stahl am 8. April 2021*

# Ziviler Ungehorsam: Klimagerechtigkeitsaktivist\*innen hängen Banner zum Schutz des Lohwalds auf

Sperrfrist Freitag (9.4.2021) 6:00 Uhr
{: .label .label-red }

**[ Diese Pressemitteilung gab nicht das Klimacamp, sondern Wald statt Stahl
heraus. Sie findet sich auf dieser Seite wegen der thematischen Relevanz der
Bequemlichkeit halber trotzdem. ]**

In der Nacht von Donnerstag (8.4.2021) auf Freitag (9.4.) hängten
Klimagerechtigkeitsaktivist\*innen des neu gegründeten Aktionsbündnisses
"Wald statt Stahl" in und um Meitingen mehrere Protestbanner zum Schutz
des Lohwalds auf. Der Lohwald ist als Bannwald durch das bayerische
Waldgesetz geschützt, soll nun aber der Erweiterung der Lech-Stahlwerke
(LSW) zum Opfer fallen. Laut dem BUND Naturschutz Bayern e.V. ist er
ökologisch besonders schützenswert [1] und erfüllt wichtige Lärm- und
Emissionsschutzfunktionen. Ende Februar demonstrierten knapp 600
Bürger\*innen mit einer Menschenkette für den Erhalt des Lohwalds, BR und
AZ berichteten [2,3].

Die Aktionen von Wald statt Stahl richten sich nicht gegen die
Arbeiter\*innen des Stahlwerks, sondern dienen dem Ziel, die
zukunftsfeindlichen Pläne der Stahlwerksführung zu verhindern. "Besitzer
Max Aicher sind die Bedürfnisse der lokalen Bevölkerung und zukünftiger
Generationen gleichgültig", so Gwendolyn Rautenberg (19). "Im Zeitalter
der Klimakrise einen Bannwald für einen ohnehin sehr CO₂-intensiven
Betrieb roden? Das ist grotesk! Wir fordern Bürgermeister Michael Higl
und die CSU auf, diesen Wahnsinnsplan zu stoppen und stattdessen eine
nachhaltige Erweiterung des Lohwalds in die Wege zu leiten!" Die
Aktivist\*innen kündigten weitere Aktionen des zivilen Ungehorsams gegen
die Rodungspläne an.

Wenn nötig, werden die Aktivist\*innen von Wald statt Stahl den Lohwald
nach dem Vorbild des Hambacher Forsts und des Dannenröder Walds besetzen
und dort ein Baumhausdorf errichten. "Wenn die CSU keinen Respekt vor
Ökosystemen hat und und ihren unbezifferbaren Wert geringschätzt, dann
ändern vielleicht die  Polizeieinsatzkosten ihre Profitkalkulationen",
so Rautenberg weiter. Der Einsatz im Dannenröder Wald kostete 150
Millionen Euro [4] – "genug, um den gesamten öffentlichen
Personennahverkehr in Hessen für vier Monate kostenlos zu machen" [5].

[1] https://augsburg.bund-naturschutz.de/fileadmin/kreisgruppen/augsburg/Ortsgruppen/Meitingen/Stellungnahme_Lohwald_.pdf<br>
[2] https://www.br.de/nachrichten/bayern/bannwaldrodung-fuer-lech-stahlwerke-umweltschuetzer-demonstrieren,SQK6YTp<br>
[3] https://www.augsburger-allgemeine.de/augsburg-land/Hunderte-protestieren-lautstark-fuer-den-Erhalt-des-Lohwalds-bei-Meitingen-id59211951.html<br>
[4] https://augsburg.klimacamp.eu/danni<br>
[5] https://klimaschutz.madeingermany.lol/


## Über das Aktionsbündnis

"Wald statt Stahl" ist ein Zusammenschluss von
Klimagerechtigkeitsaktivist\*innen diverser Bewegungen aus Meitingen,
Augsburg und weiteren Städten. Sie eint das Ziel, mit Aktionen und
zivilem Ungehorsam den Meitinger Lohwald zu erhalten und seine
Erweiterung einzufordern. Am 9.4.2021 fand die erste Aktion des
Aktionsbündnisses statt, weitere werden folgen. "Wald statt Stahl"
agiert unabhängig vom Bannwald-Bündnis Unterer Lech, welches sich
ebenfalls für den Erhalt des Lohwalds einsetzt.
