---
layout: page
title: "Awarenessleitlinien"
parent: "Mitmachen"
has_children: true
has_toc: false
permalink: /awareness/
nav_order: 10
---

# Awarenessleitlinien fürs Klimacamp Augsburg


Das Camp kann als Versuch einer kleinen gelebten Utopie gesehen werden; diese befindet sich jedoch immer noch inmitten der realen Welt und ist nicht frei von gesellschaftlichen Machtverhältnissen. Prozesse, die sich in dieser Welt und dem kapitalistischen, patriarchalen System in dem wir leben abspielen, lassen sich zwangsläufig auch in Strukturen wie dem Camp beobachten. Es gilt diese Prozesse zu erkennen und ihnen gemeinsam entgegenzuwirken.
Damit sich alle möglichst Wohlfühlen können bei ihrem Aufenthalt im Camp, ist es wichtig, dass wir gut aufeiander achten und gegenseitig unsere persönlichen Grenzen respektieren. Im Camp sollte **kein** Platz sein für **sexistisches, rassistisches, ableistisches, klassistisches** oder anderes **diskriminierendes** Verhalten. Dazu gehört sowohl das eigene Verhalten zu beobachten und zu hinterfragen, sowie sich mit betroffenen Personen zu solidarisieren wenn ihr Zeug\*in solchen Verhaltens werdet.
Das eigene Verhalten zu hinterfragen heißt auch sich mit der eigenen Rolle in der Gesellschaft auseinanderzusetzen und die eigenen jeweiligen Privilegien kritisch zu hinterfragen. Sowie anzuerkennen, dass diese nicht bei allen Menschen, die sich im Camp aufhalten, die gleichen sind.

Wir wünschen uns einen Raum, in dem ein respektvolles Miteinander herrscht und grundsätzlich eine Kommunikation auf Augenhöhe stattfindet. Das beinhaltet das Verwenden einer Sprache, die alle Menschen gleichermaßen mitdenkt, mittels **gendergerechter, barrierearmer Wortwahl**. Dabei wäre es schön, wenn mensch nicht vergisst, dass die meisten von uns nicht mit gendergerechter Sprache aufgewachsen sind und das Ablegen von patriarchal geprägtem Sprachgebrauch ein wenig dauern kann. 
Ein fehlerfreundliches Umfeld trägt zu einer entspannteren Lernatmosphäre für uns alle bei und öffnet den Raum auch für Menschen, die vielleicht bisher wenig Berührungspunkte mit bestimmten Themen der linken Szene hatten.

Um auf alle Menschen und ihre unterschiedlichen Bedürfnisse Rücksicht nehmen zu können, organisieren wir uns selbst zum Beispiel in Plena. Dort werden Entscheidungen basisdemokratisch getroffen, das bedeutet, dass möglichst ein **Konsens** gefunden werden soll, oder Mehrheitsentscheidungen mit Bedenken oder sogar Vetos reguliert werden können, um Minderheiten denselben Raum zu geben. 

Im Klimacamp ist es laut Auflage, aber auch aus Rücksicht auf eure Mitcamper\*innen **nicht gestattet, Alkohol** zu konsumieren, sowie **nur am Rand des Camps gestattet zu rauchen**. Wir bitten alle, diese Entscheidung zu respektieren und umzusetzen, damit auf die Bedürfnisse aller geachtet wird. Uns ist bewusst, dass manche Menschen dem nicht nachkommen können - lasst uns versuchen auch das zu respektieren, und mit solchen Problemen besser umzugehen als es oft in unserer Gesellschaft getan wird.

Nicht nur der Konsum von Genussmitteln kann ein triggerndes Thema sein. So unterschiedlich Menschen sind, so verschieden können auch ihre Traumata, Bedürfnisse, Ängste und Gefühle sein, und mensch sieht es ihnen meist nicht an. Denke also bevor du über etwas redest noch einmal nach, ob das Thema vielleicht sensible Punkte berühren könnte, und weise deine Mitmenschen darauf hin. Hilfreiche Formulierungen dabei können sein:

* Inhaltshinweis: ich werde über... sprechen.

* Ich werde jetzt über ... sprechen, wenn du dich dabei unwohl fühlst, dann kannst du gerne aus dem Gespräch raus gehen.

* Bevor ich weiter rede will ich darauf hinweisen, dass ich auch über ... sprechen werde.

Die Formulierung "Inhaltshinweis" (engl.: "Content Note") ist neutraler als die vielleicht bekanntere "Triggerwarnung" (engl: "Trigger Warning/ Content Warning"), und wird von uns bevorzugt, da die Worte "Trigger" und "Warnung" bereits negative Gefühle auslösen, oder einen offenen Umgang mit diesen Themen verhindern können.

Auch an dieser Stelle wollen wir nochmals darauf hinweisen, fehlerfreundlich zu bleiben. Es kann immer vorkommen, dass Menschen einmal Bedürfnisse anderer übergehen oder eine Inhaltswarnung vergessen. Schön wäre es, dann nicht die Schuld bei irgendwem zu suchen, sondern die Situation zu nutzen, um Wissen weiterzugeben und zu lernen. Natürlich haben betroffene Personen in so einem Moment das absolute Recht dazu, aus der Situation heraus zu gehen, sich Hilfe, Ansprechpersonen oder einen Schutzraum zu suchen. Aber wie bereits gesagt, befindet sich unsere kleine Utopie immer noch inmitten einer toxischen Gesellschaft, und es kann immer vorkommen, dass auch nicht lernbereite Menschen die Grenzen von Menschen im Camp überschreiten oder dies auch absichtlich tun. Deshalb ist es für uns wichtig zu betonen, dass wir natürlich alle gemeinsam versuchen das Camp als einen allgemeinen Schutzraum (engl.: safe space) für alle Menschen mit ihren unterschiedlichen Bedürfnissen zu gestalten, aber es nicht garantieren können, dass Grenzüberschreitungen nicht vorkommen werden. Wir sehen das Camp also eher als **möglichst sicheren Raum (engl.: safer space)** an, als als Schutzraum.

Weißt du mal nicht mehr weiter in Konfliktsituationen, mit Gefühlen, brauchst jemensch zum Reden/ Zuhören, möchtest dich mit traumatisierenden oder belastenden Erfahrungen an jemensch wenden oder dir Hilfe suchen, dann wende dich an die **Awareness AG**. Auf Telegram erreichst du uns über **@AwarenessAux_bot** und findest weitere Infos unter dem **#awareness** in unseren Gruppen.
Manchmal sind auch Menschen des Awareness-Teams **vor Ort im Camp** und **ansprechbar**. Aber auch sie brauchen mal Zeit für sich und haben **nicht immer Awareness-Kapazitäten** wenn sie im Camp sind.
Manchmal fällt es Menschen nicht leicht, ihre Bedürfnisse auszusprechen, daher sehen wir es als **Gruppenaufgabe**, aufmerksam und empathisch zu sein, und auch proaktiv auf Menschen zu zugehen und einfach nachzufragen.

Dieser Text wurde von Menschen mit akademischem Hintergrund verfasst. Wir haben uns bemüht, den Text möglichst barrierearm zu schreiben, aber auch das kann uns nicht immer perfekt gelingen. Bei Fragen oder Anmerkungen könnt ihr uns gerne unter @AwarenessAux_bot kontaktieren. Ausserdem findet ihr hier ein Glossar, in dem einige Begriffe erklärt werden: [Awareness-Glossar](/awareness-glossar)
