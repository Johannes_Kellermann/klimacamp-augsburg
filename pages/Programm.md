---
layout: page
title: Programm & Tagebuch
permalink: /programm/
nav_order: 30
has_children: true
has_toc: false
---

<details>
  <summary>Allgemeine Informationen ein-/ausblenden</summary>
  <div>
    Hier findest du unser Workshop- und Vortragsprogramm im Klimacamp.
    Daneben gibt es rund um die Uhr die Möglichkeit,
    mit uns ins Gespräch zu kommen und Aktionen zu planen.
  </div>
  <div>
    Nicht das gesamte Programm wird über diese Webseite angekündigt.
    Wenn du Zeit hast und in der Nähe des Camps bist,
    schau doch an unserem Aushang vorbei.
    Eine ganze Reihe von Workshops
    wird mit relativ kurzer Vorlaufzeit spontan organisiert.
  </div>
  <div style="font-style: italic;">
    Wir gestalten das Camp alle gemeinsam.
    Deine Workshopidee ist willkommen!
  </div>
  <div>
    ℹ️ Aktuelle Informationen auch über Telegram: <a href="https://t.me/klimacamp_augsburg">https://t.me/klimacamp_augsburg</a>
  </div>
  <hr>
</details>


# Anstehendes Programm

*Hier soll in Kürze wieder über geplante Aktionen berichtet werden.
Derzeit wird an an zahlreichen Projekten,
wie dem* Verkehrswendeplan Augsburg
*(siehe [https://www.verkehrswende-augsburg.de/](https://www.verkehrswende-augsburg.de/))
und dem Schutz des Bobinger Auwaldes
(siehe [https://www.bobinger-auwald-bleibt.de/](https://www.bobinger-auwald-bleibt.de/))
gearbeitet.*


## Freitag 01.07.2022 -- **Tag 731** -- Zweijähriges Jubiläum

Das Klimacamp wurde am 01.07.2020 gegründet.
Seitdem hat das Klimacamp mehrere Phasen durchlaufen
und dient heute verschiedenen Zwecken:

* Das Klimacamp macht auf die drohende Klimakatastrophe aufmerksam
  und informiert die Bevölkerung.
  Besonders wertvoll ist, dass das Klimacamp
  nicht nur diejenigen Menschen erreicht,
  die sich sowieso bereits für das Thema interessieren,
  sondern alle Menschen,
  die absichtlich oder zufällig am Camp vorbeikommen.
* Das Klimacamp übt Druck auf die Politik aus.
  Dreißig Jahre nach dem ersten Weltklimabericht
  sind Teile der Politik nun endlich so weit,
  dass sie bei der Fahrt in Richtung Abgrund
  vorsichtig den Fuß vom Gaspedal nehmen.
  Jetzt müssen sie noch überzeugt werden, die Bremse zu betätigen.
  Wir sind überzeugt, dass Augsburg mehr tun kann und mehr tun sollte.
* Das Klimacamp erleichtert die Vernetzung
  zwischen Klimagerechtigkeitsaktiven.
  Auch dient es als Anlaufpunkt für Menschen,
  die sich für Klimagerechtigkeitsaktivismus interessieren.
* Das Augsburger Klimacamp dient Klimagerechtigkeitsaktiven
  weit über Schwaben hinaus als Vorbild.

Inzwischen wird das Klimacamp sogar von Münchner Merkur / tz
als *Schatz in Bayern* bezeichnet,
den man mit dem 9-Euro-Ticket besuchen sollte.
<br> Quellen:
[Münchner Merkuer](https://www.merkur.de/bayern/oepnv-tipps-bayern-9-euro-ticket-ziele-reise-ausfluege-fahrten-91576981.html)
/
[tz](https://www.tz.de/muenchen/stadt/reise-deutsche-bahn-muenchen-9-euro-ticket-bayern-tipps-erkundung-deutschland-91576562.html?itm_source=story_detail&itm_medium=interaction_bar&itm_campaign=share)

Diese Errungenschaften gilt es zu feiern.
Das zweijährige Jubiläum werden wir voraussichtlich
nicht so sang- und klanglos verstreichen lassen,
wie beispielsweise das sechshunderttägige Jubiläum
oder das siebenhundertägige Jubiläum.
Weitere Informationen werden wir zu gegebener Zeit veröffentlichen.

* 11—16: Straßenkreidemalen (bei gutem Wetter) (Vorsicht [überzogene
  Hausdurchsuchungen](https://www.xn--pimmelgate-sd-7ob.de/kreidebleich/))
* 11—16: Aktivistisches Kinder- und Erwachsenenschminken
* 14—15: Energiewende-Workshop
* 15—16: Physikalische Grundlagen der Klimakrise: Fakten und Mythen
* 15—16: Fahrradschrotttrommeln für Anfänger\*innen
* 15—17: Kleidertausch-Party
* **16—17:30: Demo für Klimagerechtigkeit**
* 18—19: Rundgang durchs Camp: Forderungen, Rechtliches, klimaaktivistischer
  Ansatz und bisherige Erfolge (für Einsteiger*innen und Passant*innen) —
  insbesondere: verschiedene Formen von Klimaaktivismus
* 19—20: Konsumkritik-Kritik: von der Mär der angeblichen Macht der
  Verbraucher\*innen
* 20—21: Kletterworkshop in Vorbereitung der Besetzung des [Auwalds bei
  Bobingen](https://www.bobinger-auwald-bleibt.de/)
* 21—22: Einführung in die Augsburger Lokalpolitik
* 22—23: Reflexionsrunde zwei Jahre Klimacamp Augsburg
* 23—24: Aktivistisches Sterne schauen

Mit veganem Abendessen und musikalischer Umrahmung durch
[Wollstiefel](https://wollstiefel.bandcamp.com/)!


## Donnerstag, 30.6.2022
`20:00 Uhr` **Erfahrungen einer Ackerbesetzung**

Palû berichtet, wie sie in Neu Eichenberg eine 80ha große Ackerfläche besetzten, und so den Bau eines gigantischen Logistikzentrums verhinderten.

Dazu ein kleiner Exkurs zu emotionalen Dynamiken in der Klimabewegung.

`20:30 Uhr` **Gesellschaftsformenvergleich aus Klimagerechtigkeitsperspektive —
Podiumsdiskussion**

Demokratie, Kommunismus und Anarchie im direkten Vergleich.


---

👋 Herzliche Einladung zu allen Veranstaltungen, insbesondere an alle,
die mit dem Gedanken spielen, klimaaktivistisch aktiv zu werden. Ihr
seid willkommen und trefft im Klimacamp und in den anderen Augsburger
Strukturen auf motivierte Menschen, mit denen Veränderung möglich wird!

---


# Tagebuch

## Mittwoch 01.06.2022 -- **Tag 701**

### Ortsbegehung des Bobinger Auwalds

Die Grünen vom Augsburger Land veranstalten
eine Ortsbegehung des Bobinger Auwalds.<br>
Link zur Veranstaltung: [https://gruene-augsburgland.de/ortsbegehung-wehringer-auwald/](https://gruene-augsburgland.de/ortsbegehung-wehringer-auwald/)

Auch vom Klimacamp bricht eine Gruppe gegen 17:30
mit öffentlichen Verkehrsmittel zur Ortsbegehung auf.

Weitere Informationen zum Bobinger Auwald gibt es auf
[https://www.bobinger-auwald-bleibt.de/](https://www.bobinger-auwald-bleibt.de/).


## Dienstag 31.05.2022 -- **Tag 700**

Heute war der siebenhunderste Tag des Klimacamps.
Das Klimacamp besteht nun seit einhundert Wochen.
(Da das Klimacamp am ersten Tag erst um 19 Uhr gegründet wurde,
sind es streng genommen erst morgen (= Mittwoch) um 19 Uhr
genau einhundert Wochen.)

Ist das ein Grund zur Feier,
da es die Entschlossenheit, Organisation und den Durchhaltewillen
der Klimagerechtigkeitsbewegung in Augsburg zeigt,
oder ein Grund zur Trauer,
da wir nach so langer Zeit immer noch den Eindruck haben,
dass die Stadt auch weiterhin Druck von Außen braucht,
um das Richtige zu tun?


## Sonntag 29.05.2022 -- **Tag 698**

### ~~Vormittags: Fahrraddemo?~~

~~Wir planen eine Fahrraddemo.
Leider sind wir uns mit dem Ordnungsamt
noch nicht so ganz über die Route einig.
Eingeplant ist auch ein Abschnitt über die A8.
Weitere Informationen gibt es hier,
sobald sich das Gericht mit unserer Klage befasst hat.~~

Die Fahrraddemo findet nicht so wie geplant statt.
Doch kein Grund zur Sorge:
Die nächste spannende Fahrraddemo kommt bestimmt.


### 14:00 bis 17:00 Straßenfest auf der Karlstraße

Unter dem Motto *Tag der neuen Energie*
findet von 14 Uhr bis 17 Uhr ein Straßenfest
auf der Karlstraße statt.
Bei dieser angemeldeten Demonstration wird die Karlstraße gesperrt.
Dann ist Platz für Bänke, Pavillons, Stände, Musik, Pflanzen,
Spielzeug, Getränke und Essen.
Das Straßenfest ist eine Gemeinschaftsaktion eines Bündnisses
aus zahlreichen Gruppen – darunter das Klimacamp,
Fridays for Future, Students For Future, Extinction Rebellion,
Rhythms of Resistance, Gemeinwohl-Ökonomie,
Ohne Kerosin Nach Berlin, Augsburg erdgasfrei und
Psychologists for Future.
Die Aktion verbindet die notwendigen Veränderungen
in den beiden Bereichen *Mobilität* und *Energie*
mit den Forderungen nach Klimagerechtigkeit.

Programm:

* 14:00 Uhr  Aufbau
* 14:40 Uhr  Aktionen: T-Shirt Druck, Kreidebild, Spiele
* 15:00 Uhr  Redebeiträge
* 15:45 Uhr  Musik „Wollstiefel“
* 16:15 Uhr  Vorträge
* 16:45 Uhr  Tanz mit Rhythms of Resistance

<img src="/pages/material/images/karlstrasse.jpeg" width="640" height="640" style="width: 100%">

**Nachtrag:**

Die Stimmung war ausgezeichnet.
Mitten auf der Karlstraße wurde Federball und Frisbee gespielt.
Es gab leckeres Essen und Informationsstände.
Man konnte sich auf Bierbänken, Stühlen und
sogar dem ein oder anderen Liegestuhl ausruhen
und der Musik lauschen.
Zur Verzierung hatte man einige Pflanzen und ein Beet aufgestellt.

Selbst als ein starker Regenschauer hereinbrach
und man sich in aufgebauten Zelten unterstellte,
blieb die Stimmung positiv.
Einige ignorierten den Regen vollkommen
und setzten ihr Frisbeespiel fort.

Das Straßenfest setzte ein deutlich Zeichen dafür,
wie öffentlicher Raum in der Innenstadt genutzt werden kann,
wenn er nicht für Autos reserviert wird.

Während unseres Straßenfests sank
laut der in der Karlstraße stehenden Messstation
die Konzentration des Schadstoffes NO₂ auf 8 µg/m³.
Zum Teil war dies sicher auch dem Regen zu verdanken.
Trotzdem, dieser Wert war zuletzt
am Samstag (28.05.2022) um 5 Uhr in der Früh erreicht worden.<br>
Quelle: [https://www.lfu.bayern.de/luft/immissionsmessungen/messwerte/stationen/detail/1401/172/](https://www.lfu.bayern.de/luft/immissionsmessungen/messwerte/stationen/detail/1401/172/)


## Freitag 27.05.2022 -- **Tag 696**

### 9:30 – Demonstration für autofreie Zonen um Schulen

Um die Pause der Schüler\*innen der Ulrichsschule
und des Holbein-Gymnasiums sicherer zu gestalten,
findet diesen Freitag während der großen Pause
wieder eine Demonstration in der Hallstraße statt.
Ähnliche Aktionen hatte es dort schon am 13. Mai
und am 17. Mai dieses Jahres gegeben.

**Ort:** Hallstraße<br>
**Zeit:** ab 9:30

Unter dem Motto „Auto-Frei-Tag“
soll dies in Zukunft regemäßig geschehen.


### 18:00 – Critical Mass

Wie an jedem letzten Freitag im Monat
findet heute wieder eine Critical Mass statt.
Start is um 18 Uhr am Rathausplatz / Klimacamp.

**Ort:** Rathausplatz / Klimacamp<br>
**Zeit:** 18:00


## Mittwoch 25.05.2022 -- **Tag 694**

Um 18 Uhr veranstalten die Students for Future Augsburg
im Dompark ein Neuentreffen für alle Interessierten.
Der Plan ist sich kennen zu lernen, zusammen etwas zu trinken
und sich auszutauschen.
Dort kann man unter anderem erfahren,
welche Projekte nun nach Abschluss
der zweiten Augsburger Public Climate School
in Vorbereitung sind.

**Nachtrag:** An diesem Tag begannen vor Ort
die Vorbereitungen für eine Waldbesetzung im Bobinger Auwald.
Grund sind Rodungsabsichten des Wehringener Bürgermeisters.

Mehr Informationen auf:
[https://www.bobinger-auwald-bleibt.de/](https://www.bobinger-auwald-bleibt.de/)


## 20. Kalenderwoche 2022 (16.05. bis 22.05.)

Diese Woche bietet ein reichhaltiges Programm.

* Montag bis Freitag: [Public Climate School](https://www.klimauni-augsburg.de/)
* Mittwoch: Filmabend mit veganem Essen
* Freitag: gemeinsame Pressekonferenz von FFF-Augsburg und dem Klimcamp
* Freitag: Schulstreik
* Donnerstag bis Sonntag:
  [endlich. Das Augsburger Klimafestival](https://staatstheater-augsburg.de/klimafestival)


### Public Climate School

Von Montag (16.05.2022) bis Freitag (20.05.2022)
findet die von den *Students for Future Augsburg*
organisierte Public Climate School statt.
Mehr Informationen auf können auf der offiziellen Webseite
der Veranstaltung nachgelesen werden:
[https://www.klimauni-augsburg.de/](https://www.klimauni-augsburg.de/).


### Mittwoch 18.05.2022 -- **Tag 687**

Ab 19:30 Uhr Filmabend mit veganem Essen: 📽 Die rote Linie — Widerstand im Hambacher Forst 📽

Die Geschichte eines Walds, der für eine Kohlegrube gerodet werden sollte.
Eines Walds, in den Klimaaktivist\*innen zur Verteidigung einzogen.
Eines Walds, der mit seinen Baumhausdörfern Freiraum und Bildungsraum wurde.
Eines Walds, der von Laschet unter einem Vorwand geräumt werden sollte.
Und vor allem: Die Geschichte eines Walds, der bleibt. 🌱

*Die aktivistische Verteidigung des Hambacher Forsts war erfolgreich:*
Sechs Jahre nach der Initialbesetzung erklärte ein Gericht die Rodung
für rechtswidrig (und letztes Jahr dann auch die Räumung).

Kommt gerne vorbei 👋 Nach dem Film gibt es genügend Möglichkeit zum
gemeinsamen Austausch:
Wie sieht der Alltag in so einer Waldbesetzung aus?
Was macht Waldbesetzungen so schön, egal ob mensch dort zwei Tage,
zwei Wochen oder zwei Monate verbringt?
Wo liegen die nächsten aktiven Waldbesetzungen?


### Freitag 20.05.2022 -- **Tag 689**

#### 10:00 Gemeinsame Pressekonferenz mit FFF-Augsburg

Wie auch im Fall von [Pimmelgate Süd](https://www.pimmelgate-süd.de/)
planen wir an diesem Freitag gemeinsam mit FFF-Augsburg
eine Pressekonferenz abzuhalten.
Während Pimmelgate Süd den bislang letzten Fall in einer Serie
von Übergriffen durch die Abteilung „Staatsschutz“ darstellt,
geht es in dieser Pressekonferenz darum, den ersten Fall aufzuarbeiten.
<br>
Ort: Moritzsaal der Augsburger Moritzkirche<br>
Pressemappe mit Details: [https://www.pimmelgate-süd.de/kreide/](https://www.pimmelgate-süd.de/kreide/)

Eventuell veröffentlichen wir in einiger Zeit noch
eine Aufzeichnung der Pressekonferenz.
Falls dem so seien wird, wollen wir hier auf sie verlinken.

**Nachtrag:**
Der Vortragsteil des Pressekonferenz dauerte etwa eine Stunde.
Es folgte eine etwa 20 Minuten lange Fragerunde.
Im Anschluss gab es dann noch kleinere Gesprächsrunden
zwischen Vertreter\*innen von FFF-Augsburg,
dem Klimacamp und der Presse.

Bereits bei Pimmelgate Süd war angedeutet worden,
dass es kein Einzelfall gewesen sei,
sondern lediglich der Tropfen,
der das Fass zum überlaufen brachte.
Nun wurde ein kleiner Einblick in den Inhalt des Fasses gegeben.
Das Thema der Pressekonferenz waren zwei Hausdurchsuchungen,
die auf den Tag genau zwei Jahre zuvor stattgefunden hatten.
Die Umstände der Hausdurchsuchungen muten ähnlich unverständlich an
wie im Fall von [Pimmelgate Süd](https://www.pimmelgate-süd.de/).
Pimmelgate Süd war der Anlass,
nun auch diese alten Fälle medial aufzuarbeiten.

Vorwand für die Hausdurchsuchungen war eine Sprühkreideaktion
in der Nacht auf den 29.11.2019.
Richtig gelesen:
Die Hausdurchsuchung sollte Beweismittel
für eine Sprühkreideaktion sicherstellen,
die fast sechs Monate zuvor stattgefunden hatte.
Begründet wurde dies auch mit „Gefahr im Verzug“.
Die Albernheiten hören hier nicht auf.

Bei der Sprühkreideaktion waren mit wasserabwaschbarer Sprühkreide
konsumkritische Sprüche vor Läden gesprüht worden.
Weiter hatte sich Greenpeace Augsburg zu der Sprühkreideaktion bekannt.
Die Medien hatten darüber berichtet.
Die Polizei hätte also einfach an Greenpeace Augsburg
eine Rechnung für die Reinigung schicken können.
Stattdessen entschied sie sich für den massiven Grundrechtseingriff
einer Hausdruchsuchung.
Die beiden hausdurchsuchten Aktivist\*innen, Ingo und Janika,
waren und sind jedoch nicht Mitglieder von Greenpeace Augsburg.
Bekannt waren sie vor allem für ihre Tätigkeit bei FFF-Augsburg,
beispielsweise durch das Halten von Reden bei Demonstrationen,
und in städtischen Beiräten.
Hinzu kommt, dass Janika als Kritikerin von Konsumkritik bekannt ist.
Sie stand schon damals für politische Lösungen,
nicht für eine Schuldzuweisung an einzelne Konsument\*innen.

Im Verlauf der Pressekonferenz schilderte Janika,
wie in ihrem Fall der Tatverdacht dadurch begründet wurde,
dass ein Überwachungsvideo ein Mädchen mit ähnlicher Körpergröße
und ähnlicher Jackenfarbe zeigen solle.
Das Video durften die Beschuldigten nie sehen,
auch nicht, als das Verfahren nach eineinhalb Jahren
ohne Anklageerhebung eingestellt wurde.
Die Aussage der Mutter,
dass ihre Tochter in der Tatnacht zu Hause war,
wurde einfach abgetan.

Die Augsburger Klimagerechtigkeitsbewegung
traf der Vorfall damals noch unvorbereitet.
Polizist\*innen kannte man bis dahin als Freund\*innen und Helfer\*innen,
sowie als Begleitung von Demonstrationszügen.

Ingo beschrieb in der Pressekonferenz,
welcher Wandel durch die Hausdurchsuchungen
in Gang gesetzt wurde.
Hätten die Hausdurchsuchungen nicht stattgefunden,
wäre es bei friedlichen Demonstrationen als Protestform geblieben.
Stattdessen wurde durch die Hausdurchsuchungen innerhalb der
Augsburger Klimagerechtigkeitsbewegung ein Reflektionsprozess
und eine Analyse der politischen Verhältnisse angestoßen.
Friedliche Demonstrationen
wurden ergänzt um weitere Aktionsformen.
Etwas Trost findet Ingo in dem Gedanken,
dass die Gründung des Klimacamps als eine indirekte Folge
der Hausdurchsuchungen sowohl in Augsburg viel bewirkte
als auch deutschlandweit nachgeahmt wurde und wird.
<br>
*(So nicht gesagt:
Damals hatte man noch Respekt und Ehrfurcht vor amtlichen Bescheiden.
Heute schreibt man mal eben selbst eine erfolgreiche Klage
gegen so einen Bescheid, ohne wegen dieser Lappalie
die eigene Anwältin zu behelligen.
Folge: Das Ordnungsamt lässt man ohne rechtsgültigen Bescheid
dafür aber mit den Gerichtskosten zurück.)*

Alex, welcher im Kontext von Pimmelgate Süd erst vor wenigen Wochen
Opfer einer Hausdurchsuchung geworden war,
erzählte, wie die Hausdurchsuchungen bei Ingo und Janika dazu führten,
dass man sich innerhalb der Augsburger Klimagerechtigkeitsbewegung
intensiv mit den eigenen Rechten und dem korrekten Verhalten
bei Hausdurchsuchungen auseinandersetzte.
Dieses Wissen half ihm selbst bei seiner eigenen Hausdurchsuchung,
derart professionell zu reagieren
und Rechtsverstöße von Seiten der Polizei,
wie die Verweigerung eines Anrufs bei seiner Anwältin,
schriftlich und von der Polizei unterschrieben
im Durchsuchungsprotokoll festhalten zu lassen.

Ingo und Janika trafen die Hausdurchsuchungen noch unvorbereitet.
So erzählte Janika, wie sie zulies,
wie Polizist\*innen ihre Wäsche durchsuchten und
ihr Tagebuch und andere Notizen lasen und abfotografierten,
obwohl sich der Durchsuchungsbeschluss nur auf Handys,
Sprühkreide und Schablonen bezog.
Sie erzählte, wie sie in Unwissenheit ihrer eigenen Rechte
der Aufforderung der Polizei nachkam,
und ihre Passwörter herausgab und so den Beamt\*innen
Inhalte privater Kommunikation preisgab.
Heute weiß man in der Augsburger Klimagerechtigkeitsbewegung,
dass Passwörter niemals herausgegeben werden müssen
und hält am Klimacamp Verschlüsselungsworkshops ab.

Ebenfalls auf der Pressekonferenz sprach Birgit Zech,
welche Janika nach der Hausdurchsuchung psychologisch
betreute, bei den *Psychologists for Future* aktiv ist
und auch schon am 17.09.2020 mit einer Kollegin zusammen
einen [Vortrag am Klimacamp](/tagebuch/2020/#1930-uhr-gespr%C3%A4chsrunde-zur-emotionalen-verarbeitung-der-klimakrise-von-zwei-psychologists-for-future)
gehalten hatte.
Sie erklärte typische psychologische Folgen einer Hausdurchsuchung,
die vergleichbar mit denen eines Einbruchs seien.

Janika und Ingo erzählten weiter,
wie sie auf's Polizeirevier mitgenommen wurden.
Sie mussten sich halbnackt ausziehen und Fingerabdrücke abgeben.
Die damals erst 15-jährige Janika erzählte in der Pressekonferenz,
wie ihre Hand auf den Fingerabdruckscanner gedrückt wurde.
Die erfassten personenbezogenen Daten
sind aller Wahrscheinlichkeit noch heute bei der Polizei gespeichert.

Vor zwei Jahren lies man der Polizei
eine Überschreitung ihrer eigenen Befugnisse noch durchgehen.
Wie der Fall Pimmelgate Süd gezeigt hat,
entwickelt man innerhalb der Augsburger Klimagerechtigkeitsbewegung
eine Sensibilität und Nulltoleranzgrenze gegenüber Rechtsverstößen
durch Beamt\*innen im Dienst.
Die meisten Übergriffe lassen sich auf die selbe
kleine Gruppe von Beamten zurückführen.
Ingo nahm sich auf der Pressekonferenz die Zeit um zu erklären,
dass man keinesfalls einen Generalverdacht gegen alle
Polizeibeamt\*innen ausprechen wolle.
Bei vielen Anlässen arbeite man mit der Polizei gut zusammen.
Als Beispiel kann hier die Organisation
von Demonstrationen genommen werden.

Sowohl Ingo als auch Janikas Mutter bedankten
sich für die Unterstützung durch Greenpeace,
welches die juristische Verteidigung in dem Fall übernahm,
obwohl es sich bei den Betroffenen nicht um Mitglieder handelte.

Es kann mit Spannung erwartet werden,
welche alten Fälle man in Zukunft ebenfalls noch publik machen wird
und wie sich die Polizei und insbesondere
die Augsburger Abteilung „Staatsschutz“ in Zukunft
gegenüber Vertreter\*innen der Klimagerechtigkeisbewegung
verhalten wird.


#### 11:00 Schulstreik

Die letzten [Weltklimaberichte](/weltklimaberichte/)
haben noch einmal stark vor Augen geführt,
wie schnell uns die Zeit davon läuft,
wenn wir eines der milderen Erwärmungsszenarien anstreben möchten.
[Fridays for Future Augsburg](https://www.fff-augsburg.de/)
ruft nun wieder zum Schulstreik auf.

**Zeit**: 11 Uhr<br>
**Ort**: Rathausplatz

Siehe auch:
[Pressemitteilung zum rechtswidrigen Bescheid des Ordnungsamtes gegen die Versammlung](/pressemitteilungen/2022-05-19-stadt-verliert-vor-gericht-gegen-schulstreik/)

**Nachtrag:**
Die Demonstration begann gegen 11 Uhr,
während im nahen Moritzsaal noch die Pressekonferenz lief.
Nach ziemlich genau 20 Minuten am Rathausplatz
brach die Demonstration auf und zog ihre Runde
vorbei am Dom, der Ganzen Bäckerei, dem Klinkertor
und dem Landratsamt.
In der Nähe des Landratsamtes
hielt die Demonstration dann für eine kurze Rede von Janika,
die nach der Pressekonferenz zur Demo hinzugestoßen war.
Im Anschluss ging es vorbei am Hauptbahnhof
und über Königsplatz und Moritzplatz zurück zum Rathausplatz.
Hier wurde die Demonstration für beendet erklärt.
Zahlreiche Teilnehmer\*innen nutzten die Gelegenheit
noch für einen Besuch am Klimacamp.

Die Demonstration zählte etwa 150 Teilnehmer\*innen.
Damit war die Teilnehmer\*innenzahl ziemlich genau dort,
wo wir sie uns gewünscht hatten.
Wären deutlich mehr Teilnehmer\*innen gekommen,
hätte es sogar Probleme geben können.

Hintergrund:
Im Vorfeld der Demonstration hatte es einen Rechtstreit mit der Stadt gegeben.
Die Stadt hatte versucht, den Beginn und das Ende
der Demonstration auf den deutlich abgelegeneren
Elias-Holl-Platz zu verbannen.
Als Grund wurde eine befürchtete Störung der Veranstaltung
„Frühschoppen mit Fuggerei-BewohnerInnen“
durch eine Großdemonstration am Rathausplatz angegeben.
Gegen diese rechtswidrige Versammlungsauflage hatte FFF-Augsburg
geklagt und Recht bekommen.
Dabei war von Seiten von FFF-Augsburg auch argumentiert worden,
dass man mit maximal 200 Teilnehmer\*innen rechnet.

Siehe auch nochmal
[Pressemitteilung vom 19.05.2022](/pressemitteilungen/2022-05-19-stadt-verliert-vor-gericht-gegen-schulstreik/).

Diese Demonstration war als Schulstreik ausgelegt
und richtete sich überwiegend an Schüler\*innen.
Das klappte hervorragend.
Tatsächlich stellten junge Kinder
den überwiegenden Teil der Teilnehmer\*innen.
Die Demosprüche schallten lautstark durch die Straßen.
Die Stimmung auf der Demo war überaus positiv
und von einem jungen Elan getrieben,
der uns auf den größeren Demos machmal fehlt.

Im Gespräch ist, ob FFF-Demonstrationen in der näheren Zukunft,
vielleicht abwechselnd vormittags, als Schulstreik,
und spät nachmittags, zur Erleichterung der Teilnahme für Berufstätige,
stattfinden sollten.


### endlich. Das Augsburger Klimafestival

„*endlich. Das Augsburger Klimafestival*“ des Augsburger Staatstheaters
findet vom Donnerstag 19.05.2022 bis Sonntag 22.05.2022 statt.
Weitere Informationen, inklusive des Programms des Festivals, können
auf der offiziellen Webseite der Veranstaltung nachgelesen werden:
[https://staatstheater-augsburg.de/klimafestival](https://staatstheater-augsburg.de/klimafestival)

Wir waren am *Marktplatz der Möglichkeiten*,
(Samstag, den 21.05.2022, von 11 Uhr bis 18 Uhr und
Sonntag, den 22.05.2022, von 12 Uhr bis 16 Uhr)
auf der Theaterwiese im Martini-Park mit einem eigenen Stand vertreten.

Auch die Theatervorführungen auf der Brechtbühne
wurden von Einigen von uns besucht.

Vielen Dank an das Augsburger Staatstheater
für die Organisation eines so schönen Events.


## Sonntag 15.05.2022 14:30 – Kidical Mass – **Tag 684**

Die Kidical Mass ist eine Fahrraddemo für alle,
bei der besonderer Wert auf ihre Kinderfreundlichkeit gelegt wird.
Start ist um 14:30 auf dem Platz vor der City Galerie.

Mehr Infos auf [https://kidical-mass-augsburg.de/](https://kidical-mass-augsburg.de/).


## Freitag 13.05.2022 -- **Tag 682**

### 10:00

Um 10:00 Uhr stellen in der Hallstraße
Verkehrswendeaktivist\*innen ein
über die letzten Monate gemeinsam erarbeitetes Konzept
für eine [Verkehrswende](/informationen/artikel/mobilitaetswende/)
in Augsburg vor.

Links:
* [https://www.verkehrswende-augsburg.de/](https://www.verkehrswende-augsburg.de/)
* [Flyer (Stand 1. Mai)](https://www.verkehrswende-augsburg.de/assets/Flyer%20Verkehrswendeplan%20Augsburg.pdf)
* [Artikel des Klimacamps zur Verkehrswende](/informationen/artikel/mobilitaetswende/)
* [Pressemitteilung zum Verkehrswendeplan Augsburg](/pressemitteilungen/2022-05-11-verkehrswende-augsburg/)


### 16:00

Die Stadt Augsburg lädt zum 1. Mobilitätsforum.
Am Event soll es neben Vorträgen auch
Möglichkeiten zur Bürgerbeteiligung geben.<br>
**Ort**: Kongress am Park<br>
**Zeit**: 16:00 bis 20:00<br>
Weitere Informationen zur Veranstaltung:

* [https://www.augsburg.de/aktuelles-aus-der-stadt/detail/augsburger-mobilitaetsplan-wird-neu-aufgestellt](https://www.augsburg.de/aktuelles-aus-der-stadt/detail/augsburger-mobilitaetsplan-wird-neu-aufgestellt)
* [https://www.augsburg.de/buergerservice-rathaus/verkehr/augsburger-mobilitaetsplan](https://www.augsburg.de/buergerservice-rathaus/verkehr/augsburger-mobilitaetsplan)

#### Nachtrag

Es waren einige hundert Menschen gekommen.
Etwa sechzig weitere Teilnehmer waren online zugeschaltet.
Positiv war, dass sich Vertreter der Stadt zumindest mündlich
zu einer echten [Verkehrswende](/informationen/artikel/klimaaktivismus/)
bekannten.

Zum Ablauf: Es gab mehrere Vortragsrunden, gefolgt von Pausen
für jeweils einige Fragen aus dem Publikum (online wie offline).
Es gab hohes Interesse an Beteiligung und
nicht alle Personen mit Fragen konnten dran genommen werden.

In den Vorträgen wurden viele interessante Fakten aufgezählt.
So wurde benannt, dass Autos im Durchschnitt 23 Stunden am Tag
nutzlos in der Gegend herum stehen.
Auch wurden Zahlen genannt, wie viele Pendler täglich nach und aus
Augsburg pendeln und
wie die Verteilung der Benutzung verschiedener Verkehrsmittel
in verschiedenen Stadtteilen ist.
Viele dieser Informationen können auch
auf verschiedenen Webseiten der Stadt eingesehen werden.

Auch die Fragen der Teilnehmer\*innen waren interessant.
Enttäuschend war, dass Baureferent Gerd Merkle das Konzept
*[Verkehr 4.0 für den Ballungsraum Augsburg](https://www.verkehr4x0.de/)*,
welches schon seit 2019 in Augsburg diskutiert wird, nicht kannte.
Viele der Ideen aus dem Konzept fanden auch Eingang in
den am Mittwoch zuvor vorgestellten und am Vormittag beworbenen
*[Verkehrswendeplan Augsburg](https://www.verkehrswende-augsburg.de/)*.

Interessant war, dass unter den hunderten Teilnehmern
kein einziger Fürsprecher für einen Ausbau des Autoverkehrs war.
Konsens unter den Teilnehmer\*innen und
den Vertreter\*innen der Stadt schien zu sein,
dass man eine echte Verkehrswende herbeiführen wolle
und den motorisierten Individualverkehr (MIV),
wie in der städtischen Studie ein halbes Jahr zuvor empfohlen,
mindestens Halbieren will.
Auch deutete sich an, dass die Mehrheit der Teilnehmer\*innen
eine autoarme, aber nicht eine vollständig autofreie Stadt wünsche.

Nach Ende des Vortragsteils gab es mehrere Stände,
an denen Bürger\*innen Kommentare und Vorschläge
für verschiedene Schwerpunkte des Mobilitätsplans
diskutieren und in Form von Zetteln an Pinnwänden abgeben konnten.
Hier eine kurze Zusammenfassung der Ergebnisse.

##### **Autoarme Innenstadt**

52 Zettel wurden zum Thema einer autoarmen Innenstadt abgegeben.
Dazu zählen unter anderem:

* Geschwindigkeitsreduzierung
* mehr und große Park-and-Ride-Parkplatze am Stadtrand
* emissionsfreie Zonen
* mutige Experimente und Pilotprojekte
* Reduzierung der Parkflächen
* mehr Grünflächen und Bäume
* stärkere Priorisierung des Radverkehrs
  * bei Ampelschaltung
  * durch Verbesserung des Radwegenetzes

##### **Stadtteile und Anschluss an die Region**

46 Zettel wurden zum Schwerpunkt des Anschlusses
der Stadtteile und der Region abgegeben.

Dazu zählen unter anderem:

* Einführung von Ringlinien und
  Querverbindungen zwischen Randstadtteilen
* Verbesserung der Taktung
* bessere ÖPNV-Anbindung an Start und Zielort
  (erster und letzter Kilometer)
* S-Bahntaktung im Zugverkehr
* eigene ÖPNV-Spuren auf B17 und A8
* besser Verknüpfung verschiedener Verkehrsmittel (Bahn, Bus, Tram usw.)
* Vergünstigung des ÖPNV
* schnelles Handeln
  <br>Viele Personen schien die Sorge zu umtreiben,
  dass ein Mobilitätsplan bis 2038 bedeutet,
  dass die Stadt erst kurz vor 2038 mit der Umsetzung anfängt.

Auch wurden verschiedene konkrete neue ÖPNV-Strecken,
Haltestellen und Fahrradbrücken vorgeschlagen.

##### **Mobilität für Familien, Jugendliche und junge Erwachsene**

48 Zettel wurden zum Thema der Mobilität für junge Menschen abgegeben.

* Ein großer Schwerpunkt mit zahlreichen Städten
  lag auf dem Ausbau des Radwegenetzes.
* Ein zweiter Schwerpunkt lag darauf,
  die Innenstadt sicherer zu gestalten,
  beispielsweise durch eine Reduzierung
  der erlaubten Höchstgeschwindigkeit und breitere Fahrradwege.

##### **Mobilität für ältere Erwachsene, Seniorinnen & Senioren und Mobilitätseingeschränkte**

32 Zettel waren der Mobilität für ältere Menschen
und Mobilitätseingeschränkte gewidmet.
Dazu zählen unter anderem:

* bessere ÖPNV-Anbindung an Start und Zielort
  (erster und letzter Kilometer)
* Geschwindkeitsreduzierung, evtl. vermehrte Geschwindigkeitskontrollen
* durchgängige Barrierefreiheit
* breitere Gehwege und Radwege,
  durchgängig ohne unnötige Unterbrechungen
* Preisreduzierung für den ÖPNV, beispielsweise durch
  kostenlose Jahrestickets für Ältere, die ihren Führerschein abgeben
* kurze Wege, mehr Angebote und Geschäfte im Quartier vor Ort


## Donnerstag 12.05.2022 -- **Tag 681** -- Heimkehr

Nach 154 Tagen am Moritzplatz
begann das Klimacamp heute damit zum Fischmarkt zurückgekehren.

Am Perlachturm hatte Gefahr durch herabfallende Steine bestanden.
Daher war das Klimacamp am 9. Dezember 2021 zum Moritzplatz ausgewichen.

In der Nacht von Donnerstag auf Freitag
übernachteten Aktivist\*innen wieder am Fischmarkt.
Trotzdem ist der Umzug noch nicht abgeschlossen.
Dies soll am Wochenende geschehen.


## Mittwoch 11.05.2022 -- **Tag 680**

Heute ging ein Verkehrswendekonzept, der *Verkehrswendeplan Augsburg*,
an die Öffentlichkeit.
Der Plan war über die letzten Monate
durch Verkehrswendeaktivist\*innen erarbeitetet worden.

Es wird ausdrücklich um Bürger\*innenbeteiligung
für die weitere Verbesserung des Vorschlags gebeten.

* [https://www.verkehrswende-augsburg.de/](https://www.verkehrswende-augsburg.de/)
* [Flyer (Stand 1. Mai)](https://www.verkehrswende-augsburg.de/assets/Flyer%20Verkehrswendeplan%20Augsburg.pdf)
* [Artikel des Klimacamps zur Verkehrswende](/informationen/artikel/mobilitaetswende/)
* [Pressemitteilung zum Verkehrswendeplan Augsburg](/pressemitteilungen/2022-05-11-verkehrswende-augsburg/)


## Samstag 07.05.2022 -- **Tag 676** -- Willkommenstag

Herzliche Einladung zum ersten großen Willkommenstag
am Klimacamp diesen Samstag. 👋

Auch andere Initiativen wie Greenpeace und Students sind dabei 👋

Unzählige Plakate und Flyer hatten
auf den Willkommenstag aufmerksam gemacht.
Er richtete sich vor allem an Personen,
die sich für Klimagerechtigkeit interessieren,
bislang aber nicht so richtig wussten,
wie sie den Einstieg finden sollten,
um sich für Klimagerechtigkeit zu engagieren.

### 13:00 Uhr
Wie geht eigentlich Klimaaktivismus?
Wieso heizt die Politik die Klimakrise weiter an,
obwohl doch alle Fakten bekannt sind?
Unsere Analyse und ein Blick hinter die Kulissen von Klimaaktivismus. 💥

### 13:40 Uhr
Campführung und gemütliche Frage- und Austauschrunde
bei leckerem veganem Essen,
Überblick über die vielfältigen Formen von Aktivismus 🥙

### 17:00 Uhr
**Nachtrag:**
Gegen 17:00 Uhr befestigten drei Aktivist\*innen des Klimacamps
unterhalb des Vordaches der City-Galerie ein Plakat.
Die Aktivist\*innen brachten enorme Klettererfahrung mit sich.
Einige von ihnen hatten beispielsweise bereits
das Brandenburger Tor oder die Basilika in Weingarten erklettert.

Während der Aktion erschienen auch Mitarbeiter des Sicherheitsdienstes
der City-Galerie auf dem Vordach.
Nach mehreren Ohrenzeug\*innenberichten
sagte einer der Männer etwas wie:
„Ich schneid sie ab/runter. Ich hab ein Messer dabei.“
Angesichts von 14 Metern Höhe hätte das mit hoher Wahrscheinlichkeit
den Tod der „Heruntergeschnittenen“ bedeutet.
Diese Drohung löste Panik aus und
führte zu einer unnötigen Gefährdung der Beteiligten.
Später am Boden wurde versucht zu relativieren,
dass es sich bei dieser Aussage um einen Scherz gehandelt haben soll.
Gelacht hat soweit wir wissen aber niemand.
Es war in diesem Moment einfach nur eine dumme da gefährliche Ansage.

Schließlich traf die Polizei ein
– mit drei Einsatzwagen und mindestens elf Beamt\*innen.
Sie filmte und wartete ab,
bis das Plakat fertig angebracht worden war
und sich die Aktivist\*innen abgeseilt hatten.
Damit verhielt sich die Polizei korrekt.
Bei derartigen Kletteraktionen ist den Beamt\*innen
das Eingreifen untersagt.
Nur speziell im Klettern ausgebildete Beamte des SEK
hätten hier eingreifen dürfen.
Während Aktivist\*innen die Rechtslage
und das Kletterhandwerk beherrschen,
ist es für Polizeibeamt\*innen oft eine ungewohnte Situation.
So passiert es, dass sie eine Straftat oder Ordnungswidrigkeit
zu erkennen glauben, wo in Wahrheit keine vorliegt.
Manchmal kann es passieren,
dass sie dann in einen Aktionismus verfallen und glauben,
jetzt irgendwie eingreifen zu müssen.

> Beispiel:
> Das Erklettern einer Straßenlaterne
> ist weder Straftat noch Ordnungswidrigkeit.
> Gerade in der Anfangsphase,
> als Klimaaktivismus gerade erst aufgeblüht ist,
> behandelten Polizeibeamt\*innen Klimagerechtigkeitsaktivist\*innen,
> die sie beim Anbringen von Bannern auf Straßenlaternen antraf,
> als hätten sie diese in flagranti
> bei einem schweren Verbrechen erwischt.
> Inzwischen scheinen die Polizeibeamt\*innen
> hier besser sensibilisiert zu sein.
> Das Klimacamp gibt vereinzelt auch Kletterkurse für Straßenlaternen.

Zwischen 18:00 und 18:30 konnten die Aktivist\*innen dann gehen.
Das Banner hing zu dieser Zeit immer noch.

Für weitere Details siehe auch die
[Pressemitteilung der Aktion](/pressemitteilungen/2022-05-07-banner-an-city-galerie/).

Hire noch einige Bilder:

![Bildinhalt: Zwei Aktivisten hängen bereits unterhalb des Vordaches
knapp oberhalb des noch nicht richtig angebrachten Plakats.
Eine Aktivistin befindet sich angeseilt direkt an der Kante.
Zwei Männer vom Sicherheitsdienst stehen auf dem Vordach.](/pages/material/images/2022-05-07-banner/sicherheitsdienst-klein.png)
![Bildinhalt: Menschen sammeln sich vor der City-Galerie.
Einige Filmen die Aktion mit ihren Handys.](/pages/material/images/2022-05-07-banner/zuschauer-klein.png)
![Bildinhalt: Das Plaket mit Aufschrift „Hausverbot für Klimakatastrophe?“
hängt nun unterhalb des Vordaches.
Die Klimagerechtigkeitsaktivist\*innen beginnen damit sich abzuseilen.](/pages/material/images/2022-05-07-banner/plakat-haengt-klein.png)

### 19:00 Uhr
Tobi Rosswog, einer der Autor\*innen
des kürzlich veröffentlichten spendenvorfinanzierten
[Aktionsbuchs Verkehrswende](https://cycling-the-change.de/),
kommt für eine Lesung und Diskussion ins Camp! 🛣

**Nachtrag:**
Der Vortrag erfolgt in einem großen Stuhlkreis vor dem Camp.
Neben etwa 15 eher festen Teilnehmer\*innen
gesellten sich auch immer wieder Passant\*innen
für einige Minuten dazu und lauschten oder stellten Fragen.
Nach etwa eineinhalb bis zwei Stunden
ging der Vortrag in ein geselliges Beisammensein
mit leckerem veganem Essen über,
welches noch über das Verfassen dieses Tagebucheintrags hinausging.

Besprochen wurden vor allem Erfahrungsberichte und Aktionsformen,
mit denen man die
[Verkehrswende](/informationen/artikel/mobilitaetswende/)
voranbringen kann.
Viel des besprochenen Inhalts kann auch
in dem Buch „Aktionsbuch Verkehrswende“ nachgelesen werden.
Die PDF-Version des Buches unterliegt einer „CC BY-NC-ND 4.0“-Lizenz
und kann kostenlos auf der Webseite des Verlags heruntergeladen werden:
[https://www.oekom.de/buch/aktionsbuch-verkehrswende-9783962383541](https://www.oekom.de/buch/aktionsbuch-verkehrswende-9783962383541)


## Freitag 06.05.2022 -- **Tag 675**

Diesen Freitag (6.5.2022, ~~12:15-13:30~~ 12:30-13:15
passend zum Schulschluss)
veranstalten wir auf der Kreuzung Karlstraße/Karolinenstraße
eine Popup-Rundum-Grün-Demo.
Alle Fußgänger\*innen und Radler\*innen bekommen gleichzeitig Grün,
während alle Autos warten.
Damit ist es zu Fuß und mit dem Rad deutlich bequemer und
Unfälle beim Rechtsabbiegen gehören der Vergangenheit an.
Wir demonstrieren damit für eine Umwandlung von Kreuzungen
nach dem Rundum-Grün-System und weiter gedacht eine Mobilitätswende,
die über eine unsoziale Antriebswende deutlich hinausgeht.<br>
Hintergrund:
[https://klimafreunde.koeln/rundum-grun-macht-rundum-glucklich/](https://klimafreunde.koeln/rundum-grun-macht-rundum-glucklich/)

**Nachtrag:**
Das vorgeschlagene Konzept von Rundum-Grün-Phasen
unterbrochen von Phasen für die Autofahrer\*innen
war so leider nicht genehmigt worden.
Es erschien nicht umsetzbar,
die Ampelschaltung anzupassen.
Den Verkehr entsprechend durch die Polizei regeln zu lassen,
wurde mit der Begründung abgewiesen,
dass man die Autofahrer\*innen
für zu unkonzentriert und zu unvorsichtig hielte
und so eine zu große Gefahr für an der Kreuzung
den Verkehr regelnde Polizeikräfte bestehen würde.
Diese Sorge können vor allem die Radfahrer\*innen
unter uns nachvollziehen,
weshalb wir eine Herabsetzung des Tempolimits
in der gesamten Innenstadt befürworten.
Es klingt zumindest in der Begründung so,
als würde man das bei der Polizei ähnlich sehen.

Der Zeitraum der Veranstaltung
wurde für die Abänderung unseres Konzepts
durch das Ordnungsamt auf 12:30 bis 13:15 verkürzt.
An Stelle unseres Rundum-Grün-Konzepts
sperrte die Polizei den Bereich um die Kreuzung
in größerem Abstand für Autos ab.
Nur ÖPNV (also Straßenbahnen und Busse) wurden durchgelassen.
Dadurch wurde die Kreuzung de facto zur Fußgängerzone.
Das sorgte für zusätzlichen Frust bei Autofahrer\*innen.
Diese durchbrachen mitunter
die unzureichend abgesicherten Absperrungen der Polizei
und wurden dadurch zur Gefahr für Fußgänger\*innen
im Demonstrationsbereich.

Gegen 13:05 wurde die Veranstaltung
– auch aus Rücksicht auf die Polizei – leicht vorzeitig beendet.
Es war anscheinend außerhalb der Sichtweite der Demonstration
im Bereich der Absperrungen zu Chaos gekommen.

Bei der nächsten Popup-Rundum-Grün-Demo
wird das hoffentlich besser organisiert werden.


## Dienstag 03.05.2022 -- **Tag 672**

### 15:30: Essen verschenken
Um ~~17:00~~ 15:30 Uhr verschenken Aktivist\*innen gerettete Lebensmittel.
Beim sogenannten „Containern“ werden noch genießbare Lebensmittel
aus den Mülltonnen von Supermärkten geholt.
Siehe auch unsere Pressemitteilung
„[Klimaaktivist\*innen verschenken gestohlenes Essen: Essen-Retten-Aktion vor Klimacamp](/pressemitteilungen/2022-05-01-gestohlenes-essen-verschenken/)“.

Vorbild ist die Aktion von Pater Jörg Alt in Nürnberg.<br>
Siehe auch Bayerischer Rundfunk:
[Bis die Polizei kommt: Pater aus Nürnberg rettet Lebensmittel](https://www.br.de/nachrichten/bayern/bis-die-polizei-kommt-pater-aus-nuernberg-rettet-lebensmittel,SsFhNiM)

*Anmerkung:
Das Event wurde ursprünglich für 17 Uhr beworben,
später aber auf 15:30 Uhr vorverlegt.
Wir bitten eventuell damit verbundene Unannehmlichkeiten
zu entschuldigen.*

### 18:00: ‼️DEMO AUFRUF‼️ (nicht vom Klimacamp organisiert)

Am 2. Mai ist in Mannheim ein Mensch gestorben,
nachdem dieser mitten in der Innenstadt Opfer von Polizeigewalt wurde.

Die Polizei fühlte sich offenbar sicher genug,
am helllichten Tag in aller Öffentlichkeit
Gewalt an einem Menschen auszuüben, der bereits am Boden liegt.
Dieses Verhalten ist inakzeptabel!

Wir rufen zu einer Eilversammlung auf,
um laut zu sein gegen Machtmissbrauch und Polizeigewalt.

**🗓 Wann?** 3. Mai um **18:00 Uhr**<br>
**📍Wo?** Treffpunkt ist der **Rathausplatz** in Augsburg.
Nach einer Startkundgebung wird wahrscheinlich eine Laufdemo stattfinden.

Kommt zahlreich, lasst uns wütend sein und gemeinsam zeigen,
dass Polizeigewalt keinen Platz in unserer Gesellschaft haben darf! ✊

Hier das Video. Triggerwarnung: extreme Polizeigewalt.
https://twitter.com/Ezgi_Guyildar/status/1521175986066399233

### 19:00: Podiumsdiskussion des Presseclubs Augsburg
Der Presseclub Augsburg veranstaltet eine Podiumsdiskussion.
Mit dabei ist auch ein Vertreter des Klimacamps.
Für Ort und Anmeldeformalitäten verweisen wir auf die offizielle
[Seite des Veranstalters](https://presseclub-augsburg.de/2022/04/mitgliederversammlung-und-club-gespraech-vom-gaswerk-zum-kulturland/).

**Nachtrag:**
![Links eine Person am Podium,
rechts ein Tisch mit vier Stühlen und drei
Personen.](/pages/material/images/2022-05-03-podiumsdiskussion/diskussion-19-37-klein.png)
Von links nach rechts:<br>
Alfred Schmidt, stellv. Vorsitzender des *Presseclub Augsburg*
und Moderator der Diskussion,<br>
Alex Mai, Vertreter des Klimacamps und *Fahrradstadt jetzt*,<br>
Beate Schabert-Zeidler,
ehemalige Richterin und für *Pro Augsburg* im Stadtrat,<br>
Peter Rauscher, Fraktionsvorsitzender der Grünen im Stadtrat,<br>
und Volker Ullrich, welcher das Klimacamp kürzlich aufgefordert hatte,
sich vielmehr der politischen Debatte zu stellen.<br>
Das Foto wurde gegen 19:35 vor der Ankunft Volker Ullrichs aufgenommen.


## Sonntag 01.05.2022 -- **Tag 670** -- Müllsammeln und Plenum

### Müllsammelaktion

Greenpeace organisierte von 11 Uhr bis 15 Uhr
eine Müllsammelaktion am Vogeltor.
Es wurde um die Mitnahme eigener Arbeitshandschuhe gebeten.<br>
Treffpunkt: Vogeltor,
an der kleinen Brücke von der Jakoberwallstraße
über den Äußeren Stadtgraben

***Nachtrag:***
Bereits gegen 13:30 waren genügend Restmüll für etwa vier Müllsäcke,
zahlreiche Flaschen sowie ein ganzer Klappstuhl gefunden worden.
Besonders widerlich waren die Mengen an Zigarettenkippen.

![Ein Eimer mit Müll. Darüber befindet sich ein Schild mit der Aufschrift
„Ist das deins?“](/pages/material/images/2022-05-01-muellsammlung/eigentuemer-gesucht-klein.png)
![Zwei große Eimer, einer mit Restmüll, einer mit Flaschen.
Am linken Bildrand sieht man einen blauen Müllsack.
Im Hintergrund sieht man zahlreiche kleinere Eimer,
mit denen der Müll gesammelt werden kann.](/pages/material/images/2022-05-01-muellsammlung/restmuell-klein.png)
![Weiter Flaschen und Dosen, teilweise bereits sortiert auf verschiedene Eimer](/pages/material/images/2022-05-01-muellsammlung/flaschen-klein.png)

Greenpeace organisiert derartige Müllsammelaktionen immer wieder.
Die Nächste ist für den 7. Mai geplant.
Treffpunkt und Sammelpunkt wird dann von 11 Uhr bis 15 Uhr
an der Freien Waldorfschule Augsburg in der Dr.-Schmelzing-Straße 52
im Stadtteil Hammerschmiede sein.<br>
[Mehr Informationen zum Programm von Greenpeace Augsburg](https://greenwire.greenpeace.de/group/greenpeace-augsburg)


### Plenum zum Standort

Zeitgleich zur Müllsammelaktion fand auch ein Plenum
über den zukünftigen Standort des Klimacamps statt.
Das Klimacamp befand sich von seiner Gründung am 1. Juli 2020
bis in den Dezember 2021 hinein
am Fischmarkt unmittelbar neben dem Rathaus.
Der Fischmarkt war zwischenzeitlich als „Klimacampplatz“
bzw. „Am Klimacamp“ deutlich bekannter
als unter seinem offiziellen Namen.
Aufgrund eines Gutachtens zur Gefahr
des Herabfallens von Steinen vom renovierungsbedürften Perlachturm,
musste das Camp dann zum Moritzplatz umziehen.
Demnächst wird der Perlachturm voraussichtlich soweit gesichert sein,
dass das Klimacamp zu seinem alten Standort zurückkehren könnte.
Für den Fischmarkt sprechen die Nähe zur Augsburger Stadtpolitik.
Der Moritzplatz ist gemütlicher.
Außerdem kommt man hier leichter mit Passanten ins Gespräch.

Bereits am 26. April hatte man seitenweise Argumente
für und gegen die beiden Standorte besprochen.
Heute fiel nun die Entscheidung,
den Rückumzug zum Fischmarkt vorzubereiten.
Die Nähe zur Lokalpolitik ist uns wichtig.
Gerade erst vor wenigen Tagen haben uns
sowohl Oberbürgermeisterin Eva Weber
als auch Stadtrat Peter Hummel
jeweils eine eigene Stellungnahme gewidmet.


## Samstag 30.04.2022 -- **Tag 669**

### 15:00: Fahrraddemo „Fossil free for Peace Ride“

Greenpeace organsiert unter dem Titel „Fossil free for Peace Ride“
eine Fahrraddemo für eine Verkehrswende, Tempolimit, Frieden
und autofreie Sonntage.
Geplant ist eine entspannte Radtour die nach etwa 10km
mit einer Abschlusskundgebung am Provino endet.<br>
Zeit: 15:00 Uhr<br>
Ort: Konrad Adenauer Allee / Königsplatz


## Freitag 29.04.2022 -- **Tag 668**

### 🚴‍♀️ 18:00 Rathausplatz: Critical Mass — entspannt Fahrrad fahren durch die Stadt

Wie jeden letzten Freitag im Monat trifft man sich um 18:00
am Rathausplatz und fährt gemeinsam
über für Autos vorgesehene Straßenspuren.
Das ist keine angemeldete Demo und muss es auch nicht sein.
Ab 16 Radfahrenden ist es durch §27 StVO legal.
Bei dem aktuellen Wetter nehmen meistens mit 100–200 Leuten
an der entspannten Spazierfahrt durch die Stadt teil.
Am 15.5. gibt's auch wieder die
[Kidical Mass](https://augsburg.adfc.de/artikel/kidicalmass),
die bunte Fahrraddemo für alle und insbesondere Kinder!


## Donnerstag 28.04.2022 17:00 – Offenes Kennenlerntreffen – **Tag 667**

Die Parents for Future Augsburg treffen sich im Camp für ein offenes
Kennenlerntreffen. Bei den Parents können sich alle engagieren, auch Menschen,
die nicht Eltern sind. Die Parents gründeten sich zur Unterstützung der Arbeit
der Schüler\*innen und verfolgen zudem eigene Projekte für Klimagerechtigkeit.


## Donnerstag 21.04.2022 -- **Tag 660**

Heute gab der Bayerische Verwaltungsgerichtshof
die schriftliche Begründung seines Urteils
gegen die Stadt ab.
Dies fand ein breites Medienecho,
wie man auch unserem [Pressespiegel](/pressespiegel/#21042022)
entnehmen kann.


## Mittwoch 20.04.2022 -- **Tag 659**

Der heutige Tag ist ein ganz gutes Beispiel,
um zu veranschaulichen,
dass nicht das gesamte Programm des Augsburger Klimacamps
hier auf der Webseite angekündigt wird.
Eine ganze Reihe von Workshops
wird mit relativ kurzer Vorlaufzeit spontan organisiert.
Die Ankündigung erfolgt manchmal einige Stunden vorher
per Aushang direkt am Klimacamp.

Das stand heute auf dem Aushang:

* 18 Uhr: Erste – Hilfe – Skillshare
* 20 Uhr: Plenum Students 4 Future
* 21 Uhr: AG Öffentlichkeitsarbeit


## Samstag 16.04.2022 -- **Tag 655**

In den frühen Morgenstunden gegen 5:30 Uhr
war am Klimacamp ein Feuer gelegt worden.
Angezündet wurde eine Regenbogenfahne.

Das Feuer wurde durch die Aktivist\*innen,
die in dieser Nacht im Camp übernachteten, gelöscht,
bevor es größeren Schaden anrichteten konnte.

Der Fall taucht im Polizeibericht auf.
In Folge dessen berichteten die Medien.
Ein ähnlicher Vorfall am 4. März war nicht erwähnt worden.

Die Polizei nahm Ermittlungen auf und bittet Zeugen,
die sachdienliche Hinweise geben können, darum,
sich bei der Kriminalpolizei Augsburg
unter der Telefonnummer 0821/323 3810 zu melden.


## Donnerstag 14.04.2022 – **Tag 653**

### Gemeinsame Pressekonferenz mit Fridays for Future Augsburg

*Fridays for Future Augsburg* und wir vom *Klimacamp Augsburg*
haben etwas Wichtiges zu sagen.<br>
Zeit: 10 Uhr<br>
Ort: Klimacamp (am Moritzplatz)

Thema: [Pimmelgate Süd](https://www.pimmelgate-süd.de/)

#### Nachtrag

Das Thema der heutigen Pressekonferenz war die Hausdurchsuchung
vom 05. April, der auch die Webseite
[https://www.pimmelgate-süd.de/](https://www.pimmelgate-süd.de/)
gewidmet ist.
Verschiedene Presseberichte über dieses Ereignis
wie auch Presseberichte über andere Ereignisse
rund um das Augsburger Klimacamp können in unserem
[Pressespiegel](/pressespiegel) nachgelesen werden.

Einige Punkte, die uns wichtig sind, jedoch nicht in der
Berichterstattung aller Medien erwähnt werden,
sind die Folgenden.

1. Alex wurde sowohl das Hinzuziehen eigener Zeugen
   als auch ein Anruf bei seiner Anwältin verweigert.
   Das Polizeipräsidium Schwaben Nord hatte auf Medienanfrage behauptet,
   dass bei der Hausdurchsuchung die geltenden Rechtsvorschriften
   beachtet wurden und Alex für ein Telefonat ein Polizeihandy
   angeboten worden sei.
   Dem widerspricht das von den Beamten bei der Durchsuchung
   angefertigte Durchsuchungsprotokoll, in dem klar festgehalten ist:
   „Der Betroffene war mit der DUSU-Zeugin nicht einverstanden
   und ist unzufrieden damit, dass er für die Dauer der Maßnahme
   nicht telefonieren darf.“<br>
   Die [Süddeutsche Zeitung](https://www.sueddeutsche.de/bayern/augsburg-pimmelgate-klimacamp-polizei-1.5566562?reduced=true)
   sowie [Netzpolitik.org](https://netzpolitik.org/2022/augsburg-pimmel-kommentar-fuehrt-zu-razzia-bei-klimaaktivisten/)
   haben diesen zentralen Aspekt
   in ihrer Berichterstattung berücksichtigt.

2. Das ist nicht der erste Fall,
   sondern nur der bislang letzte Fall in einer Kette von Fällen,
   die bis in die Zeit vor die Gründung des Klimacamps zurück reicht.
   In diesem Fall waren sowohl wir als lokale Gruppe
   der Klimagerechtigkeitsbewegung als auch der Betroffene bereit,
   damit an die Öffentlichkeit zu gehen.
   Etwas unglücklich ist, dass im aktuellen Fall
   als Grund der Hausdurchsuchung die Anzeige eines AfD-Stadtrat
   angegeben wurde.
   Manche Medien erliegen dadurch der Versuchung,
   das Ganze als Konflikt zwischen dem Augsburger Klimacamp
   und der lokalen AfD zu inszenieren.
   Diese Darstellung stimmt nicht.
   Die Beteiligung der AfD ist unerheblich für den Missstand,
   der uns dazu brachte damit an die Öffentlichkeit zu gehen.
   In den letzten zwei Jahren gab es andere Vorfälle,
   darunter auch Hausdurchsuchungen,
   in denen andere teilweise noch absurdere Vorwände geschaffen wurden.<br>
   Bislang trugen wir diese Fälle aus verschiedenen Gründen
   nicht an die Medien.
   Wir wollten den Betroffenen den zusätzlichen Stress ersparen.
   Wir dachten, dass dies von unseren Kernthemen ablenken würde.
   Zu den Kernthemen zählen günstigere Busse und Trams,
   Flächenentsiegelung statt -versiegelung
   und allgemein Klimagerechtigkeit.
   Allerdings bereiteten wir uns in Folge
   auf mögliche zukünftige Vorfälle vor,
   beispielsweise indem wir allen interessierten Menschen,
   egal ob Klimagerechtigkeitsaktivist\*in oder nicht,
   Workshops wie „Aktivistische Rechtskunde“
   (siehe auch Tagebucheintrag vom 12.01.2022) anboten.

In der etwa einstündigen Pressekonferenz wurden noch viele andere
Aspekte der Hausdurchsuchung besprochen,
aber dies hier ist nur ein Tagebucheintrag,
kein Protokoll der Pressekonferenz.

Eventuell veröffentlichen wir in einiger Zeit noch
eine Aufzeichnung der Pressekonferenz.
Falls dem so seien wird, wollen wir hier auf sie verlinken.


### Filmabend

20:30 im Klimacamp schauen wir: *Fiese Tricks von Polizei und Justiz*


#### Nachtrag

Der Film behandelt einen hessischen Polizei- und Justizskandal
mit politischen Verstrickungen bis hin zum damaligen Ministerpräsidenten.

Wer neugierig geworden ist, aber den Filmabend verpasst hat,
findet den Film auch hier:
[https://www.youtube.com/watch?v=-N8sRA0ITPk](https://www.youtube.com/watch?v=-N8sRA0ITPk)


## Montag 04.04.2022 -- **Tag 643**

Heute erschien der dritte und letzte Teil des sechsten
Sachstandsberichts zum Weltklima.
Wer Hineinlesen will, findet Links zum Download
der verschiedenen Teile der letzten Klimaberichte
bei uns unter [Weltklimaberichte](/weltklimaberichte/).

Zu den Hauptaussagen des Berichts existieren
die folgenden offiziellen deutschen Übersetzungen:

* [Hauptaussagen von Arbeitsgruppe I](https://www.de-ipcc.de/media/content/Hauptaussagen_AR6-WGI.pdf)
* [Hauptaussagen von Arbeitsgruppe II](https://www.de-ipcc.de/media/content/Hauptaussagen_AR6-WGII.pdf)
* [Hauptaussagen von Arbeitsgruppe III](https://www.de-ipcc.de/media/content/Hauptaussagen_AR6-WGIII.pdf)

Uno-Generalsekretär António Guterres sprach angesichts der
Veröffentlichung von einem „Dossier der Schande,
welches die leeren Zusagen katalogisiert,
die uns auf einen fest Kurs hin zu einer unbewohnbaren Welt gebracht
haben“.<br>
Quelle: [„World on 'fast track to climate disaster', say UN secretary general“ auf Youtube](https://www.youtube.com/watch?v=FD2BGCA6x6Y&ab_channel=GuardianNews)


## Freitag 01.04.2022 15:00 - 19:00 – Müllsammelaktion im Wittelsbacher Park

Greenpeace organisiert an diesem Tag
ein gemeinsames Müllsammeln im Wittelsbacher Park.<br>
Treffpunkt ist an der Ecke Göggingener Straße / Rosenaustraße.

Es wird empfohlen feste Kleidung und Schuhe zu tragen
und gebeten sofern möglich eigene Arbeitshandschuhe mitzubringen.


## 🕊 Sonntag 27.03.2022 – Fahrraddemo über die B17 -- **Tag 635**

**14:00 am Königsplatz:**
Die erste kombinierte Fahrraddemo über die B17 für
die eng verknüpften Themen Frieden und Klimagerechtigkeit.
Tempolimit auf A8 und B17 und Investitionen in Pflege, Bildung
und öffentlichen Nahverkehr und den Ausbau von Erneuerbaren
statt Aufrüstung und fossile Energien.
Mobilitätswende jetzt! Autofreier Sonntag jetzt!

![Die Route der Fahrraddemo führte vom Königsplatz
über die Gögginger Straße und die Eichleitnerstraße auf die B17.
Nachdem die Demo der B17 etwa fünf Kilometer nach Nordwesten folgte,
ging es über die Bürgermeister-Ackermann-Straße,
die Pferseer Unterführung, Frölichstraße und Schaetzlerstraße
zurück zum Königsplatz.](/pages/material/images/b17-tour_2022-03-27.png)
Das Kartenmaterial stammt von [https://www.openstreetmap.org](https://www.openstreetmap.org).

### Nachtrag

Wie bereits am Freitag herrschte bei der Demo
eine ausgesprochen gute Stimmung.
Kinder wie auch Erwachsene entlang der Strecke winkten
uns Teilnehmer\*innen zu.
Wir Teilnehmer\*innen winkten Passant\*innen zu
und drückten durch Klingeln unsere Begeisterung aus.
Auch unter den Autofahrer\*innen,
die aufgrund der Demo einige Minuten warten mussten,
drückten Einige ihre Zustimmung aus,
Andere wirkten gestresst und
auch ein paar Mittelfinger wurden uns gezeigt.

Die Demoroute selbst bildete für viele Teilnehmer\*innnen
einen starken Kontrast zu dem Erlebnis,
welches Radfahrer\*innen in Augsburg sonst so erwartet.
Es gab keine roten Ampeln,
an denen aufgrund der autofreundlichen Schaltung
Minuten gewartet werden musste.
Autos wurden von der Polizei abgeblockt,
bevor sie den Teilnehmer\*innen gefährlich Nahe kamen.
Auf der Route fehlten die üblichen zerschlagenen Glasflaschen,
– *Persönliche Anmerkung:
Außerhalb der Demo führte mich am selben Tag
der Radweg an zwei verschiedenen Stellen
durch zerschlagene Glasflaschen.* –
Schlaglöcher und Kopfsteinpflaster.
Besonders angenehm zu Fahren war der Abschnitt auf der B17.
Kurz vor Ende passierte die Demo noch die bei Radfahrer\*innen
gefürchtete Pferseer Unterführung,
die auch schon im Vertrag zum Radbegehren eigens erwähnt wird.

Eine Zählung entlang der Strecke ergab 362 Teilnehmer\*innen.
Ganz genau lässt es sich nicht sagen,
weil sich Personen entlang der Strecke anschlossen,
manchmal sogar spontan,
während andere die Demo nicht komplett bis zum Königsplatz
zurück fuhren.

#### Im Nachgang

Kurz nach Ende der Demo gab es noch einen Eklat mit der Polizei,
als diese die Identität einer Teilnehmerin feststellte,
die an der Demo oben ohne mit überklebten Nippeln teilgenommen hatte.
Wir gehen davon aus, dass die Handlung der Teilnehmerin rechtmäßig war.
Nach unserem Rechtsverständnis, welches nicht perfekt ist,
sich aber immerhin bereits gegen das Rechtsverständnis
von Augsburgs Bürgermeisterin, einer studierten Juristin,
behaupten konnte, ist die Polizei eigentlich nicht berechtigt,
die Identität von Teilnehmer\*innen einer politischen Versammlung
festzustellen,
sofern keine Bedrohung für ein polizeiliches Schutzgut
oder eine Straftat vorliegt.
Das umfasst nach gängigen Gerichtsurteilen auch den Weg zur
und von der Versammlung.

In Folge der Polizeiaktion bildete sich eine Spontandemo
gegen die Sexualisierung des weiblichen Körpers,
die nach einer kurzen Fahrradphase auf der Karlstraße Platz nahm
und später auch noch zum Rathausplatz weiterzog.


## 🏘 Samstag 26.03.2022 13:00 – Demonstration für Wohnraum

Rathausplatz:
Demonstrationszug für Wohnraum für alle mit verschiedenen
sinnbildlichen Orten Augsburgs als Zwischenstationen


## Freitag 25.03.2022 -- **Globaler Klimastreik** -- **Tag 633**

### 🏘 14:00 vor der Patrizia AG in der Fuggerstraße:

24-Stunden-Mahnwache (https://augsburgfueralle.noblogs.org/)
für fairen Wohnraum und gegen Immobilienspekulation


### Großdemonstration um 16:00 am Ulrichsplatz:<br>

Auch Augsburg wird sich wird sich mit eigenen Veranstaltungen
am globalen Klimastreik beteiligen.
Um 16 Uhr am Ulrichsplatz eine große Demonstration geplant.

Mehr Infos zum globalen Klimastreik:
[Fridays for Future Deutschland zum Globalen Klimastreik am 25. März 2022](https://fridaysforfuture.de/reichthaltnicht/)
<br>
In English: [Fridays for Future on the Global Climate Strike on the 25. March 2022](https://fridaysforfuture.org/march25/)

💒 Das Bistum Augsburg veranstaltet extra für den Globalstreik
eine zuvorgehende Schöpfungsandacht um 13:30 Uhr
in der Kirche Heilig Kreuz und ruft zur Streikteilnahme auf:
[Informationen auf bistum-augsburg.de](https://bistum-augsburg.de/Hauptabteilung-II/Kirche-und-Umwelt/Aktuelles/Globaler-Klimastreik-am-25.-Maerz_id_0)

#### Andere Klimastreiks in der Nachbarschaft

Falls die Anfahrt nach Augsburg für dich zu weit ist,
erkundige dich doch, ob eine andere Demonstration
für dich nicht näher gelegen ist.
An diesem Tag werden an vielen Orten Demos stattfinden.
[Hier](https://fridaysforfuture.de/reichthaltnicht/)
gibt es eine Übersicht über die in Deutschland
an diesem Tag geplaneten Demos.

Hier eine Liste von Demonstrationen in unserer Nachbarschaft.

##### Nördlich von Augsburg:
* Donauwörth: 15:00 an der Westspange 1
* Ingolstadt: 14:00 am Theaterplatz
* Neuburg an der Donau: 13:30 am Schrannenplatz

##### Östlich von Augsburg:
* München: 12 Uhr am Königsplatz

##### Südlich von Augsburg:
* Füssen: 18:00 am Rathaus Füssen
* Kempten: 13:00 am Forum Allgäu
* Schongau: 14:30 am Marienplatz

##### Westlich von Augsburg:
* Memmingen: 11:00 am Marktplatz
* Ulm: 15:00 am Münsterplatz

Alle Angaben ohne Gewähr.
Kurzfristige Planänderungen durch die jeweiligen Veranstalter
werden hier nicht aktualisiert.<br>
Quelle: [https://fridaysforfuture.de/reichthaltnicht/](https://fridaysforfuture.de/reichthaltnicht/)

#### Nachtrag

Die Stimmung bei der Demo war ausgesprochen positiv.
Im Anschluss wurde auch wieder getanzt. 🙂
Die Teilnehmerzahl war nicht so hoch
wie bei den Demos vor der Pandemie
oder der Demo vor der letzten Bundestagswahl.
An verschiedenen Stellen entlang der Strecke wurden
zwischen 950 und 1000 Menschen gezählt.


### 🚴‍♀️ 18:10 Rathausplatz: Critical Mass — entspannt Fahrrad fahren durch die Stadt

Wie jeden letzten Freitag im Monat trifft man sich um 18:00
am Rathausplatz und fährt meistens ab ca. 18:15 gemeinsam
über für Autos vorgesehene Straßenspuren.
Das ist keine angemeldete Demo und muss es auch nicht sein.
Ab 16 Radfahrenden ist es durch §27 StVO legal.
Bei dem aktuellen Wetter nehmen meistens mit 100–200 Leuten
an der entspannten Spazierfahrt durch die Stadt teil.
Am 15.5. gibt's auch wieder die
[Kidical Mass](https://augsburg.adfc.de/artikel/kidicalmass),
die bunte Fahrraddemo für alle und insbesondere Kinder!


## Samstag 12.03.2022

Wir planen wie bereits am Vortag eine Fahrraddemo abzuhalten.
Beginn ist um 16 Uhr am Klimacamp.<br>
Bereits für 15:30 ist ein Workshop
zur Gestaltung von Fahrrädern mit politischen Botschaften geplant.


## Freitag 11.03.2022

Wir planen um 16 Uhr eine Fahrraddemo abzuhalten.
Beginn ist um 16 Uhr am Klimacamp.<br>
Bereits für 15:30 ist ein Workshop
zur Gestaltung von Fahrrädern mit politischen Botschaften geplant.


## Dienstag 08.03.2022 -- **Tag 616**

Der Bayerischen Verwaltungsgerichtshof (VGH)
informierte uns telefonisch,
dass der Räumungsbescheid der Stadt Augsburg vom 10. Juli 2020
auch weiter rechtswidrig bleibt.
Dazu gaben wir im Verlauf des Vormittags
[eine Pressemitteilung](/pages/Pressemitteilungen/2022-03-08-Raeumungsbescheid-rechtswidrig.html)
heraus.

## Montag 07.03.2022 -- **Tag 615**

Heute fand die Verhandlung vor dem Bayerischen
Verwaltungsgerichtshof (VGH) statt.
Schon im Vorfeld hatte man in der Presse
zahlreiche [Berichte](/pressespiegel/)
über die anstehende Verhandlung gesehen,
die mit Spannung erwartet wurde.
Doch warum verhandeln wir überhaupt vor dem VGH?
<br>
Zur Erinnerung:

1. Vertreter der Stadt hatten uns an Tag 10 einen Räumungsbescheid
   überreicht.
2. Wir gingen per Eilverfahren vor dem Verwaltungsgericht Augsburg
   gegen den Räumungsbescheid vor und konnten bleiben.
3. Im Hauptverfahren erklärte das Verwaltungsgericht Augsburg den
   Räumungsbescheid für rechtswidrig.
4. Die Stadt stellte vor dem Bayerischen Verwaltungsgerichtshof den
   Antrag in Berufung gehen zu dürfen. Diesem wurde stattgegeben.<br>
   Siehe: [Pressemitteilung der Stadt Augsburg zur Berufung](https://www.augsburg.de/aktuelles-aus-der-stadt/detail/vgh-laesst-antrag-der-stadt-auf-berufung-gegen-vg-urteil-zum-klimacamp-zu)

So befinden wir uns nun an Tag 615
vor dem Bayerischen Verwaltungsgerichtshof.

Für Details, die über diese trockene Auflistung an Fakten hinausgehen,
planen wir morgen im Verlauf des Tages
[eine Pressemitteilung](/pages/Pressemitteilungen/2022-03-08-Raeumungsbescheid-rechtswidrig.html)
herauszugeben.


## Freitag 04.03.2022 -- **Tag 612**

Heute gab es um 18 Uhr am benachbarten Rathausplatz
eine große Friedenskundgebung.
Anlass war der russische Angriff auf die Ukraine.

In den Abendstunden gab es im Klimacamp einen Brand.
Der Brand brach am Moritzplatz etwa gegen 20 Uhr
an einem Zelt in der südwestlichen Ecke des Camp aus,
sprich der rückseitigen Ecke in Richtung Zeughausplatz.

Nachdem es bemerkt worden war,
konnte das Feuer zügig durch unsere Aktivist\*innen gelöscht werden.

Der Schaden hält sich in Grenzen und
konnte innerhalb weniger Tage ersetzt werden.
Ein Zelt und eine Matratze wurden zerstört.
Ein Schlafsack wurde beschädigt.
Personen waren zu keinem Zeitpunkt in Gefahr.

Die Polizei sah sich den Brandort an.
Wir vermuten Brandstiftung,
können aber auch ein Versehen,
wie eine weggeworfene Zigarette,
nicht komplett ausschließen.

Wir haben den Fall nicht an die Medien gegeben
und auch erst mit über einem Monat Verzögerung
hier in unserem Tagebuch erwähnt.


## Montag 28.02.2022 -- **Tag 608**

Heute wurde der zweite Teil des sechsten Sachstandsberichts zum
Weltklima veröffentlicht.
Die verschiedenen Teile des Berichts sind bereits bei uns unter
[Weltklimaberichte](/weltklimaberichte/) verlinkt.


## Sonntag 20.02.2022 -- **Tag 600**

Heute war der 600. Tag des Klimacamps.
Ist das ein Grund zum Feiern?
Wir sind uns nicht sicher.

Auf der einen Seite machen die Entschlossenheit und der Einsatz
so vieler Menschen für Klimagerechtigkeit Hoffnung und Mut.
Das bekommen wir auch immer wieder von Passant\*innen gesagt.
Die Stadt Augsburg kann stolz sein auf ihr Klimacamp.
Sein 600-tägiges Bestehen ist ein Monument.
Als solches ist das Klimacamp vielleicht
noch nicht ganz so bekannt wie der Perlachturm,
im Gegensatz zu [diesem](https://www.augsburg.de/aktuelles-aus-der-stadt/detail/gefahr-durch-steinschlag-am-perlachturm-fischmarkt-gesperrt)
bröckelt
[es aber auch nicht](/pages/Pressemitteilungen/2022-01-29-Durchhalten.html). 😄

Auf der anderen Seite zeigt es,
wie schnell uns die Zeit davon läuft
und wie langsam Klimagerechtigkeitsmaßnahmen beschlossen,
geschweige denn umgesetzt werden.
Das geringe CO₂-Restbudget,
das uns für die überlebenswichtige 1,5°C-Grenze
oder auch nur die 2°C-Grenze noch verbleibt,
wird verschleudert.
CO₂-Neutralität ist die Zukunft.
Daran führt kein Weg vorbei.
Eine entscheidende Frage ist,
wie viel Schaden wir dadurch anrichten,
dass wir den Schritt in diese Zukunft verschleppen.
Beim Klimacamp lautet die Antwort auf diese Frage:
„So wenig wie möglich.“

Daher blicken wir mit gemischten Gefühlen auf die letzten 600 Tage zurück
und mit ebenso gemischten Gefühlen in die Zukunft.
<br>
Die Stadt muss sich an ihren Vertrag mit
„[Fahrradstadt jetzt](https://www.fahrradstadt-jetzt.de/)“ halten.<br>
Die Stadt muss den Empfehlungen der
[KlimaKom-Studie](https://www.augsburg.de/fileadmin/user_upload/umwelt_soziales/umwelt/klima%20und%20energie/Studie_Klimaschutz_2030_mit_allen_anlagen.pdf)
folgen.<br>
Die Stadt sollte aus ihrem eigenen Interesse
[unsere Forderungen](/forderungen/) erfüllen.<br>
Es gibt Vieles im Mobilitätssektor zu tun.<br>
Es gibt Vieles im Heizsektor zu tun.<br>
Es gibt Vieles im Energiesektor zu tun.<br>
Am 25. März steht der nächste globale Klimastreik an.

Wir (das Klimacamp und andere Klimagerechtigkeitsorganisationen)
freuen uns auf Eure Mithilfe.

![Foto des Augsburger Klimacamps an Tag 600:
Die Zahl 600 ist auf einem Lastenfahrrad angebracht,
welches vor dem Klimacamp steht.](/pages/material/images/tag_600.jpeg)


## Donnerstag 17.02.2022 -- **Tag 597**

Gegen 19 Uhr ist erneut, wie bereits am 03. Februar,
ein Vortrag von „Aufstand der letzten Generation“ am Klimacamp geplant.<br>
Für Vorträge und Aktionstrainings von „Aufstand der letzten Generation“ in anderen Städten siehe:
[https://letztegeneration.de/vortraege/](https://letztegeneration.de/vortraege/)


## Samstag 12.02.2022 -- **Tag 592**

„Augsburg Solidarisch“ organisiert unter dem Titel
„Solidarisch durch die Krise“ eine Laufdemo.
Mit der Demonstration soll ein Zeichen für
einen evidenzbasierten und sozialen Umgang mit Klimakrise und Pandemie
gesetzt werden.<br>
Beginn um 12 Uhr am Rathausplatz<br>


## Donnerstag 03.02.2022 -- **Tag 583**

Gegen 19 Uhr ist am Klimacamp ein Vortrag von „Aufstand der letzten Generation“ geplant:

> Hey Bundesregierung!
>
> Die bösen Aktivist\*innen blockieren schon wieder Autobahnen? Haben die denn
> nichts besseres zu tun?
>
> Doch! Haben sie.
>
> Und trotzdem gehen viele junge wie alte Menschen auf die Straße, um so sichtbar
> wie kaum woanders für ein Stück bessere Zukunft zu protestieren.
> Der "Aufstand der letzten Generation" fordert im langem Atem einen dringend
> benötigten politischen Kurswechsel, um die 1,5-Grad-Grenze nicht zu
> überschreiten. Dafür haben sie sich ein leicht umsetzbares Ziel gesetzt:
> ein Gesetz gegen Lebensmittelverschwendung durch Großkonzerne.
>
> Ein Vertreter der "letzten Generation" wird am Donnerstag, den 3.2. um 19
> Uhr im Klimacamp am Moritzplatz von der Kampagne und den strategischen
> Überlegungen hinter den Aktionen sprechen. Neben Möglichkeiten, sich selber zu
> engagieren, wird auch auf die Dringlichkeit des Aktionsgrundes eingegangen: die
> Klimakrise und die Zeit, die uns noch zum Handeln bleibt.
>
> Wir freuen uns auf einen regen Austausch und einen interessanten Vortrag.


## Workshops in der 5. Kalenderwoche 2022 (31.01. bis 02.02.)

 * 🏭 (Montag 31.01. 20:00) **Ende Gelände —
   wenn tausende Menschen friedlich Kohleinfrastruktur blockieren**<br>
   Jedes Jahr bietet das Ende-Gelände-Aktionswochenende
   eine ermächtigende Erfahrung, die wie wenige andere Aktionen zeigt:
   Es gibt Hoffnung. Wandel ist möglich. Wir sind nicht allein!<br>
   Im Rahmen von Ende Gelände blockieren mehrere Tausend Aktivist\*innen
   friedlich Kohleinfrastruktur — indem sie auf den speziellen Gleisen
   übernachten, die Kohlegrube und Kohlekraftwerk verbinden,
   oder sich in die Kohlegruben begeben.
   Sind dort Passant\*innen, dürfen die Bagger nämlich nicht mehr arbeiten.<br>
   Wie läuft eine solche Blockade ab? Was muss mensch mitbringen?
   Was besser zu Hause lassen? Wie sieht die rechtliche Einschätzung aus?<br>
   Der Workshop richtet sich, wie Ende Gelände selbst,
   insbesondere an Menschen mit wenig Aktionsvorerfahrung.

 * 👋 (Dienstag 01.02. 17:00) **Einsteiger\*innentreffen**
 * 🎦 (Dienstag 01.02. 19:00) **Filmvorführung mit anschließender Diskussion:
   Beyond the red lines — Systemwandel statt Klimawandel**<br>
   Ob im rheinischen Braunkohlegebiet, am Hafen von Amsterdam
   oder auf den Straßen von Paris während des Weltklimagipfels,
   die Kämpfe für Klimagerechtigkeit werden an immer mehr Fronten geführt.
   Beyond the red lines (Jenseits der roten Linien)
   ist die Geschichte einer wachsenden Bewegung,
   die „Es reicht! Ende Gelände!“ sagt, zivilen Ungehorsam leistet
   und die Transformation hin zu einer klimagerechten Gesellschaft
   selber in die Hand nimmt.

 * 👋 (Mittwoch 02.02. 14:00) **Einsteiger\*innentreffen**
 * ~~🎻 (Mittwoch 02.02. 18:30) **4. Mahnung spielt Erträgliche Musik aus Augsburg. VivA la Akustikpunk!**~~
   <span style="color:rgb(255,32,0)">Das Event fällt aufgrund eines COVID-Falls aus. ☹</span><br>
   Wir wünschen allen Betroffenen eine gute Besserung.<br>
   [4. Mahnung auf Facebook](https://www.facebook.com/VierteMahnung/)


## Workshops in der 4. Kalenderwoche 2022 (24.01. bis 26.01.)

 * 🌐 (Montag 24.01. 20:00) **Livedemonstration Internetunsicherheit**<br>
   Eine Freiwillige loggt sich auf dem eigenen Handy in eine Website ein,
   das Passwort wird dann am Beamer angezeigt. Einstieg in die Grundlagen
   von Netzwerktechnik: Was passiert eigentlich hinter den Kulissen, wenn
   wir im Internet surfen? Ein Workshop für Menschen ohne
   Technikvorkenntnisse, die ihr Sicherheitsbewusstsein schärfen möchten.<br>
   Konsequenzen für Aktivismus und Journalismus.

 * 🔐 (Dienstag 25.01. 18:00) **Verschlüsselungsworkshop**<br>
   (Warum) brauche ich Verschlüsselung auf meinen Geräten? Was kann sie
   leisten, wo liegen ihre Grenzen? Wer möchte, kann gerne Handy oder
   Laptop zum Verschlüsseln mitbringen; dafür vorab ein Backup anlegen,
   Akku voll laden, und Netzteil/Ladekabel nicht vergessen.

 * 🏪 (Mittwoch 26.01. 20:00) **Workshop Konsumkritik-Kritik: von der Mär der
   angeblichen Macht der Verbraucher\*innen**<br>
   Fünf überraschende Sachverhalte, wieso die Erzählung „Dein Kassenbon
   ist ein Stimmzettel“ fehlerhaft ist, in die Irre führt und echte
   gesellschaftliche Veränderung blockiert. Eine grundlegende
   Situationsanalyse und wirksame Alternativen.

Diese Workshops fanden im Klimacamp am Moritzplatz statt.


## Sonntag 23.01.2022 -- **Tag 572**

### 10:00 am Königsplatz -- **Fahrraddemo**
Die Stadt wird sich nur dann
an ihre vertragliche Verpflichtung zum Radbegehren halten,
wenn wir sie daran immer und immer wieder erinnern.
Wer für sichere Radwege und sinnvolle Tempobeschränkungen ist,
kann das am Sonntag auf der Fahrraddemo ausdrücken.

Die geplante Route beginnt und endet am Königsplatz.
Vorgesehen ist auch eine Durchfahrt
des vierspurigen Schleifenstraßen-Tunnels der Nagahama-Allee.
![Route der Fahrraddemo als PNG](/pages/material/images/fahrraddemo_2022-01-23.png)
Das Kartenmaterial stammt von [https://www.openstreetmap.org](https://www.openstreetmap.org).

Fazit:
Die Stimmung war überaus ausgelassen und fröhlich.
Über sechzig Radfahrer\*innen hatten bei kalter Witterung
an der relativ kurzfristig als Ersatzveranstaltung
angekündigten Demonstration teilgenommen.

### 18:00 am Königsplatz -- **Augsburg Solidarisch**

+++ Heute seid ihr gefragt, für Solidarität in Zeiten der Krise auf die Straße
zu gehen! +++

📣 Ab 18:00 Uhr: Kundgebung am Königsplatz von Augsburg Solidarisch

Lasst uns gemeinsam zeigen, dass wir da sind, dass wir laut sind und dass wir
den politischen Diskurs nicht den Schwurbelnden überlassen werden!

Am Samstag, den 22.01. um 18 Uhr wollen wir wieder am Königsplatz gemeinsam auf
die Straße gehen und uns für einen solidarischen Weg durch die Krise stark
machen!

Wir möchten die Demonstrationen der selbsternannten „Coronarebell\*innen“, die
auch diesen Samstag wieder durch Augsburg ziehen werden, nicht unbeantwortet
lassen. Stattdessen möchten wir deutlich machen, dass Verschwörungsglaube,
Wissenschaftsfeindlichkeit und egoistische Freiheitsforderungen nicht in der
Mitte der Gesellschaft stattfinden, keine Mehrheitsmeinung repräsentieren!

Wir möchten zeigen: wir, die sich für einen besonnenen, wissenschaftsfundierten
und solidarischen Weg durch die Krise einsetzen, sind viele!

Und wir möchten zeigen, dass – auch diesen Weg beschreitend – fundierte,
konstruktive Kritik am Krisenmanagement der Bundesregierung möglich und wichtig
ist.

Lasst uns also gemeinsam auf die Straße gehen: für eine gerechtere
Impfstoffverteilung, für faire Löhne im Pflege- und Gesundheitsberufen, für
eine solidarische Krisenpolitik, die nicht auf Kosten der Verwundbarsten in
unserer Gesellschaft stattfindet. Und gegen Verschwörungsglaube,
Wissenschaftsfeindlichkeit und Egoismus!

Alerta!


## Freitag 21.01.2022 15:00 am Rathausplatz -- **Geburtstagskundgebung von Fridays for Future Augsburg**
Neben Redebeiträgen und Musik gibt es auch genügend Möglichkeit
zum informellen Austausch:
Was ist die Vision für die Zukunft, wie geht es weiter?
Herzliche Einladung dazu!


## Workshops in der 3. Kalenderwoche 2022 (17.01. bis 19.01.)

 * 📈 (Montag 17.01. 20:00) **Physikalische Grundlagen der Klimakrise: Mythen und Fakten**<br>
   Wieso hat so wenig CO₂ so große Auswirkungen?
   Woher wissen wir, wie das Klima früher war?
   Wie können wir aus den langsamen Klimaveränderungen der Vergangenheit
   lernen?
   Woher rührt die große Verlässlichkeit unserer Klimamodelle?
   Was sind Konsequenzen für Augsburg, Deutschland und den globalen Süden?
   Zusammenhänge zur Klimakrise allgemeinverständlich erklärt

 * 👩‍🔬 (Dienstag 18.01. 20:00) **Klimagerechtigkeit und Feminismus**<br>
   Zwei Themen, die auf den ersten Blick überhaupt nichts miteinander zu
   tun haben und auf den zweiten untrennbar verknüpft sind. Versteckte
   Zusammenhänge, Konsequenzen für unseren Alltag und unseren Aktivismus
   und eine Einladung zum 8. März.

 * ⚖️ (Mittwoch 19.01. 20:00) **Über die vier prinzipiellen Grundsatzprobleme der
   Demokratie, die systematisch die Klimakrise befeuern**<br>
   Demokratie: Kaum ein Wort ist so positiv besetzt wie dieses. Inwiefern
   legen demokratische Strukturen kontinuierlich einer Lösung der
   Klimakrise Steine in den Weg? Wieso können wir CO₂-Emissionen nicht
   einfach an €SU-Umfragewerte koppeln? Über vier Aspekte, die zu selten
   unser kollektives Bewusstsein erreichen. Eine Analyse aus
   herrschaftstheoretischer Sicht unter besonderer Betrachtung der Vision,
   dass Betroffene von Entscheidungen auf sie Einfluss nehmen könnten.

All diese Workshops fanden im Klimacamp am Moritzplatz statt.


## Workshops in der 2. Kalenderwoche 2022 (10.01. bis 13.01.)

Die aktuelle Woche ist ein besonders guter Zeitpunkt,
in die Augsburger Klimagerechtigkeitsbewegung einzusteigen,
da wir viele Einstiegsworkshops anbieten.
Ideal, um auf den aktuellen Stand zu kommen!

 * (Montag 10.01. 20:00) **Augsburger Lokalpolitik**:<br>
   Wer sind die Akteure und welchen Zwängen unterliegen sie?
   Was sind stabile Forderungen? Wieso fordern wir X und nicht Y?
   Was sind unsere bisherigen Erfolge? Was macht die Stadt schon?
 * (Dienstag 11.01. 20:00) **Das Einmaleins der Pressearbeit**:<br>
   Pressemitteilungen, Begriffsrahmung, Interviews.
   Analyse von Pressemitteilungen der Stadt Augsburg,
   der CSU Augsburg und der Grünen.
 * (Mittwoch 12.01. 20:00) **Aktivistische Rechtskunde**:<br>
   Ordnungswidrigkeiten, Straftaten, Versammlungsrecht;
   aktivistischer Zusammenhalt: unsere Antwort auf ihre Repression
 * (Donnerstag 13.01. 20:00) **Sichere Kommunikation**:<br>
   ein kritischer Vergleich von WhatsApp, Signal & Co.
   Anonymes Surfen im Internet: (Wie) geht das?

All diese Workshops fanden im Klimacamp am Moritzplatz statt.


## [Tagebuch 2021](/tagebuch/2021/)

Das Tagebuch wurde zu lang.
Die Einträge für das Jahr 2021
sind [hierhin](/tagebuch/2021/) umgezogen.

## [Tagebuch 2020](/tagebuch/2020/)

Das Tagebuch wurde zu lang.
Die Einträge des Gründungsjahres 2020
sind [hierhin](/tagebuch/2020/) umgezogen.
