---
layout: page
title: "Dokumente und Errungenschaften"
permalink: /lokalpolitik/dokumente/
parent: "Lokalpolitik"
nav_order: 10
---

# Dokumente und Errungenschaften rund um die Lokalpolitik

Hier sammeln wir Links zu Dokumenten,
die für die Lokalpolitik in Augsburg von besonderer Bedeutung sind.
Für weitere Dokumente,
beispielsweise solcher mit überregionaler Bedeutunng,
siehe auch
unser [Quellenverzeichnis](/informationen/artikel/quellen/).

In Augsburg konnten wir,
das Klimacamp und andere Klimagerechtigkeitsorganisationen,
bereits einige kleinere Erfolge erzielen.
Neben Beschlüssen der Stadt
werten wir auch Verbesserungen der Informationslage
als Errungenschaften.
Den Zugang zu diesen Themen und Dokumenten
wollen wir für die Öffentlichkeit erleichtern.
Dazu zählen:

* Klimaschutzsofortprogramm (Dezember 2020)
* CO₂-Budget (Januar 2021)
* Radentscheid (July 2021)
* Studie „Klimaschutz 2030“ (November 2021)


## Klimaschutzsofortprogramm

[Infos im Ratsinformationssystem](https://ratsinfo.augsburg.de/bi/to020.asp?TOLFDNR=29655)


## CO₂-Budget

* [Infos im Ratsinformationssystem](https://ratsinfo.augsburg.de/bi/to020.asp?TOLFDNR=30051)
* [Weitere Infos im Ratsinformationssystem](https://ratsinfo.augsburg.de/bi/vo020.asp?VOLFDNR=10973)


## Vertrag zwischen der Stadt und dem Aktionsbündnis „Fahrradstadt jetzt“

[Infos im Ratsinformationssystem](https://ratsinfo.augsburg.de/bi/to020.asp?TOLFDNR=31402)


## Klimaschutz 2030: Studie für ein Augsburger Klimaschutzprogramm

Die *Studie für ein Augsburger Klimaschutzprogramm*,
welche nach dem Institut, welches sie durchgeführt hat,
auch oft kurz „KlimaKom-Studie“ genannt wird,
wurde 2020 nach Gründung des Klimacamps von der Stadt Augsburg
in Auftrag gegeben.
Die Ergebnisse wurden im November 2021 veröffentlicht.
Seitdem bilden die Ergebnisse der Studie
einen wichtigen Wegweiser für die Verantwortung der Stadtregierung
und ein zentrales Argument unserer Forderungen gegenüber der Stadt.

* [Studie als PDF](https://www.augsburg.de/fileadmin/user_upload/umwelt_soziales/umwelt/klima%20und%20energie/Studie_Klimaschutz_2030_mit_allen_anlagen.pdf)
* [Zusammenfassung der Studie als PDF](https://www.augsburg.de/fileadmin/user_upload/umwelt_soziales/umwelt/klima%20und%20energie/21-10-29_Klimastudie_Augsburg_Zusammenfassung.pdf)
