---
layout: page
title: "Ausreden"
permalink: /lokalpolitik/ausreden/
parent: "Lokalpolitik"
nav_order: 30
---

# Beliebte Ausreden der Stadt (und warum sie falsch sind)

Hier sammeln wir die Lieblingsausreden der Stadt Augsburg
und erklären, warum wir sie für falsch halten.


## „Uns sind beim Ausbau der Windkraft durch die 10H-Regel die Hände gebunden.“

Kommunen können in ihrer Bauleitplanung Ausnahmen von der 10H-Regel festlegen.
Wenn die Stadt ihre Juristen, die seit eineinhalb Jahren
ihre Zeit mit Klagen gegen das Klimacamp verbringen,
mal eben die Rechtslage prüfen lassen würde,
würde schnell klar werden, wie viel ungenutzten Handlungsspielraum
die Stadt in Bezug auf Windkraftausbau noch hat.


## „Wir tun ja was in Sachen Klimaschutz. Wir sind ja schon größter kommunaler Waldbesitzer Bayerns.“

Wald zu besitzen ist keine Leistung, es ist eine Verantwortung.
An dieser Stelle übrigens vielen Dank an die Initiativen
und vielen Aktivist\*innen,
mit Stadtrat Bruno Marcon als prominentem Vertreter,
die die Stadt am Verkauf des Stadtwaldes gehindert haben.


## „Das Klimacamp ist keine Versammlung.“

Die Stadt argumentiert, dass das Klimacamp keine Versammlung sei
und es damit nicht vom Versammlungsrecht geschützt sei.
Mit dieser Argumentation drohte uns Oberbürgermeisterin
(und Juristin) Eva Weber mit einer Räumung
– also mit einem Ende unserer Versammlung durch äußeren Zwang,
etwa durch uns wegtragende Polizisten.
Den Räumungsbescheid händigte die Stadt uns am 10. Juli 2020 aus.

Gegen diesen Räumungsbescheid haben wir bereits
in den folgenden Verfahren gewonnen:

* Eilverfahren vor dem Verwaltungsgericht Augsburg<br>
  Siehe [Pressemitteilung vom 17. Juli 2020](/pages/Pressemitteilungen/2020-07-17-PM_sieg_vor_gericht.html).
* Hauptverfahren vor dem Verwaltungsgericht Augsburg<br>
  Siehe [Pressemitteilung vom 10. November 2020](/pages/Pressemitteilungen/2020-11-10-PM_Gerichtsurteil.html).
* Hauptverfahren vor dem Bayerischen Verwaltungsgerichtshof (VGH)<br>
  Siehe [Pressemitteilung vom 8. März 2022](/pages/Pressemitteilungen/2022-03-08-Raeumungsbescheid-rechtswidrig.html).

Insofern die Stadt nicht vor dem Bundesverwaltungsgericht
in Berufung geht, gilt der Räumungsbescheid damit als rechtswidrig.
